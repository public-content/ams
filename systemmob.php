<?php
 //session user
$byWho = "0";
include('asset/main.php');
$main = new Engine();
?>
 <!DOCTYPE html>

 <html lang="en" >
  <head>
		<meta charset="utf-8" />
		<title>KPJ Ipoh Specialist Hospital | AMS</title>
		<!-- <meta http-equiv="refresh" content="5"> -->
    <meta name="author" content="Nahahmad">
		<meta name="description" content="KPJ Ipoh Specialist Hospital AMS">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="js/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>

		<link href="css/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<link href="css/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
  <style media="screen">
    .red{color:red;}
  </style>

 	<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default" >
 		<div class="m-grid m-grid--hor m-grid--root m-page">
      <header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="#" class="m-brand__logo-wrapper">
										<img alt="KPJ Ipoh Specialist Hospital GL Portal" src="images/file_more/file_logo/logo.png" width="80"/>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<!-- <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a> -->
									<a id="m_aside_header_topbar_mobile_toggle" href="./index.php?action=<?php print securestring('encrypt','logout');?>" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-logout"></i>
									</a>
								</div>
							</div>
						</div>

						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<!-- <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
								<i class="la la-close"></i>
							</button> -->

							<?php //$main->includephp('view','inc_nav'); ?>

						</div>

					</div>
				</div>
			</header>

 			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <?php
          $allPageDir = "asset/mob/";
				  if(isset($_GET['p']) && trim($_GET['p']) != ''){
					$page	= securestring('decrypt',trim($_GET['p']));
				// 	switch($page){
        //
        //       case "1"	:	include($allPageDir.'dashboard.php');	break;
        //
				// 		default		:	break;
				// 	}
        $pagesql = mysql_query("SELECT * FROM tbl_page WHERE did = '$page'"); if(mysql_num_rows($pagesql)){while($pagerow = mysql_fetch_assoc($pagesql)){
          $pagelink = $pagerow['pageTypeTitle']."_".$pagerow['pageFileName'];

          include($allPageDir.$pagelink.'.php');
          }}else{
            print "<script>window.open('./index.php?action=".securestring('encrypt','no-permission')."','_self');</script>";
          }
				}else{
					print "<script>window.open('./index.php?action=".securestring('encrypt','no-permission')."','_self');</script>";
				}

			  ?>

 			</div>

      <footer class="m-grid__item	m-footer" style="margin-left:0;">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2018 &copy; AMS System by
								<a href="https://kpjipoh.com" class="m-link">
									KPJ Ipoh Specialist Hospital
								</a>
							</span>
						</div>
						<!-- <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">
											About
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#"  class="m-nav__link">
										<span class="m-nav__link-text">
											Privacy
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">
											T&C
										</span>
									</a>
								</li>
							</ul>
						</div> -->
					</div>
				</div>
			</footer>
 		</div>


 		<div id="m_scroll_top" class="m-scroll-top">
 			<i class="la la-arrow-up"></i>
 		</div>
 		<script src="js/vendors.bundle.js" type="text/javascript"></script>
 		<script src="js/scripts.bundle.js" type="text/javascript"></script>
    <script src="js/select2.js" type="text/javascript"></script>
 	</body>

 </html>
