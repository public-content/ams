<?php

 header('Content-Type:image/png');

 require_once 'vendor/endroid/vendor/autoload.php';

 if(isset($_GET['text'])){
	 $text = $_GET['text'];
	 $size = isset($_GET['size']) ? $_GET['size'] : 200;
	 $padding = isset($_GET['padding']) ? $_GET['padding'] : 10;

 }

 use Endroid\QrCode\QrCode;

  $qrCode = new QrCode();
  $qrCode->setText($text);
  $qrCode->setSize($size);
  $qrCode->setMargin($padding);


  header('Content-Type: '.$qrCode->getContentType());
  echo $qrCode->writeString();

  //save qrcode as image
  //$qrCode->writeFile(__DIR__.'/qrcode.png');
  $qrCode->writeFile('images/qrcode/'.$text.'.png');
?>
