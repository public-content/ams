<?php
 //session user
$byWho = "0";
include('asset/main.php');
$main = new Engine();
?>

 <!DOCTYPE html>

 <html lang="en" >
  <head>
		<meta charset="utf-8" />
		<title>KPJ Ipoh Specialist Hospital | AMS</title>
		<!-- <meta http-equiv="refresh" content="5"> -->
        <meta name="author" content="Nahahmad">
		<meta name="description" content="KPJ Ipoh Specialist Hospital AMS">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="js/webfont.js"></script>

		<link rel="stylesheet" href="css/fonts.googleapis.css" media="all">
		<link href="css/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<link href="css/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="images/favicon.ico" />
		<script src="js/jquery.mousewheel.min.js"></script>
		<script src="js/jquery-3.3.1.min.js"></script>
	</head>
  <style media="screen">
    .red{color:red;}
  </style>

 	<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default" >
 		<div class="m-page-loader m-page-loader--base">
			<div class="m-blockui">
			  <span>Please wait...</span>
			  <span>
				<div class="m-loader m-loader--brand"></div>
			  </span>
			</div>
		  </div>
		<div class="m-grid m-grid--hor m-grid--root m-page">
      <header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="#" class="m-brand__logo-wrapper">
										<img alt="KPJ Ipoh Specialist Hospital GL Portal" src="images/file_more/file_logo/logo.png" width="120"/>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>
								</div>
							</div>
						</div>

						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
								<i class="la la-close"></i>
							</button>

							<?php $main->includephp('view','inc_nav'); ?>
							<?php $main->includephp('view','inc_pro'); ?>

						</div>

					</div>
				</div>
			</header>

 			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <?php

          if(isset($_POST['btn-changePassword'])){
          $userID = securestring('decrypt',$_SESSION['userid']);
          $newPass = mysql_real_escape_string(trim($_POST['Npass']));
          $conPass = mysql_real_escape_string(trim($_POST['Cpass']));

            if($conPass == $newPass){
              $hashNewPass = sha1($newPass);
              $sql = mysql_query("UPDATE tbl_user SET password = '$hashNewPass' WHERE did = '$userID'");
              if(!$sql){
              ?>
              <div class="col-lg-12">
                <div class="m-alert m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
    							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    							</button>
    						  	<strong>Ohh no!</strong> Something Wrong.
    						</div>
              </div>
              <?php
            }else{
              $ipaddress = getUserIP2();
              $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                                     VALUES('','','User','Password changed','$ipaddress','$userID',NOW())");
              ?>
              <div class="col-lg-12">
                <div class="m-alert m-alert--square alert alert-success alert-dismissible fade show" role="alert">
    							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    							</button>
    						  	<strong>Success!</strong> Password has been changed.
    						</div>
              </div>
              <?php
            }
            }else{
            ?>
            <div class="col-lg-12">
              <div class="m-alert m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
  							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  							</button>
  						  	<strong>Ohh no!</strong> New password and Confirmation is not match.
  						</div>
            </div>
            <?php
            }

          }

        ?>
        <?php
          $allPageDir = "asset/view/";
				  if(isset($_GET['p']) && trim($_GET['p']) != ''){
					$page	= securestring('decrypt',trim($_GET['p']));
				// 	switch($page){
        //
        //       case "1"	:	include($allPageDir.'dashboard.php');	break;
        //
				// 		default		:	break;
				// 	}
        $pagesql = mysql_query("SELECT * FROM tbl_page WHERE did = '$page'"); if(mysql_num_rows($pagesql)){while($pagerow = mysql_fetch_assoc($pagesql)){
          $pagelink = $pagerow['pageTypeTitle']."_".$pagerow['pageFileName'];

          include($allPageDir.$pagelink.'.php');
          }}else{
            print "<script>window.open('./index.php?action=".securestring('encrypt','no-permission')."','_self');</script>";
          }
				}else{
					print "<script>window.open('./index.php?action=".securestring('encrypt','no-permission')."','_self');</script>";
				}

			  ?>

 			</div>

      <footer class="m-grid__item	m-footer" style="margin-left:0;">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2018 &copy; AMS System by
								<a href="https://kpjipoh.com" class="m-link">
									KPJ Ipoh Specialist Hospital
								</a>
							</span>
						</div>
						<!-- <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">
											About
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#"  class="m-nav__link">
										<span class="m-nav__link-text">
											Privacy
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">
											T&C
										</span>
									</a>
								</li>
							</ul>
						</div> -->
					</div>
				</div>
			</footer>
 		</div>


 		<div id="m_scroll_top" class="m-scroll-top">
 			<i class="la la-arrow-up"></i>
 		</div>
 		<script src="js/vendors.bundle.js" type="text/javascript"></script>
 		<script src="js/scripts.bundle.js" type="text/javascript"></script>
		<script src="js/select2.js" type="text/javascript"></script>
		<script>
			$(window).on('load', function() {
			  $('body').removeClass('m-page--loading');
			});
		</script>
 	</body>

 </html>



 <div class="modal fade" id="changePass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-md" role="document">
     <div class="modal-content">
      <form method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            Changing Password
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
              &times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group m-form__group">
    				<label for="">New password</label>
    				<input type="password" class="form-control m-input" name="Npass">
    			</div>
         <div class="form-group m-form__group">
   				<label for="">Cofirm new password</label>
   				<input type="password" class="form-control m-input" name="Cpass">
   			</div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              Close
            </button>
            <button type="submit" name="btn-changePassword" class="btn btn-brand">
              <i class="la la-lock"></i> Change
            </button>
        </div>
      </form>
     </div>
   </div>
 </div>
