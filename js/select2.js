var Select2 = {
  init: function() {
    $("#m_select2_2, #m_select2_2_validate").select2({placeholder: "Choose"}),
    $(".m-select2-general").select2({placeholder: "Choose"}),
    $("#m_select2_modal").on("shown.bs.modal", function() {
      $("#m_select2_2_modal").select2({placeholder: "Choose"})
    })
  }
};
jQuery(document).ready(function() {
  Select2.init()
});
