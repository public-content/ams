var MorrisChartsDemo = {
  init: function() {
    new Morris.Bar({
      element: "m_morris_3",
      data: [
        {
          y: "Jan",
          a: 100,
          b: 90,
          c: 50
        }, {
          y: "Feb",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Mac",
          a: 50,
          b: 40,
          c: 50
        }, {
          y: "Apr",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "May",
          a: 50,
          b: 40,
          c: 50
        }, {
          y: "Jun",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Jul",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Aug",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Sept",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Oct",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Nov",
          a: 75,
          b: 65,
          c: 50
        }, {
          y: "Dec",
          a: 100,
          b: 90,
          c: 50
        }
      ],
      xkey: "y",
      ykeys: [
        "a", "b", "c"
      ],
      labels: ["Asset", "Inventory", "Condemn"],
      barColors:["#4e7fbf","#43cb71","#d4dd32"],
    })
  }
};
jQuery(document).ready(function() {
  MorrisChartsDemo.init()
});
