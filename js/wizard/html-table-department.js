var DatatableHtmlTableDemo1 = {
  init: function() {
    var e;
    e = $(".m-datatableDe").mDatatable({
      data: {
        saveState: {
          cookie: !1
        }
      },
      search: {
        input: $("#generalSearch")
      },
      columns: [
        {
          field: "Code",
          type: "text"
        },{
          field: "Name",
          type: "text"
        },{
          field: "Location",
          type: "text"
        }, {
          field: "Date",
          type: "date",
          format: "DD-MM-YYYY"
        }
      ]
    }),
    $("#m_form_status").on("change", function() {
      e.search($(this).val().toLowerCase(), "Status")
    }),
    $("#m_form_type").on("change", function() {
      e.search($(this).val().toLowerCase(), "Type")
    }),
    $("#m_form_status, #m_form_type").selectpicker()
  }
};
jQuery(document).ready(function() {
  DatatableHtmlTableDemo1.init()
});
