<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','2');
$pageDesc	= '';
$main->includephp('model','model_dashboard');
$controller = new Dashboard();
?>
<style media="screen">
  #mycanvas{
    width: 100%;
    height: 250px;
    border: solid 2px #ccc;
    padding: 10px;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <!-- <?php echo $pageTitle; ?> -->
          Mobile Pixam
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <!-- <?php echo $pageName; ?> -->
                Menu
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12" style="text-align:center;">
        <?php $alertMe = $alert; ?>
      </div>
      <div class="col-lg-12">
        <div class="col-lg-12"  style="text-align:center;">
          <canvas id="mycanvas"></canvas>
        </div>
        <div class="col-lg-12" style="text-align:center;">
          <button id="btnScan" class="btn m-btn--square  m-btn m-btn--gradient-from-brand m-btn--gradient-to-info" style="width:100%;height:80px;">
  					<span style="text-align:center; font-size:30px">Scan</span>
            <i class="la la-search" style="text-align:center;font-size:30px"></i>
  				</button>
        </div>
        <br>
        <div class="col-lg-12">
          <!-- <button type="button" class="btn m-btn--square  m-btn m-btn--gradient-from-info m-btn--gradient-to-warning" style="width:100%;height:150px;">
  					<span style="text-align:center; font-size:30px">Inventory</span>
            <br>
            <br>
            <i class="flaticon-open-box" style="text-align:center;font-size:30px"></i>
  				</button> -->
        </div>

      </div>
    </div>
  </div>
</div>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="vendor/scanner/jsQR.js"></script>
  <script src="vendor/scanner/Nahqrscan.js"></script>
  <script type="text/javascript">
      $("#mycanvas").hide();
      DWTQR("mycanvas");
      $("#btnScan").click(function(){
        $("#mycanvas").toggle();
        dwStartScan();
      });
      function dwQRReader(data){
        // alert(data);
        window.location.replace("system.php?p=aElPaXgrQ1hESnhCdVdUL3pMcERPZz09&r=" + data);
      }
  </script>
