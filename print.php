<?php
function securestring($action,$string){
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Nor Amirun Hazwan';
    $secret_iv = 'deBiens NahAhmad';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if( $action == 'encrypt' ) {
      $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
      $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ){
      $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}

function countString($value){
  $c = strlen($value);
  if($c >= 18){
  return "<span style='font-size:10px;'>$value</span>";
  }else{
  return "<span style='font-size:16px;'>$value</span>";
  }
}

foreach ($_POST as $key => $value) {
  if($key != 'services' && $key != 'print-selected' ){
    $keys = securestring('encrypt',$key);
  ?>
  <body style="padding:0;margin:0;">
    <div class="" style="display:table;width:302px;height:188px;padding:0;">
      <div class="" style="height:140px;float:left;padding:0;margin-top:24px;margin-bottom:24px;margin-left:10px;">
        <img src="qrcode.php?text=<?php echo $keys ?>&size=120&padding=10" alt="No QR Code" style="padding:0;margin:0;">
      </div>
      <div class="" style="display:inline-block;padding:0;margin:4px 0px;max-width:152px">
        <h1 style="padding:0;margin-top:24px;margin-left:10px;"><?php echo $key ?></h1>
        <p style="padding:0;margin-top:16px;margin-bottom:16px;margin-left:10px;"><b><?php echo countString($value); ?></b></p>
        <h5 style="padding:0;margin-top:16px;margin-bottom:16px;margin-left:10px;">KPJ Ipoh Specialist <br>Hospital</h5>
      </div>
    </div>
  </body>
  <script>
    myPrint();
    function myPrint() {
      window.print();
    }
  </script>
  <?php
  }
}
?>
