<?php
class assetCondemn extends General{
  public function getMaster($asKey,$dataCode){
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE asKey = '$asKey' AND dataCode = '$dataCode'");
    $row = mysql_fetch_assoc($sql);
    return $row['asValue'];
  }

  public function getMasterColumn($column,$dataCode){
    $get = $this->thisMaster($column,$dataCode);
    $sql = mysql_query("SELECT dataValueWrite FROM tbl_category WHERE did = '$get'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataValueWrite'];
  }

  public function getCondemnRule($role){
    $role = securestring('decrypt',$role);
    $sql = mysql_query("SELECT condemnRule FROM tbl_role WHERE did = '$role'");
    $row = mysql_fetch_assoc($sql);
    return $row['condemnRule'];
  }

  public function getUser($did){
    $sql = mysql_query("SELECT name FROM tbl_user WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row['name'];
  }

  public function thisMaster($column,$dataCode){
    $sql = mysql_query("SELECT $column FROM tbl_master WHERE dataCode = '$dataCode'");
    $row = mysql_fetch_assoc($sql);
    return $row[$column];
  }

  public function insertFormFill($data){
    $by = $this->byWho;
    $code = mysql_real_escape_string(trim($data['code']));
    $tagging = mysql_real_escape_string(trim($data['tagging']));
    $cost = mysql_real_escape_string(trim($data['cost']));
    $dateReceived = mysql_real_escape_string(trim($data['dateReceived']));
    $remarks = mysql_real_escape_string(trim($data['remarks']));
    $others = mysql_real_escape_string(trim($data['others']));
    $dataValue = mysql_real_escape_string(trim($data['dataCode']));
    $getDidServices = $this->thisMaster('dataRefServices',$dataValue);
    $ipaddress = $this->getUserIP();

    $sql = mysql_query("UPDATE tbl_condemn SET code = '$code',
                                               tagging = '$tagging',
                                               cost = '$cost',
                                               dateReceived = '$dateReceived',
                                               remarks = '$remarks',
                                               others = '$others',
                                               dataWhoRequest = '$by',
                                               dataStatus = 'Complete Data',
                                               dateModified = NOW() WHERE dataValue = '$dataValue'");
    if(!$sql){
      return mysql_error();
    }else{
      // report
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataValue','condemn','Asset Condemn [Complete Data]: $code','$ipaddress','$by',NOW())");
      return $sql;
    }
  }

  public function returnToAssets($data){
    $dataValue = mysql_real_escape_string(trim($data['dataCode']));
    $code = mysql_real_escape_string(trim($data['code']));
    $getDidServices = $this->thisMaster('dataRefServices',$dataValue);
    $ipaddress = $this->getUserIP();
    $by = $this->byWho;
    $sql = mysql_query("DELETE FROM tbl_condemn WHERE dataValue = '$dataValue'");
    if(!$sql){
      return mysql_error();
    }else{
      // returning to assetsList
      $sqll = mysql_query("UPDATE tbl_master set active = 'Yes' WHERE dataCode = '$dataValue'");
      // report
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataValue','condemn','Asset Condemn [Returning to List]: $code','$ipaddress','$by',NOW())");
      return $sql;

    }
  }

  public function returnNewCondemns($data){
    $dataValue = mysql_real_escape_string(trim($data['dataCode']));
    $code = mysql_real_escape_string(trim($data['code']));
    $getDidServices = $this->thisMaster('dataRefServices',$dataValue);
    $ipaddress = $this->getUserIP();
    $by = $this->byWho;

    $sql = mysql_query("UPDATE tbl_condemn SET dataStatus = 'New Request' WHERE dataValue = '$dataValue'");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataValue','condemn','Asset Condemn [Returning to New Request]: $code','$ipaddress','$by',NOW())");
      return $sql;
    }

  }

  public function pushToDispossal($data){
    $dataValue = mysql_real_escape_string(trim($data['dataCode']));
    $code = mysql_real_escape_string(trim($data['code']));
    $getDidServices = $this->thisMaster('dataRefServices',$dataValue);
    $ipaddress = $this->getUserIP();
    $by = $this->byWho;

    $sql = mysql_query("UPDATE tbl_condemn SET dataStatus = 'Verified' WHERE dataValue = '$dataValue'");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataValue','condemn','Asset Condemn [Verifed]: $code','$ipaddress','$by',NOW())");
      return $sql;
    }

  }

  public function pushToMethod($data){
    $dataValue = mysql_real_escape_string(trim($data['dataCode']));
    $code = mysql_real_escape_string(trim($data['code']));
    $disposalMethod = mysql_real_escape_string(trim($data['disposalMethod']));
    $receiptNo = mysql_real_escape_string(trim($data['receiptNo']));
    $getDidServices = $this->thisMaster('dataRefServices',$dataValue);
    $ipaddress = $this->getUserIP();
    $by = $this->byWho;

    $sql = mysql_query("UPDATE tbl_condemn SET dataStatus = 'Condemn', disposalMethod = '$disposalMethod', receiptNo = '$receiptNo' WHERE dataValue = '$dataValue'");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataValue','condemn','Asset Condemn [Condemn complete]: $code','$ipaddress','$by',NOW())");
      $sqll = mysql_query("UPDATE tbl_master set active = 'Condemn' WHERE dataCode = '$dataValue'");
      return $sql;
    }
  }

}
?>
