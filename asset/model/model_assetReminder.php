<?php
  class reminderer extends General{

    public function getCode($dc){
      $sql = mysql_query("SELECT asValue FROM tbl_master WHERE asKey = 'code' AND dataCode = '$dc'");
      $row = mysql_fetch_assoc($sql);
      return $row['asValue'];
    }

    public function getDataCode($did){
      $sql = mysql_query("SELECT dataCode FROM tbl_reminder WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataCode'];
    }

    public function getDidServices($r){
      $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE asValue = '$r'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataRefServices'];
    }

    public function getUserName($did){
      $sql = mysql_query("SELECT name FROM tbl_user WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      return $row['name'];
    }


    // -------------------------------------------------------------------------------------------------- process

    public function sendChanges($data){
      $did = mysql_real_escape_string(trim($data['did']));
      $whom = mysql_real_escape_string(trim($data['whom']));
      $dateActual = mysql_real_escape_string(trim($data['dateActual']));
      $dateActual = date('d-m-Y', strtotime($dateActual));
      $notifyMe = mysql_real_escape_string(trim($data['notifyMe']));
      $dateAlert = date('d-m-Y', strtotime($dateActual. $notifyMe));
      $remarks = mysql_real_escape_string(trim($data['remarks']));
      $ipaddress = $this->getUserIP();
      $byWho = $this->byWho;
      $dataCode = mysql_real_escape_string(trim($data['dataCode']));
      $getDidServices = $this->getDidServices($dataCode);
      $code = $this->getCode($dataCode);

      $sql = mysql_query("UPDATE tbl_reminder SET email = '$whom',
                                                  dateReminder = '$dateActual',
                                                  notifyValue = '$notifyMe',
                                                  dateAlert = '$dateAlert',
                                                  remarks = '$remarks',
                                                  dateModified = NOW(),
                                                  byWho = '$byWho' WHERE did = '$did'");

      if(!$sql){
        return mysql_error();
      }else{
        // report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$dataCode','Reminder','Reminder has been change $dateAlert : $code','$ipaddress','$byWho',NOW())");
        return 1;
      }
      echo mysql_error();
    }

    public function sendDelete($data){
      $did = mysql_real_escape_string(trim($data['did']));
      $dataCode = $this->getDataCode($did);
      $getDidServices = $this->getDidServices($dataCode);
      $code = $this->getCode($dataCode);
      $ipaddress = $this->getUserIP();
      $byWho = $this->byWho;
      $sql = mysql_query("UPDATE tbl_reminder set status = 'Delete'");
      if(!$sql){
        return mysql_error();
      }else{
        // report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$dataCode','Reminder','Reminder has been Deleted : $code','$ipaddress','$byWho',NOW())");
        return $sql;
      }
    }

  }
?>
