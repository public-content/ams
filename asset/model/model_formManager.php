<?php
class formManager extends General{

  public function valueRef($ref){
    $sql = mysql_query("SELECT dataValueWrite FROM tbl_category WHERE did = '$ref'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataValueWrite'];
  }

  public function saveFieldset($s){
    $by = $this->byWho;
    $dataRef          = mysql_real_escape_string(trim($s['dataRef']));
    $valueLabel       = mysql_real_escape_string(trim($s['valueLabel']));
    // $valueName        = mysql_real_escape_string(trim($s['valueName']));
    $valuePlaceholder = mysql_real_escape_string(trim($s['valuePlaceholder']));
    $valueType        = mysql_real_escape_string(trim($s['valueType']));
    $newValuetype     = str_replace(' ','',$valueType);
    $valueOption      = mysql_real_escape_string(trim($s['valueOption']));
    $valueRequired    = mysql_real_escape_string(trim($s['valueRequired']));
    $getNameNew       = $this->setFieldName($valueType)+1;
    $valueName        = mysql_real_escape_string(trim($newValuetype."fields".$getNameNew));
    // echo $valueName;

    $sql = mysql_query("INSERT INTO tbl_fieldsetunit(dataType,dataValueLabel,dataValueName,dataValuePlaceholder,dataValueType,dataValueDropdown,dataValueRequired,dataRefCategory,dateCreated,dateModified,byWho,active)
                        VALUES('DYNAMIC','$valueLabel','$valueName','$valuePlaceholder','$valueType','$valueOption','$valueRequired','$dataRef',NOW(),NOW(),'$by','Yes')");
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function editFieldset($s){
    $by = $this->byWho;
    $dataRefEdit          = mysql_real_escape_string(trim($s['dataRefEdit']));
    $valueLabelEdit       = mysql_real_escape_string(trim($s['valueLabelEdit']));
    $valuePlaceholderEdit = mysql_real_escape_string(trim($s['valuePlaceholderEdit']));
    $valueTypeEdit        = mysql_real_escape_string(trim($s['valueTypeEdit']));
    $valueOptionEdit      = mysql_real_escape_string(trim($s['valueOptionEdit']));
    $valueRequiredEdit    = mysql_real_escape_string(trim($s['valueRequiredEdit']));

    $sql = mysql_query("UPDATE tbl_fieldsetunit SET dataValueLabel = '$valueLabelEdit',
                                                    dataValuePlaceholder = '$valuePlaceholderEdit',
                                                    dataValueType = '$valueTypeEdit',
                                                    dataValueDropdown = '$valueOptionEdit',
                                                    dataValueRequired = '$valueRequiredEdit',
                                                    dateModified = NOW(),
                                                    byWho = '$by' WHERE did = '$dataRefEdit'");
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function delFieldset($s){
    $did = mysql_real_escape_string(trim($s['did']));
    $sql = mysql_query("UPDATE tbl_fieldsetunit SET active = 'No' WHERE did = '$did'");
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function addOption($s){
    $valueParent = mysql_real_escape_string(trim($s['valueParent']));
    $totalrow = mysql_real_escape_string(trim($s['totalrow']));

    $first = $this->saveParentOption($valueParent);
    if ($first == 1) {
      $parentDid = $this->getParentDid($valueParent);
      for ($i=1; $i <= $totalrow; $i++) {
        $childValue = mysql_real_escape_string(trim($s['child'.$i]));
        if ($childValue != "") {
          $second = $this->saveChildOption($parentDid,$childValue);
        }
      }
      return $second;
    }else{
      return mysql_error();
    }
  }

  public function saveParentOption($valueParent){
    $by = $this->byWho;
    $sql = mysql_query("INSERT INTO tbl_optionunit(dataType,dataValue,dateCreated,dateModified,byWho,active)VALUES('Parent','$valueParent',NOW(),NOW(),'$by','Yes')");
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function getParentDid($valueParent){
    $sql = mysql_query("SELECT did FROM tbl_optionunit WHERE dataValue = '$valueParent'");
    $row = mysql_fetch_assoc($sql);
    return $row['did'];
  }

  public function saveChildOption($parentDid,$childValue){
    $by = $this->byWho;
    $sql = mysql_query("INSERT INTO tbl_optionunit(dataType,dataValue,dataRef,dateCreated,dateModified,byWho,active)VALUES('Child','$childValue','$parentDid',NOW(),NOW(),'$by','Yes')");
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function pushEdit($s){
    $value = mysql_real_escape_string(trim($s['value']));
    $did = mysql_real_escape_string(trim($s['did']));
    $by = $this->byWho;

    $sql = mysql_query("UPDATE tbl_optionunit SET dataValue = '$value', dateModified = NOW(), byWho = '$by' WHERE did = '$did'");
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function pushDel($s,$type){
    $did = mysql_real_escape_string(trim($s['did']));
    if ($type == 'Parent') {
      $sql = mysql_query("DELETE FROM tbl_optionunit WHERE did = '$did' or dataRef = '$did'");
    }elseif ($type == 'Child') {
      $sql = mysql_query("DELETE FROM tbl_optionunit WHERE did = '$did'");
    }
    if (!$sql) {
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function addNewChild($s){
    $dataRef = mysql_real_escape_string(trim($s['did']));
    $row = 4;
    for ($i=1; $i <= $row ; $i++) {
      $value = mysql_real_escape_string(trim($s['childValue'.$i]));
      if ($value != "") {
        $second = $this->saveChildOption($dataRef,$value);
      }
    }
    return $second;
  }

  public function setFieldName($type){
    switch ($type) {
      case 'text':
          $sql = mysql_query("SELECT dataValueType FROM tbl_fieldsetunit WHERE dataValueType = '$type'");
          return mysql_num_rows($sql);
        break;
      case 'number':
          $sql = mysql_query("SELECT dataValueType FROM tbl_fieldsetunit WHERE dataValueType = '$type'");
          return mysql_num_rows($sql);
        break;
      case 'date':
          $sql = mysql_query("SELECT dataValueType FROM tbl_fieldsetunit WHERE dataValueType = '$type'");
          return mysql_num_rows($sql);
        break;
      case 'checkbox':
          $sql = mysql_query("SELECT dataValueType FROM tbl_fieldsetunit WHERE dataValueType = '$type'");
          return mysql_num_rows($sql);
        break;
      case 'select':
          $sql = mysql_query("SELECT dataValueType FROM tbl_fieldsetunit WHERE dataValueType = '$type'");
          return mysql_num_rows($sql);
        break;
      case 'file browser':
          $sql = mysql_query("SELECT dataValueType FROM tbl_fieldsetunit WHERE dataValueType = '$type'");
          return mysql_num_rows($sql);
        break;
      default:

        break;
    }
  }

}
?>
