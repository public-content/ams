<?php
class loan extends General{

  public function sendThisNew($data){
    $assetName = mysql_real_escape_string(trim($data['assetName']));
    $serialNo = mysql_real_escape_string(trim($data['serialNo']));
    $whoLoan = mysql_real_escape_string(trim($data['whoLoan']));
    $startLoan = mysql_real_escape_string(trim($data['startLoan']));
    $locationLoan = mysql_real_escape_string(trim($data['locationLoan']));
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();

    $sql = mysql_query("INSERT INTO tbl_rental(type,item,serialNo,who,startDate,location,dateCreated,byWho,status)
                        VALUES('normal','$assetName','$serialNo','$whoLoan','$startLoan','$locationLoan',NOW(),'$by','active')");

    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('','','Loan','New Loan: $assetName($serialNo) to $whoLoan','$ipaddress','$by',NOW())");
      return 1;
    }
  }

  public function sendNewAsset($data){
    $assetType = mysql_real_escape_string(trim($data['assetType']));
    $assetName = mysql_real_escape_string(trim($data['assetName']));
    $assetSerial = mysql_real_escape_string(trim($data['assetSerial']));
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();

    $sql = mysql_query("INSERT INTO tbl_rental(type,item,serialNo,dateCreated,byWho,status)
                        VALUES('$assetType','$assetName','$assetSerial',NOW(),'$by','available')");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('','','Loan','New Available: $assetName($assetSerial)','$ipaddress','$by',NOW())");
      return 1;
    }
  }

  public function sendEditAsset($data){
    $did = mysql_real_escape_string(trim($data['did']));
    $assetType = mysql_real_escape_string(trim($data['assetType']));
    $assetName = mysql_real_escape_string(trim($data['assetName']));
    $assetSerial = mysql_real_escape_string(trim($data['assetSerial']));
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();

    $sql = mysql_query("UPDATE tbl_rental SET type = '$assetType', item = '$assetName', serialNo = '$assetSerial' WHERE did = '$did'");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('','','Loan','Edit Available: $assetName($assetSerial)','$ipaddress','$by',NOW())");
      return 1;
    }
  }

  public function sendDelAsset($data){
    $did = mysql_real_escape_string(trim($data['did']));
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();
    $assetName = $this->getTbl_rental($did,'item');
    $assetSerial = $this->getTbl_rental($did,'serialNo');

    $sql = mysql_query("DELETE FROM tbl_rental WHERE did = '$did'");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('','','Loan','Delete Available: $assetName($assetSerial)','$ipaddress','$by',NOW())");
      return 1;
    }
  }

  public function sendLoanAsset($data){
    $did = mysql_real_escape_string(trim($data['did']));
    $whoLoan = mysql_real_escape_string(trim($data['whoLoan']));
    $startLoan = mysql_real_escape_string(trim($data['startLoan']));
    $locationLoan = mysql_real_escape_string(trim($data['locationLoan']));
    $assetName = $this->getTbl_rental($did,'item');
    $assetSerial = $this->getTbl_rental($did,'serialNo');
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();

    $sql = mysql_query("UPDATE tbl_rental SET who = '$whoLoan', startDate = '$startLoan', location = '$locationLoan', status = 'loan' WHERE did = '$did'");
    if(!$sql){
      return mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('','','Loan','Loan Asset: $assetName($assetSerial)','$ipaddress','$by',NOW())");
      return 1;
    }

  }

  public function countLoanlist($data){
    $sql = mysql_query("SELECT did FROM tbl_rental WHERE status = '$data'");
    return mysql_num_rows($sql);
  }

  public function getTbl_rental($did,$col){
    $sql = mysql_query("SELECT $col FROM tbl_rental WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row[$col];
  }

  // -----------------------------------------------------------------------------------------------------
  public function modalEdit($did){
  ?>
  <div class="modal fade" id="edit<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
  		<form class="" action="" method="post">
        <div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="exampleModalLabel">
    					Edit Available Asset
    				</h5>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">
    						&times;
    					</span>
    				</button>
    			</div>
    			<div class="modal-body">
            <div class="form-group m-form__group row">
  						<label for="assetType" class="col-4 col-form-label">TYPE</label>
  						<div class="col-6">
                <select class="form-control m-input m-input--air" id="assetType" name="assetType" required>
    							<option value="">Choose</option>
                  <?php $sqlOpt = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '94'"); if(mysql_num_rows($sqlOpt)){ while($rowOpt = mysql_fetch_assoc($sqlOpt)){ ?>
                    <option value="<?php echo $rowOpt['dataValue'] ?>" <?php if($rowOpt['dataValue'] == $this->getTbl_rental($did,'type')){print 'selected';} ?>><?php echo $rowOpt['dataValue'] ?></option>
                  <?php } } ?>
    						</select>
  						</div>
  					</div>
            <div class="form-group m-form__group row">
  						<label for="assetName" class="col-4 col-form-label">ASSET NAME</label>
  						<div class="col-8">
  							<input class="form-control m-input" type="text" placeholder="Assets Code / Name" value="<?php echo $this->getTbl_rental($did,'item') ?>" id="assetName" name="assetName"  required>
  						</div>
  					</div>
            <div class="form-group m-form__group row">
  						<label for="serialNo" class="col-4 col-form-label">SERIAL NO.</label>
  						<div class="col-8">
                <input class="form-control m-input" type="text" placeholder="Serial No." value="<?php echo $this->getTbl_rental($did,'serialNo') ?>" id="serialNo" name="assetSerial" required>
  							<input class="form-control m-input" type="hidden" value="<?php echo $did ?>" id="did" name="did" required>
  						</div>
  					</div>
    			</div>
    			<div class="modal-footer">
    				<button type="button" class="btn btn-secondary" data-dismiss="modal">
    					Close
    				</button>
    				<button type="submit" name="btneditable" class="btn btn-success">
    					Edit
    				</button>
    			</div>
    		</div>
      </form>
  	</div>
  </div>
  <?php
  }

  public function modalDel($did){ ?>
  <div class="modal fade" id="del<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">
						Delete Available Asset
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">
							&times;
						</span>
					</button>
				</div>
				<div class="modal-body">
					<p>You wish to delete this ? <?php echo $did ?></p>
				</div>
				<div class="modal-footer">
					<form class="" action="" method="post">
            <input type="hidden" name="did" value="<?php echo $did ?>">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
  						Close
  					</button>
  					<button type="submit" name="btnDelable" class="btn btn-danger">
  						Delete
  					</button>
          </form>
				</div>
			</div>
		</div>
	</div>
  <?php }

  public function modalLoan($did){ ?>
    <div class="modal fade" id="loan<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<form class="" action="" method="post">
          <div class="modal-content">
      			<div class="modal-header">
      				<h5 class="modal-title" id="exampleModalLabel">
      					Loan Available Asset
      				</h5>
      				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      					<span aria-hidden="true">
      						&times;
      					</span>
      				</button>
      			</div>
      			<div class="modal-body">
              <div class="form-group m-form__group row">
    						<label for="whoLoan" class="col-4 col-form-label">Person Loan</label>
    						<div class="col-8">
    							<input class="form-control m-input" type="text" placeholder="Person Name" id="whoLoan" name="whoLoan" required>
                  <input class="form-control m-input" type="hidden" name="did" value="<?php echo $did; ?>" required>
    						</div>
    					</div>
              <div class="form-group m-form__group row">
    						<label for="startLoan" class="col-4 col-form-label">Start Loan</label>
    						<div class="col-8">
    							<input class="form-control m-input" type="date" placeholder="" id="startLoan" name="startLoan" required>
    						</div>
    					</div>
              <div class="form-group m-form__group row">
    						<label for="Location" class="col-4 col-form-label">Location</label>
    						<div class="col-8">
                  <select class="form-control m-input m-input--air" id="Location" name="locationLoan" required>
      							<option>Choose</option>
                    <?php $sqlOpt = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '1'"); if(mysql_num_rows($sqlOpt)){ while($rowOpt = mysql_fetch_assoc($sqlOpt)){ ?>
                      <option value="<?php echo $rowOpt['dataValue'] ?>"><?php echo $rowOpt['dataValue'] ?></option>
                    <?php } } ?>
      						</select>
    						</div>
    					</div>
      			</div>
      			<div class="modal-footer">
      				<button type="button" class="btn btn-secondary" data-dismiss="modal">
      					Close
      				</button>
      				<button type="submit" name="btnloanable" class="btn btn-success">
      					Edit
      				</button>
      			</div>
      		</div>
        </form>
    	</div>
    </div>
  <?php }
}
?>
