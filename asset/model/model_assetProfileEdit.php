<?php
class profileEdit extends General{

  public function getCategoryValue($ref,$type){
    $dataCode = $this->getDataCode($ref);
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE asKey = '$type' AND dataCode = '$dataCode'");
    $row = mysql_fetch_assoc($sql);
    return $row['asValue'];
  }

  public function getDataCode($ref){
    $sql = mysql_query("SELECT dataCode FROM tbl_master WHERE asValue = '$ref'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataCode'];
  }

  public function getValueCategory($value){
    $sql = mysql_query("SELECT dataValueWrite FROM tbl_category WHERE did = '$value'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataValueWrite'];
  }

  public function getEditForm($childValue,$ref){
    // echo $childValue.'<br>';
    $sql1 = mysql_query("SELECT dataRefFieldset FROM tbl_form WHERE dataRefCategory = '$childValue'");
    if (mysql_num_rows($sql1)) {
      while($row1 = mysql_fetch_assoc($sql1)){
        $dataRefFieldset = $row1['dataRefFieldset'];
        // echo $dataRefFieldset.'<br>';
        $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE did = '$dataRefFieldset'");
        $row = mysql_fetch_assoc($sql);
        $type = $row['dataValueType'];
        $placeholder = $row['dataValuePlaceholder'];
        $name = $row['dataValueName'];
        $valueValue = $this->getValueMaster($name,$ref);
        $label = $row['dataValueLabel'];
        $option = $row['dataValueDropdown'];
        $required = $row['dataValueRequired'];
        if($required == 'on'){
          $required = "<span style='color:red;'>*</span>";
          $req = "required";
        }else{
          $required = "";
          $req = "";
        }
        if($type == 'text' || $type == 'number' || $type == 'date'){ ?>
          <div class="form-group m-form__group row">
            <label class="col-form-label col-lg-3 col-sm-12">
              <?php echo $label; ?> <?php echo $required; ?>
            </label>
            <div class="col-lg-4 col-md-9 col-sm-12">
              <input type="<?php echo $type ?>" class="form-control m-input m-input--solid" name="<?php echo $name ?>" value="<?php echo $valueValue ?>" <?php echo $req ?>>
            </div>
          </div>
        <?php }elseif($type == 'select'){ ?>
          <div class="form-group m-form__group row">
            <label class="col-form-label col-lg-3 col-sm-12">
              <?php echo $label; ?> <?php echo $required; ?>
            </label>
            <div class="col-lg-4 col-md-9 col-sm-12">
              <select class="form-control m-input m-input--solid" name="<?php echo $name ?>" <?php echo $req ?>>
                  <option value=""> Select <?php echo $valueValue ?></option>
                  <?php $sqlchild = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '$option'"); if (mysql_num_rows($sqlchild)) { while ($rowchild = mysql_fetch_assoc($sqlchild)) { ?>
                    <option <?php if($valueValue == $rowchild['dataValue']){ print 'selected'; } ?> value="<?php echo $rowchild['dataValue']; ?>" >
                      <?php echo $rowchild['dataValue']; ?>
                    </option>
                  <?php } } ?>
              </select>
            </div>
          </div>
        <?php }elseif ($type == 'checkbox') { ?>
          <div class="form-group m-form__group row">
            <label class="col-form-label col-lg-3 col-sm-12">
              <?php echo $label; ?> <?php echo $required; ?>
            </label>
            <div class="col-lg-4 col-md-9 col-sm-12">
              <label class="m-checkbox">
                <input type="<?php echo $type ?>" name="<?php echo $name ?>" <?php if($valueValue == 'on'){ print 'checked'; } ?>>
                <span></span>
              </label>
            </div>
          </div>
        <?php }elseif($type == 'file browser'){ ?>
          <!-- <div class="form-group m-form__group row">
            <label class="col-form-label col-lg-3 col-sm-12">
              <?php echo $label; ?> <?php echo $required; ?>
            </label>
            <div class="col-lg-4 col-md-9 col-sm-12">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="<?php echo $name ?>" name="<?php echo $name ?>" <?php echo $req ?>>
                <label class="custom-file-label" for="<?php echo $name ?>">
                  Choose file
                </label>
              </div>
            </div>
          </div> -->
        <?php }
      }
    }
  }

  public function getValueMaster($name,$ref){
    $dataCode = $this->getDataCode($ref);
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = '$name'");
    $row = mysql_fetch_assoc($sql);
    return $row['asValue'];
  }

  public function saveAsset($data,$ref){
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();
    $dataCode = $this->getDataCode($ref);

    foreach ($data as $key => $value) {
      $key = mysql_real_escape_string(trim($key));
      $value = mysql_real_escape_string(trim($value));
      $getDidFieldset = $this->getFieldsetDid($key);
      $getDidServices = $this->getCategoryDid($key);
      $check = $this->chckNameExist($key,$dataCode);
      if ($check == 1) {
        $task = $this->updateData($value,$key,$dataCode);
        $result = $task;
      }else{
        $task = $this->insertData($key,$value,$dataCode,$getDidFieldset,$getDidServices);
        $result = $task;
      }
    }
    // log report
    $getDidServices = $this->getServicesDid($dataCode);
    $serviceCode = $this->getServiceCode($dataCode);
    $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                           VALUES('$getDidServices','$dataCode','assets','Assets Update [EDIT]: $serviceCode','$ipaddress','$by',NOW())");

    return $result;
  }

  public function getFieldsetDid($key){
    $sql = mysql_query("SELECT did FROM tbl_fieldsetunit WHERE dataValueName = '$key'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataValueName'];
  }

  public function getCategoryDid($key){
    $sql = mysql_query("SELECT dataRefCategory FROM tbl_fieldsetunit WHERE dataValueName = '$key'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataRefCategory'];
  }

  public function chckNameExist($key,$dataCode){
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = '$key'");
    $result = mysql_num_rows($sql);
    return $result;
  }

  public function updateData($value,$key,$dataCode){
    $by = $this->byWho;
    $sql = mysql_query("UPDATE tbl_master SET asValue = '$value', dateModified = NOW(), byWho = '$by' WHERE asKey = '$key' AND dataCode = '$dataCode'");
    if(!$sql){
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function insertData($key,$value,$dataCode,$getDidFieldset,$getDidServices){
    $by = $this->byWho;
    $sql = mysql_query("INSERT INTO tbl_master(asKey,asValue,dataCode,dataRefFieldset,dataRefServices,dateCreated,dateModified,byWho,active)VALUES('$key','$value','$dataCode','$getDidFieldset','$getDidServices',NOW(),NOW(),'$by','Yes')");
    if(!$sql){
      return mysql_error();
    }else{
      return 1;
    }
  }

  public function getServiceCode($data){
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$data' AND asKey = 'code'");
    $row = mysql_fetch_assoc($sql);
    return $row['asValue'];
  }

  public function getServicesDid($data){
    $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE dataCode = '$data'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataRefServices'];
  }



}
?>
