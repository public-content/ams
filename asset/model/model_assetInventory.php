<?php
  class assetInventory extends General{

    public function getName($did){
      $sql = mysql_query("SELECT name FROM tbl_user WHERE did='$did'");
      $row = mysql_fetch_assoc($sql);
      return $row['name'];
    }

    public function saveThis($data,$type){
      if ($type == "parent") {
        $by = $this->byWho;
        $value = mysql_real_escape_string(trim($data['category']));
        $check = $this->checkExist($value,'dataValue','tbl_inventory');
        if ($check == 0) {
          $sql = mysql_query("INSERT INTO tbl_inventory(dataType,dataValue,dateCreated,dateModified,byWho,active)
                              VALUES('MAIN','$value',NOW(),NOW(),'$by','Yes')");
          if (!$sql) {
            $result = mysql_error();
          }else{
            $result = 1;
            mysql_query("INSERT INTO rpt_inventory(dataType,dataValueDesc,dataValuePersonRequest,dateCreated)
                                VALUES('MAIN','New parent <b>$value</b> been registered','$by',NOW())");
          }
          return $result;
        }else{
          $result = "Value Exist, please try another name";
        }
        return $result;
      }elseif($type == "child"){
        $by = $this->byWho;
        $value = mysql_real_escape_string(trim($data['sub-category']));
        $ref = mysql_real_escape_string(trim($data['category']));
        $check = $this->checkExist($value,'dataValue','tbl_inventory');
        if ($check == 0) {
          $sql = mysql_query("INSERT INTO tbl_inventory(dataType,dataValue,dataRef,dateCreated,dateModified,byWho,active)
                              VALUES('CHILD','$value','$ref',NOW(),NOW(),'$by','Yes')");
          if (!$sql) {
            $result = mysql_error();
          }else{
            $result = 1;
            mysql_query("INSERT INTO rpt_inventory(dataType,dataValueDesc,dataValuePersonRequest,dateCreated)
                                VALUES('ITEM','New child <b>$value</b> been registered','$by',NOW())");
          }
          return $result;
        }else{
          $result = "Value Exist, please try another name";
        }
        return $result;
      }

    }

    public function deleteThis($data){
      $by = $this->byWho;
      $did = mysql_real_escape_string(trim($data['did']));
      $name = mysql_real_escape_string(trim($data['name']));
      $sql = mysql_query("DELETE FROM tbl_inventory WHERE did = '$did'");
      if (!$sql) {
        return mysql_error();
      }else{
        mysql_query("INSERT INTO rpt_inventory(dataType,dataValueDesc,dataValuePersonRequest,dateCreated)
                            VALUES('DELETE',' <b>$name</b> has been deleted','$by',NOW())");
        return 1;
      }
    }

    public function addingStock($data){
      $by = $this->byWho;
      $newitem = mysql_real_escape_string(trim($data['newitem']));
      $totalrm = mysql_real_escape_string(trim($data['totalrm']));
      $vendor = mysql_real_escape_string(trim($data['vendor']));
      $did = mysql_real_escape_string(trim($data['did']));

      $sql = mysql_query("SELECT * FROM tbl_inventory WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      $oldrm = $row['totalrm'];
      $oldstock = $row['stock'];
      $newrm = $oldrm + $totalrm;
      $newstock = $oldstock + $newitem;
      $name = $this->findThis($did,'did','dataValue','tbl_inventory');
      $dataNewItem = "<span><b>".$newitem."</b></span>";

      $sql2 = mysql_query("UPDATE tbl_inventory SET stock = '$newstock', totalrm = '$newrm', dateModified = NOW() WHERE did = '$did'");
      if (!$sql2) {
        $result = mysql_error();
      }else{
        $sql3 = mysql_query("INSERT INTO rpt_inventory(dataType,dataValueDesc,dataValueQtyNew,dataValueRm,dataValueVendor,dataValuePersonRequest,dateCreated,dataRef)
                             VALUES('ADDSTOCK','$dataNewItem new stock has been add under <b>$name</b>','$newitem','$totalrm','$vendor','$by',NOW(),'$did')");
        if (!$sql3) { $result = mysql_error(); }else{ $result = 1; }
      }
      return $result;

    }

    public function stockOut($data){
      $stockout = mysql_real_escape_string(trim($data['stockout']));
      $reason = mysql_real_escape_string(trim($data['reason']));
      $personr = mysql_real_escape_string(trim($data['personr']));
      $persont = mysql_real_escape_string(trim($data['persont']));
      $did = mysql_real_escape_string(trim($data['did']));

      $sql = mysql_query("SELECT * FROM tbl_inventory WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      $oldstockout = $row['stockout'];
      $newStockOut = $oldstockout + $stockout;
      $name = $this->findThis($did,'did','dataValue','tbl_inventory');

      $stockleft = $this->stockLeft($did);
      if ($stockleft >= $stockout) {
        $sql2 = mysql_query("UPDATE tbl_inventory SET stockout = '$newStockOut', dateModified = NOW() WHERE did = '$did'");
        if (!$sql2) {
          $result = 0;
        }else{
          $sql3 = mysql_query("INSERT INTO rpt_inventory(dataType,dataValueDesc,dataValuePersonRequest,dataValueQtyOut,dataValueReason,dataValuePersonTakeOut,dateCreated,dataRef)
                               VALUES('TAKEOUT','<b>$stockout</b> stock has been takeout from <b>$name</b> for $reason request by $personr','$persont','$stockout','$reason','$personr',NOW(),'$did')");
          if (!$sql3) { $result = 0; }else{ $result = 1; }
        }
        return $result;
      }else{
        return 'Only '.$stockleft.' can be taking out from '.$name;
      }


    }

    public function checkExist($value,$column,$table){
      $sql = mysql_query("SELECT * FROM $table WHERE $column = '$value'");
      $result = mysql_num_rows($sql);
      return $result;
    }

    public function findThis($value,$ref,$column,$table){
      $sql = mysql_query("SELECT $column FROM $table WHERE $ref = '$value'");
      $row = mysql_fetch_assoc($sql);
      return $row[$column];
    }

    public function stockLeft($did){
      $sql = mysql_query("SELECT * FROM tbl_inventory WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      $result = $row['stock'] - $row['stockout'];
      return $result;
    }

  }
?>
