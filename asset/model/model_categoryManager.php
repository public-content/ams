<?php
class categoryManager extends General{
  public function getAllValue($s){

    $by             = $this->byWho;
    $valueService   = mysql_real_escape_string(trim($s['valueService']));
    $tagChar        = mysql_real_escape_string(trim($s['tagChar']));
    $imageFieldset  = mysql_real_escape_string(trim($s['imageFieldset']));
    $vendorFieldset = mysql_real_escape_string(trim($s['vendorFieldset']));
    $assetFieldset  = mysql_real_escape_string(trim($s['assetFieldset']));
    $toolEdit       = mysql_real_escape_string(trim($s['toolEdit']));
    $toolCondemn    = mysql_real_escape_string(trim($s['toolCondemn']));
    $toolDelete     = mysql_real_escape_string(trim($s['toolDelete']));
    $tooltransfer   = mysql_real_escape_string(trim($s['tooltransfer']));
    $toolreplace    = mysql_real_escape_string(trim($s['toolreplace']));
    $toolrepair     = mysql_real_escape_string(trim($s['toolrepair']));

    $valueServiceRead = str_replace(" ","",$valueService);

    $checkExist = $this->checkCategory($valueService);
    if ($checkExist == 0) {
      $saveServices = $this->saveServices($valueService,$tagChar,$imageFieldset,$vendorFieldset,$assetFieldset,$toolEdit,$toolCondemn,$toolDelete,$tooltransfer,$toolreplace,$valueServiceRead,$toolrepair);
      if ($saveServices == 1) {
        $result = 1;
      }else{
        $result = $saveServices;
      }
    }else{
      $result = "This Category Exist";
    }
    return $result;
  }

  public function checkCategory($valueService){
    $sql = mysql_query("SELECT * FROM tbl_category WHERE dataValueRead = '$valueService' OR dataValueWrite = '$valueService'");
    $result = mysql_num_rows($sql);
    return $result;
  }

  public function saveServices($valueService,$tagChar,$imageFieldset,$vendorFieldset,$assetFieldset,$toolEdit,$toolCondemn,$toolDelete,$tooltransfer,$toolreplace,$valueServiceRead,$toolrepair){
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();
    $sql = mysql_query("INSERT INTO tbl_category(dataType,dataValueWrite,dataValueRead,dataTag,runNumber,qrCode,toolEdit,toolSwap,toolTransfer,toolRepair,toolCondemn,toolDelete,dateCreated,dateModified,byWho,active)
                        VALUES('Service','$valueService','$valueServiceRead','$tagChar','0000','on','$toolEdit','$toolreplace','$tooltransfer','$toolrepair','$toolCondemn','$toolDelete',NOW(),NOW(),'$by','Yes')");
    if (!$sql) {
      $result = mysql_error();
    }else{
      $servicesDid = $this->getDid($valueService);
      // log report
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$servicesDid','services','New Services has been created: $valueService','$ipaddress','$by',NOW())");

      $createField = $this->createField($imageFieldset,$vendorFieldset,$assetFieldset,$servicesDid);
      if ($createField == 1) {
        $result = 1;
      }else{
          $result = $createField;
      }
      mysql_query("INSERT INTO tbl_fieldsetunit(dataType,dataValueLabel,dataValueName,dataValueType,dataValueRequired,dataRefCategory,dateCreated,dateModified,byWho,active)
                          VALUES('CODE','$tagChar','code','text','on','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','Name','name','text','on','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','Brand','brand','text','on','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','Serial No','serialNo','text','on','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','Price','price','text','on','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','Location','location','select','on','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','User','user','text','','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('FIX','Date Received','dateReceived','date','on','$servicesDid',NOW(),NOW(),'$by','Yes')");
    }
    return $result;
  }

  public function createField($imageFieldset,$vendorFieldset,$assetFieldset,$servicesDid){
    $by = $this->byWho;
    if ($imageFieldset == 'on') {
      $sql = mysql_query("INSERT INTO tbl_fieldsetunit(dataType,dataVAlueLabel,dataValueName,dataValueType,dataRefCategory,dateCreated,dateModified,byWho,active)
                          VALUES('IMAGE','Image Asset 1','imgass1','file browser','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('IMAGE','Image Asset 2','imgass2','file browser','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('IMAGE','Image Asset 3','imgass3','file browser','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('IMAGE','Image Asset 4','imgass4','file browser','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('IMAGE','Image Asset 5','imgass5','file browser','$servicesDid',NOW(),NOW(),'$by','Yes')");
      if (!$sql) {
        $result = mysql_error();
      }else {
        $result = 1;
      }
    }else{
      $result = 1;
    }
    if ($vendorFieldset == 'on') {
      $sql2 = mysql_query("INSERT INTO tbl_fieldsetunit(dataType,dataVAlueLabel,dataValueName,dataValueType,dataRefCategory,dateCreated,dateModified,byWho,active)
                          VALUES('VENDOR','Vendor Name','vendorName','text','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('VENDOR','Date Warranty','dateWarranty','date','$servicesDid',NOW(),NOW(),'$by','Yes'),
                          ('VENDOR','Invoice Number','invonceNo','text','$servicesDid',NOW(),NOW(),'$by','Yes')");
      if (!$sql2) {
        $result = mysql_error();
      }else {
        $result = 1;
      }
    }else{
      $result = 1;
    }
    if ($assetFieldset == 'on') {
      $sql3 = mysql_query("INSERT INTO tbl_fieldsetunit(dataType,dataVAlueLabel,dataValueName,dataValueType,dataRefCategory,dateCreated,dateModified,byWho,active)
                          VALUES('TAGGING','Asset Tag Unit','assetTag','text','$servicesDid',NOW(),NOW(),'$by','Yes')");
      if (!$sql3) {
        $result = mysql_error();
      }else {
        $result = 1;
      }
    }else{
      $result = 1;
    }

    return $result;
  }

  public function getDid($valueService){
    $sql = mysql_query("SELECT did FROM tbl_category WHERE dataValueWrite = '$valueService'");
    $row = mysql_fetch_assoc($sql);
    return $row['did'];
  }

  public function pushMainValue($s,$type){
    $by = $this->byWho;
    $ipaddress = $this->getUserIP();
    if ($type == 'Main') {
      $valueMain = mysql_real_escape_string(trim($s['valueMain']));
      $dataRefCategory = mysql_real_escape_string(trim($s['dataRefCategory']));

      $sql = mysql_query("INSERT INTO tbl_category(dataType,dataValueWrite,dataValueRead,dataRefService,dateCreated,dateModified,byWho,active)
                          VALUES('Main','$valueMain','$valueMain','$dataRefCategory',NOW(),NOW(),'$by','Yes')");
      if (!$sql) {
        $result = mysql_error();
      }else {
        $result = 1;
        // log repor
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$dataRefCategory','services','New services Main Category has been created: $valueMain','$ipaddress','$by',NOW())");
      }
    }elseif($type == 'Sub'){
      $valueMain = mysql_real_escape_string(trim($s['valueSub']));
      $dataRefCategory = mysql_real_escape_string(trim($s['dataRefService']));
      $dataRefMain = mysql_real_escape_string(trim($s['dataRefMain']));

      $sql = mysql_query("INSERT INTO tbl_category(dataType,dataValueWrite,dataValueRead,dataRefService,dataRefMain,dateCreated,dateModified,byWho,active)
                          VALUES('Child','$valueMain','$valueMain','$dataRefCategory','$dataRefMain',NOW(),NOW(),'$by','Yes')");
      if (!$sql) {
        $result = mysql_error();
      }else {
        $getLocationInput = $this->getInputDid($dataRefCategory,'location');
        $getPriceInput = $this->getInputDid($dataRefCategory,'price');
        $getUserInput = $this->getInputDid($dataRefCategory,'user');
        $getNameInput = $this->getInputDid($dataRefCategory,'name');
        $getBrandInput = $this->getInputDid($dataRefCategory,'brand');
        $getSerialInput = $this->getInputDid($dataRefCategory,'serialNo');
        $getDateReInput = $this->getInputDid($dataRefCategory,'dateReceived');
        $getSubDid = $this->getDid($valueMain);
        $run = mysql_query("INSERT INTO tbl_form(dataRefCategory,dataRefService,dataRefFieldset,dateCreated,dateModified)
                            VALUES('$getSubDid','$dataRefCategory','$getNameInput',NOW(),NOW()),
                            ('$getSubDid','$dataRefCategory','$getBrandInput',NOW(),NOW()),
                            ('$getSubDid','$dataRefCategory','$getSerialInput',NOW(),NOW()'),
                            ('$getSubDid','$dataRefCategory','$getPriceInput',NOW(),NOW()),
                            ('$getSubDid','$dataRefCategory','$getDateReInput',NOW(),NOW()),
                            ('$getSubDid','$dataRefCategory','$getLocationInput',NOW(),NOW()),
                            ('$getSubDid','$dataRefCategory','$getUserInput',NOW(),NOW())");
        if(!$run){
          $result = mysql_error();
        }else{
          $result = 1;
          // log report
          $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                                 VALUES('$dataRefCategory','services','New services Sub Category has been created: $valueMain','$ipaddress','$by',NOW())");
        }

      }
    }
    return $result;
  }

  public function pushEdit($s,$type){
    $ipaddress = $this->getUserIP();
    $by = $this->byWho;
    if ($type == 'Main') {
      $valueMain = mysql_real_escape_string(trim($s['valueMain']));
      $didMain = mysql_real_escape_string(trim($s['didMain']));

      $sql = mysql_query("UPDATE tbl_category SET dataValueWrite = '$valueMain' , dataValueRead = '$valueMain' WHERE did = '$didMain'");
      if (!$sql) {
        $result = mysql_error();
      }else{
        $result = 1;
        $dataRefCategory = $this->getServiceDid($didMain);
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$dataRefCategory','services','Services Update: $valueMain','$ipaddress','$by',NOW())");
      }
    }elseif ($type == 'Child') {
      $valueSub = mysql_real_escape_string(trim($s['valueSub']));
      $didChild = mysql_real_escape_string(trim($s['didChild']));

      $sql = mysql_query("UPDATE tbl_category SET dataValueWrite = '$valueSub' , dataValueRead = '$valueSub' WHERE did = '$didChild'");
      if (!$sql) {
        $result = mysql_error();
      }else{
        $result = 1;
        $dataRefCategory = $this->getServiceDid($didChild);
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$dataRefCategory','services','Services Update: $valueSub','$ipaddress','$by',NOW())");
      }
    }
    return $result;
  }

  public function pushDel($s,$type){
    $ipaddress = $this->getUserIP();
    $by = $this->byWho;
    if ($type == 'Service') {
      $did = mysql_real_escape_string(trim($s['did']));
      $valueData = $this->getValueData($did);
      $sql = mysql_query("DELETE FROM tbl_category WHERE did = '$did' OR dataRefService = '$did'");
      if (!$sql) {
        $return = mysql_error();
      }else {
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$did','services','Services Deleted: $valueData','$ipaddress','$by',NOW())");
        $result = 1;
      }
    }elseif ($type == 'Main') {
      $did = mysql_real_escape_string(trim($s['didMain']));
      $valueData = $this->getValueData($did);
      $sql = mysql_query("DELETE FROM tbl_category WHERE did = '$did' OR dataRefMain = '$did'");
      if (!$sql) {
        $return = mysql_error();
      }else {
        $dataRefCategory = $this->getServiceDid($did);
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$dataRefCategory','services','Services Deleted: $valueData','$ipaddress','$by',NOW())");
        $result = 1;
      }
    }elseif ($type == 'Child') {
      $did = mysql_real_escape_string(trim($s['didChild']));
      $valueData = $this->getValueData($did);
      $sql = mysql_query("DELETE FROM tbl_category WHERE did = '$did'");
      if (!$sql) {
        $return = mysql_error();
      }else {
        $dataRefCategory = $this->getServiceDid($did);
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$dataRefCategory','services','Services Deleted: $valueData','$ipaddress','$by',NOW())");
        $result = 1;
      }
    }
    return $result;
  }

  public function getInputDid($ref,$value){
    $sql = mysql_query("SELECT did FROM tbl_fieldsetunit WHERE dataValueName = '$value' AND dataRefCategory = '$ref'");
    $row = mysql_fetch_assoc($sql);
    return $row['did'];
  }

  public function getServiceDid($did){
    $sql = mysql_query("SELECT dataRefService FROM tbl_category WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataRefService'];
  }

  public function getValueData($did){
    $sql = mysql_query("SELECT dataValueWrite FROM tbl_category WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataValueWrite'];
  }
}
?>
