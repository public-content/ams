<?php
  class assetProfile extends General{
    public function getDatacode($r){
      $sql = mysql_query("SELECT dataCode FROM tbl_master WHERE asValue = '$r' ");
      $row = mysql_fetch_assoc($sql);
      return $row['dataCode'];
    }

    public function getValue($dataCode,$need){
      $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = '$need'");
      $row = mysql_fetch_assoc($sql);
      return $row['asValue'];
    }

    public function getValueCondemn($dataCode,$need){
      $sql = mysql_query("SELECT active FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = 'code'");
      $row = mysql_fetch_assoc($sql);
      $result = $row['active'];
      if($result == 'Condemn'){
        return "Condemn";
      }elseif($result == "In List"){
        return "In List";
      }elseif($result == "Repair"){
        return "Repair";
      }else{
        return "Normal";
      }
    }

    public function getValueCategory($mainValue){
      $sql = mysql_query("SELECT dataValueWrite FROM tbl_category WHERE did = '$mainValue'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataValueWrite'];
    }

    public function getQrCode($st){
      $result = "https://192.168.43.115/ams/thiscode.php?st=$st";
      return $result;
    }
    public function checkqrcode($r){
      $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE asValue = '$r'");
      $row = mysql_fetch_assoc($sql);
      $Category = $row['dataRefServices'];
      $sqlqr = mysql_query("SELECT qrCode FROM tbl_category WHERE did = '$Category'");
      $rowqr = mysql_fetch_assoc($sqlqr);
      $qrCode = $rowqr['qrCode'];
      if ($qrCode == 'on') {
        $secureValue = securestring('encrypt',$r);
        return $secureValue;
      }
    }

    public function getTool($type,$didServices){
      $sql = mysql_query("SELECT $type FROM tbl_role WHERE did = '$didServices'");
      $row = mysql_fetch_assoc($sql);
      return $row[$type];
    }

    public function getDidServices($r){
      $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE asValue = '$r'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataRefServices'];
    }

    public function checkImage($r){
      $dataCode = $this->getDatacode($r);
      $sql = mysql_query("SELECT * FROM tbl_master WHERE dataCode = '$dataCode' AND asValue LIKE '%images/file_assets%'");
      // $row = mysql_fetch_assoc($sql);
      // return $row['asValue'];
      $result = mysql_num_rows($sql);
      return $result;
    }

    public function getServices($r){
      $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE asValue = '$r'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataRefServices'];
    }

    public function delAsset($code){
      $getDataCode = $this->getDatacode($code['code']);
      $getDidServices = $this->getServicesdid($getDataCode);
      $assetcode = $this->getAssetCode($getDataCode);
      $ipaddress = $this->getUserIP();
      $sendBy = $this->byWho;
      $sql = mysql_query("UPDATE tbl_master SET active = 'Deleted' WHERE dataCode = '$getDataCode'");
      if(!$sql){
        return mysql_error();
      }else{
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$getDataCode','assets','Assets Terminate [DELETE]: $assetcode','$ipaddress','$sendBy',NOW())");
        return $sql;
      }
    }

    public function valueOption($did){
      $sql = mysql_query("SELECT dataValue FROm tbl_optionunit WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataValue'];
    }

    public function toCondemn($data){
      $sendBy = $this->byWho;
      $ipaddress = $this->getUserIP();
      $dataCode = mysql_real_escape_string(trim($data['dataCode']));
      $getDidServices = $this->getServicesdid($dataCode);
      $assetcode = $this->getAssetCode($dataCode);
      $sql = mysql_query("UPDATE tbl_master SET active = 'In List' WHERE dataCode = '$dataCode'");
      if(!$sql){
        return mysql_error();
      }else{
        // save to tbl_condemn
        $condemn = mysql_query("INSERT INTO tbl_condemn(dataType,dataValue,dataWhoRequest,dataStatus,dateCreated,dateModified,dataActive)
                                VALUES('Condemn','$dataCode','$sendBy','New Request',NOW(),NOW(),'Yes')");
        if(!$condemn){
          echo mysql_error();
        }
        // log report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$dataCode','assets','Assets Condemn [In-List]: $assetcode','$ipaddress','$sendBy',NOW())");
        return 1;
      }
    }

    public function sendRepair($data){
      $assetcode = mysql_real_escape_string(trim($data['assetcode']));
      $datacode = mysql_real_escape_string(trim($data['datacode']));
      $repairVendor = mysql_real_escape_string(trim($data['repairVendor']));
      $reasonRepair = mysql_real_escape_string(trim($data['reasonRepair']));
      $getDidServices = $this->getDidServices($datacode);
      $assetcode = $this->getAssetCode($datacode);
      $ipaddress = $this->getUserIP();
      $sendBy = $this->byWho;

      $sql = mysql_query("INSERT INTO tbl_assetrepair(dataCode,dataVendor,dataReason,dataSendBy,dateSend)
                          VALUES('$datacode','$repairVendor','$reasonRepair','$sendBy',NOW())");
      if (!$sql) {
        return mysql_error();
      }else{
        mysql_query("UPDATE tbl_master SET active = 'Repair' WHERE dataCode = '$datacode'");
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$datacode','assets','Assets Update [SEND/REPAIR]: $assetcode','$ipaddress','$sendBy',NOW())");
        return 1;
      }
    }

    public function receiveRepair($data){
      $price = mysql_real_escape_string(trim($data['price']));
      $datacode = mysql_real_escape_string(trim($data['datacode']));
      $did = mysql_real_escape_string(trim($data['did']));
      $getDidServices = $this->getDidServices($datacode);
      $assetcode = $this->getAssetCode($datacode);
      $ipaddress = $this->getUserIP();
      $sendBy = $this->byWho;
      $by = $this->byWho;
      $sql = mysql_query("UPDATE tbl_assetrepair SET dateReceived = NOW(), dataReceivedby = '$by', price = '$price' WHERE did = '$did'");
      if(!$sql){
        return mysql_error();
      }else{
        mysql_query("UPDATE tbl_master SET active = 'Yes' WHERE dataCode = '$datacode'");
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$datacode','assets','Assets Update [RECEIVE/REPAIR]: $assetcode','$ipaddress','$sendBy',NOW())");
        return 1;
      }
    }

    public function thisTransfer($data,$ref){
      $by = $this->byWho;
      $ipaddress = $this->getUserIP();
      $dataCode = $this->getDataCode($ref);
      $oldLocation = $this->getDataMaster($dataCode,'location');
      foreach ($data as $key => $value) {
        $key = mysql_real_escape_string(trim($key));
        $value = mysql_real_escape_string(trim($value));
        $getDidFieldset = $this->getFieldsetDid($key);
        $getDidServices = $this->getCategoryDid($key);
        $check = $this->chckNameExist($key,$dataCode);
        if ($check == 1) {
          $task = $this->updateData($value,$key,$dataCode);
          $result = $task;
        }else{
          $task = $this->insertData($key,$value,$dataCode,$getDidFieldset,$getDidServices);
          $result = $task;
        }
      }
      $newLocation = $this->getDataMaster($dataCode,'location');
      // log report
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataCode','assets','Assets Update [TRANSFER]: $ref || $oldLocation -> $newLocation','$ipaddress','$by',NOW())");
      return $result;
    }

    public function updateData($value,$key,$dataCode){
      $by = $this->byWho;
      $sql = mysql_query("UPDATE tbl_master SET asValue = '$value', dateModified = NOW(), byWho = '$by' WHERE asKey = '$key' AND dataCode = '$dataCode'");
      if(!$sql){
        return mysql_error();
      }else{
        return 1;
      }
    }

    public function insertData($key,$value,$dataCode,$getDidFieldset,$getDidServices){
      $by = $this->byWho;
      $sql = mysql_query("INSERT INTO tbl_master(asKey,asValue,dataCode,dataRefFieldset,dataRefServices,dateCreated,dateModified,byWho,active)VALUES('$key','$value','$dataCode','$getDidFieldset','$getDidServices',NOW(),NOW(),'$by','Yes')");
      if(!$sql){
        return mysql_error();
      }else{
        return 1;
      }
    }

    public function getFieldsetDid($key){
      $sql = mysql_query("SELECT did FROM tbl_fieldsetunit WHERE dataValueName = '$key'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataValueName'];
    }

    public function getCategoryDid($key){
      $sql = mysql_query("SELECT dataRefCategory FROM tbl_fieldsetunit WHERE dataValueName = '$key'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataRefCategory'];
    }

    public function chckNameExist($key,$dataCode){
      $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = '$key'");
      $result = mysql_num_rows($sql);
      return $result;
    }

    public function thisSwap($value){
      $ipaddress = $this->getUserIP();
      $by = $this->byWho;
      $oldCode = mysql_real_escape_string(trim($value['oldCode']));
      $oldLocation = mysql_real_escape_string(trim($value['oldLocation']));
      $oldUser = mysql_real_escape_string(trim($value['oldUser']));
      $newCode = mysql_real_escape_string(trim($value['newCode']));
      $newLocation = mysql_real_escape_string(trim($value['newLocation']));
      $newUser = mysql_real_escape_string(trim($value['newUser']));

      // return $oldCode.'<br>'.$oldLocation.'<br>'.$oldUser.'<br>'.$newCode.'<br>'.$newLocation.'<br>'.$newUser;
      // old to new
      $dataCodeOld = $this->getDatacode($oldCode);
         $sql1 = mysql_query("UPDATE tbl_master SET asValue = '$newLocation' WHERE dataCode = '$dataCodeOld' AND asKey = 'location'");
         mysql_query("UPDATE tbl_master SET asValue = '$newUser' WHERE dataCode = '$dataCodeOld' AND asKey = 'user'");
      if(!$sql1){
        return mysql_error();
      }else{
        // new to old
        $dataCodenew = $this->getDatacode($newCode);
        $sql2 = mysql_query("UPDATE tbl_master SET asValue = '$oldLocation' WHERE dataCode = '$dataCodenew' AND asKey = 'location' ");
        mysql_query("UPDATE tbl_master SET asValue = '$oldUser' WHERE dataCode = '$dataCodenew' AND asKey = 'user'");
        if(!$sql2){
          return mysql_error();
        }else{
          // log report
          $newUpper = strtoupper($newCode);
          $newDataCode = $this->getDataCode($newCode);
          $getDidServices = $this->getDidServices($newLocation);
          $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                                 VALUES('$getDidServices','$newDataCode','assets','Assets Update [SWAP]: $oldCode -> $newUpper','$ipaddress','$by',NOW())");

          return 1;
        }
      }
    }

    public function checkImageDid($r){
      $dataCode = $this->getDatacode($r);
      $sql = mysql_query("SELECT did,asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asValue LIKE '%images/file_assets%'");
      if(mysql_num_rows($sql)){
        while ($row = mysql_fetch_assoc($sql)) {
          $data[] = $row['did'];
        }
      }
      return $data;

    }

    public function getMaster($did,$column){
      $sql = mysql_query("SELECT $column FROM tbl_master WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      return $row[$column];
    }

    public function getLabel($did){
      $sql = mysql_query("SELECT dataValueLabel FROM tbl_fieldsetunit WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataValueLabel'];
    }

    public function getLabel2($name){
      $sql = mysql_query("SELECT dataValueLabel FROM tbl_fieldsetunit WHERE dataValueName = '$name'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataValueLabel'];
    }

    public function getDataMaster($dataCode,$column){
      $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = '$column'");
      $row = mysql_fetch_assoc($sql);
      return $row['asValue'];
    }

    public function getAssetCode($datacode){
      $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$datacode' AND asKey = 'code'");
      $row = mysql_fetch_assoc($sql);
      return $row['asValue'];
    }

    public function getServicesdid($dataCode){
      $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE dataCode = '$dataCode'");
      $row = mysql_fetch_assoc($sql);
      return $row['dataRefServices'];
    }

    // public function getDataCode($itcode){
    //   $sql = mysql_query("SELECT dataCode FROM tbl_master WHERE asValue = '$itcode'");
    //   $row = mysql_fetch_assoc($sql);
    //   return $row['dataCode'];
    // }

    public function pushReminder($data){
      $whom = mysql_real_escape_string(trim($data['whom']));
      $dataCode = mysql_real_escape_string(trim($data['dataCode']));
      $dateActual = mysql_real_escape_string(trim($data['dateActual']));
      $dateActual = date('d-m-Y', strtotime($dateActual));
      $notifyMe = mysql_real_escape_string(trim($data['notifyMe']));
      $dateAlert = date('d-m-Y', strtotime($dateActual. $notifyMe));
      $remarks = mysql_real_escape_string(trim($data['remarks']));
      $by = $this->byWho;
      $getDidServices = $this->getDidServices($dataCode);
      $code = $this->getAssetCode($dataCode);
      $ipaddress = $this->getUserIP();

      $sql = mysql_query("INSERT INTO tbl_reminder(dataType,dataCode,email,dateReminder,notifyValue,dateAlert,remarks,dateCreated,dateModified,byWho,emailsatu,emaildua,status,status2)
                          VALUES('Reminder','$dataCode','$whom','$dateActual','$notifyMe','$dateAlert','$remarks',NOW(),NOW(),'$by','No','No','New','New')");

      if(!$sql){
        return mysql_error();
      }else{
        // report
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('$getDidServices','$dataCode','Reminder','Reminder has been set at $dateAlert : $code','$ipaddress','$by',NOW())");
        return $sql;
      }

    }

  }
?>
