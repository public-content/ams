<?php
$a = securestring('decrypt',$_GET['a']);
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'systemmob.php?p='.securestring('encrypt',$pageID);
$homeDir = 'systemmob.php?p='.securestring('encrypt','800');
$pageDesc	= '';
$main->includephp('model','model_dashboard');
$controller = new Dashboard();

if ($a=="NoData") {
  $setAlert = "<div class='alert alert-danger alert-dismissible fade show m-alert m-alert--square m-alert--air' role='alert'>
    <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
    The system cannot find the file specified
  </div>";
}

if(isset($_POST['btn-search'])){
  $code = securestring('encrypt',$_POST['code']);
  // echo $code;
  ?>
  <script type="text/javascript">
    window.location.replace("systemmob.php?p=amdNV0tSOEJ0T2RpazhCQndHNExtdz09&r=<?php echo $code ?>");
  </script>
  <?php
}

?>
<style media="screen">
  #mycanvas{
    width: 100%;
    height: 250px;
    border: solid 2px #ccc;
    padding: 10px;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <!-- <?php echo $pageTitle; ?> -->
          Mobile Pixam
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <!-- <?php echo $pageName; ?> -->
                Search
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12" style="text-align:center;">
        <?php $alertMe = $alert; ?>
      </div>
      <div class="col-lg-12" style="text-align:center;" >
        <div class="col-lg-12"  style="text-align:center;padding:0;margin:0;">
          <canvas id="mycanvas"></canvas>
        </div>
        <form class="" action="" method="post">
          <div class="form-group m-form__group" id="myInput">
    				<div class="m-input-icon m-input-icon--left m-input-icon--right">
    					<input type="text" class="form-control m-input m-input--pill" name="code" placeholder="Type Code" style="height:50px;border-radius:50px;border:1px solid grey;">
    					<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
    				</div>
    			</div>
          <div class="form-group m-form__group" style="margin-top:60px;">
            <button type="submit" name="btn-search" class="btn m-btn--pill btn-info" style="height:40px;width: 150px;border:1px solid grey;">Search</button>
          </div>
        </form>
        <div class="form-group m-form__group" style="margin-top:20px;">
          or
        </div>
        <div class="form-group m-form__group" style="margin-top:20px;">
          <button type="button" class="btn m-btn--pill btn-primary" id="btnScan" style="height:40px;width: 150px;border:1px solid grey;">Scan</button>
        </div>
      </div>
      <div class="col-lg-12">

        <!-- <div class="col-lg-12" style="text-align:center;">
          <button id="btnScan" class="btn m-btn--square  m-btn m-btn--gradient-from-brand m-btn--gradient-to-info" style="width:100%;height:80px;">
  					<span style="text-align:center; font-size:30px">Scan</span>
            <i class="la la-search" style="text-align:center;font-size:30px"></i>
  				</button>
        </div> -->
        <br>
        <div class="col-lg-12">
          <?php if($setAlert!=""){
            echo $setAlert;
          } ?>
          <!-- <button type="button" class="btn m-btn--square  m-btn m-btn--gradient-from-info m-btn--gradient-to-warning" style="width:100%;height:150px;">
  					<span style="text-align:center; font-size:30px">Inventory</span>
            <br>
            <br>
            <i class="flaticon-open-box" style="text-align:center;font-size:30px"></i>
  				</button> -->
        </div>
      </div>
    </div>
  </div>
</div>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="vendor/scanner/jsQR.js"></script>
  <script src="vendor/scanner/Nahqrscan.js"></script>
  <script type="text/javascript">
      $("#mycanvas").hide();
      DWTQR("mycanvas");
      $("#btnScan").click(function(){
        $("#mycanvas").toggle();
        dwStartScan();
      });
      function dwQRReader(data){
        // alert(data);
        window.location.replace("systemmob.php?p=amdNV0tSOEJ0T2RpazhCQndHNExtdz09&r=" + data);
      }
  </script>
