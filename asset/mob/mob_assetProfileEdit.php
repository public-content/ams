<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'systemmob.php?p='.securestring('encrypt',$pageID);

$homeDir = 'systemmob.php?p='.securestring('encrypt','800');
$pageDesc	= '';
$main->includephp('model','model_assetProfileEdit');
// $main->includephp('view/modal','modal_categoryManager');
$controller = new profileEdit();
// $modal = new popServices();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
$ref = securestring('decrypt',$_GET['r']);
$r = $_GET['r'];
$newdir = $pageDir.'&r='.$r;
$pageDirProfile = 'systemmob.php?p='.securestring('encrypt',801).'&r='.securestring('encrypt',$ref);

if (isset($_POST['btn-register-assets'])) {
  // var_dump($_POST);
  $task = $controller->saveAsset($_POST,$ref);
  if ($task == 1) {
    $services = getTheServices($ref);

    $noty = getMessageNormal('success','The Asset has been Update');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}


$alert = $noty;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDirProfile ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                Asset Profile
              </span>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $newdir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                Edit
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
				<div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
                  Edit Asset <span style="color:red"><?php echo $ref; ?></span>
								</h3>
							</div>
						</div>
					</div>
          <div class="m-portlet__body">
            <form class="m-form m-form--fit m-form--label-align-right" action="" method="post">
              <div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">
									Code <span class="red">*</span>
								</label>
								<div class="col-form-label col-lg-4 col-md-9 col-sm-12" style="font-weight:800;">
                  <?php echo $ref ?>
								</div>
							</div>
              <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                  Category <span class="red">*</span>
                </label>
                <div class="col-form-label col-lg-2 col-md-9 col-sm-12"  style="font-weight:800;">
                  <?php
                    $mainValue = $controller->getCategoryValue($ref,'main');
                    echo $controller->getValueCategory($mainValue);
                  ?>
                </div>
                <div class="col-form-label col-lg-2 col-md-9 col-sm-12" style="font-weight:800;">
                  <?php
                    $childValue = $controller->getCategoryValue($ref,'child');
                    echo $controller->getValueCategory($childValue);
                  ?>
                </div>
              </div>
              <div class="m-form__seperator m-form__seperator--dashed"></div>
              <?php $controller->getEditForm($childValue,$ref); ?>
              <div class="m-form__seperator m-form__seperator--dashed"></div>
              <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                </label>
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions" style="padding-top:0;">
                    <button type="submit" name="btn-register-assets" class="btn btn-success">
                      Submit
                    </button>
                    <button type="reset" class="btn btn-secondary">
                      Reset
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
