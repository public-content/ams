<?php
function getMessageNormal($type,$message){
  if ($type == "success") {
    $msg = "<div class='alert alert-success alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Successfully! </strong>".$message." </div>";
    return $msg;
  }elseif ($type == "danger") {
    $msg = "<div class='alert alert-danger alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Ohh No! </strong> ".$message." </div>";
    return $msg;
  }elseif ($type == "warning") {
    $msg = "<div class='alert alert-warning alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Warning! </strong> ".$message." </div>";
    return $msg;
  }
}

function getMessageDatabase($serviceName,$link){
  $msg =  "<div class='m-alert m-alert--icon alert alert-primary' role='alert'> <div class='m-alert__icon'> <i class='fa fa-database'></i> </div>
  <div class='m-alert__text'> <strong> Well done! </strong> You successfully register new services.<br> System has create a database table for this services( ".$serviceName." ).<br> All you have to do is to set up a table. </div> <div class='m-alert__actions' style='width: 400px;'> <a href='".$link."' class='btn btn-warning btn-sm m-btn m-btn--pill m-btn--wide' > Setup </a> <button type='button' class='btn btn-light btn-sm m-btn m-btn--pill m-btn--wide' data-dismiss='alert' aria-label='Close'> Remind me later </button> </div> </div>";
  return $msg;
}

?>
