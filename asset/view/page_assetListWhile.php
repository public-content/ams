<?php
ini_set('max_execution_time', 10000);
$page = securestring('decrypt',$_GET['p']);
$ref = securestring('decrypt',$_GET['r']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID).'&r='.securestring('encrypt',$ref);
$homeDir = 'system.php?p='.securestring('encrypt','1');
$pageDesc	= '';
$main->includephp('model','model_assetListWhile');

// $main->includephp('view/modal','modal_formManager');
// $modal = new popFormManager();
$controller = new assetListWhile();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
$serviceName = $controller->getValue('tbl_category','dataValueRead','did',$ref);
$listName = $serviceName.'_masterlist';
$alert = $noty;
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>

    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>

      <div class="col-lg-12">
        <div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									<?php echo $controller->getValue('tbl_category','dataValueWrite','did',$ref)."'s Assets List" ?>
									<small>
                    <?php echo $pageDesc ?>
									</small>
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
            <div class="m-section">
							<div class="m-section__content">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
            			<div class="row align-items-center">
            				<div class="col-xl-8 order-2 order-xl-1">
            					<div class="form-group m-form__group row align-items-center">
            						<div class="col-md-4">
            							<div class="m-input-icon m-input-icon--left">
            								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
            								<span class="m-input-icon__icon m-input-icon__icon--left">
            									<span><i class="la la-search"></i></span>
            								</span>
            							</div>
            						</div>
            					</div>
            				</div>
            				<!-- <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <div>
                        <a href="<?php echo 'system.php?p='.securestring('encrypt',2).'&r='.securestring('encrypt',$ref) ?>" class="btn btn-focus m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                          <span>
                            <i class="la la-plus"></i>
                            <span>
                              Add New
                            </span>
                          </span>
                        </a>
                			</div>
            					<div class="m-separator m-separator--dashed d-xl-none"></div>
            				</div> -->
            			</div>
            		</div>
                <table class="m-datatableE" id="html_table" width="100%">
									<thead>
										<tr>
											<th title="Field #1">
												Code
											</th>
											<th title="Field #2">
												Category
											</th>
											<th title="Field #3">
												Name
											</th>
											<th title="Field #4">
												Serial No
											</th>
											<th title="Field #5">
												Date Received
											</th>
											<th title="Field #5">
												Action
											</th>
										</tr>
									</thead>
									<tbody>
                    <?php
                      $sqldataCode = mysql_query("SELECT DISTINCT dataCode FROM tbl_master WHERE dataRefServices = '$ref' AND active = 'Yes' OR active = 'Repair' LIMIT 10");
                      if (mysql_num_rows($sqldataCode)) {
                        while($rowdataCode = mysql_fetch_assoc($sqldataCode)){
                          $dataCode = $rowdataCode['dataCode'];
                              ?>
                              <tr>
          											<td>
                                <?php
                                  $assetCode = $controller->getValueMaster($dataCode,'code');
                                  echo $assetCode;
                                ?>
          											</td>
          											<td>
          												<?php
                                    $getMain = $controller->getValueMaster($dataCode,'child');
                                    echo $controller->getValueCategory($getMain);
                                  ?>
          											</td>
          											<td>
          												<?php echo $controller->getValueMaster($dataCode,'name'); ?>
          											</td>
          											<td>
          												<?php echo $controller->getValueMaster($dataCode,'serialNo'); ?>
          											</td>
          											<td>
          												<?php
                                    $dateReceived = $controller->getValueMaster($dataCode,'dateReceived');
                                    echo date('d/m/Y', strtotime($dateReceived));
                                  ?>
          											</td>
          											<td>
                                  <a href="<?php echo 'system.php?p='.securestring('encrypt',200).'&r='.securestring('encrypt',$assetCode); ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="View"> <i class="fa fa-eye"></i> </a>
          											</td>
          										</tr>

                          <?php
                        }
                      }
                    ?>

                  </tbody>
                </table>
							</div>
						</div>
					</div>
				</div>
      </div>
      <?php $inList = $controller->checkCondemnExist($ref); if($inList > 0){ ?>
      <div class="col-lg-12" id='showTable' onload="showCondemnList('<?php echo $ref ?>','<?php echo $dataCode ?>')">
        <div class="col-lg-12" id="yayornay">

        </div>
          <button class="btn btn-outline-metal active" type="button" name="button" onclick="showCondemnList('<?php echo $ref ?>','<?php echo $dataCode ?>')">Open Condem List</button>

      </div>
      <?php } ?>
    </div>
  </div>
  <!-- <p id="demo"></p> -->
</div>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script src="js/html-table.js" type="text/javascript"> </script>
<script type="text/javascript">
function showCondemnList(ref,datacode){
  var dataString = { ref : ref, dcode : datacode};

  $.ajax({
    type:"post",
    url:"asset/view/ajax/condemn.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('showTable').innerHTML = html;
    }
  });
  return false;
}
function isCondemn(type,dataCode,ref){
  var by = <?php echo $byWho; ?>;
  var dataString = { datatype : type, dataCode : dataCode, dataRef : ref, by : by};

  $.ajax({
    type:"post",
    url:"asset/view/ajax/condemn.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('showTable').innerHTML = html;
    }
  });
  return false;
}
</script>
