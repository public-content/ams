<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','1');
$pageDesc	= '';
$main->includephp('model','model_logsMaster');
$controller = new logsMaster();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script>
$(document).ready(function(){
  $("#logInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#logTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12">
        <?php $alertMe = $alert; ?>
      </div>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									<?php echo $pageName; ?>
								</h3>
							</div>
						</div>
            <div class="m-portlet__head-tools" style="text-align:right;">
                <div class="m-input-icon m-input-icon--left col-lg-4" style="float:right;">
  								<input id="logInput" type="text" class="form-control m-input m-input--pill" placeholder="Search">
  								<span class="m-input-icon__icon m-input-icon__icon--left">
  									<span>
  										<i class="la la-search"></i>
  									</span>
  								</span>
  							</div>
            </div>
					</div>
					<div class="m-portlet__body">
            <table class="table table-sm m-table m-table--head-bg-brand">
							<thead class="thead-inverse">
								<tr>
									<th>
										#
									</th>
									<th>
										Type
									</th>
									<th>
										Status
									</th>
									<th>
                    Name
									</th>
									<th>
										IP Address
									</th>
									<th>
                    DateCreated
									</th>
								</tr>
							</thead>
							<tbody id="logTable">
                <?php $bil = 1; $logSql = mysql_query("SELECT * FROM tbl_report ORDER BY did DESC"); if(mysql_num_rows($logSql)){ while($logRow = mysql_fetch_assoc($logSql)){ ?>
                <?php if($bil <= 1000){ ?>
                  <tr>
  									<th scope="row">
  										<?php echo $bil ?>
  									</th>
  									<td>
  										<?php echo $logRow['dataType'] ?>
  									</td>
  									<td>
  										<?php echo $logRow['dataStatus'] ?>
  									</td>
  									<td>
  										<?php echo $controller->getUserName($logRow['byWho']); ?>
  									</td>
  									<td>
  										<?php echo $logRow['byIP'] ?>
  									</td>
  									<td>
  										<?php echo $logRow['dateCreated'] ?>
  									</td>
  								</tr>
                <?php } ?>
                <?php $bil++; } }else{ ?>
                  <tr>
  									<td colspan="6">
  										No Data
  									</td>
  								</tr>
                <?php } ?>
            	</tbody>
            </table>
					</div>
				</div>
      </div>
    </div>
  </div>
</div>
