<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_dashboard');
$controller = new Dashboard();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script>
$(document).ready(function(){
  $("#mySearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#assetTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
        <!-- height: -moz-max-content; -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12">
        <?php $alertMe = $alert; ?>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-8">
            <div class="row">
              <div class="col-lg-4" style="margin-bottom:28.6px;">
                <a class="btn m-btn m-btn--gradient-from-brand m-btn--gradient-to-accent  m-btn m-btn--icon active" style="cursor:unset;width:100%;height:150px;color:white;" onclick="btnFunction('fc')">
                  <span>
                    <i class="la la-folder-open"></i>
                    <span>
                      Assets in Department
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;"><?php echo $controller->valueLocation(securestring('decrypt',$_SESSION['location'])); ?></span>
                  </span>
                </a>
              </div>
              <div class="col-lg-4" style="margin-bottom:28.6px;">
                  <a href="<?php echo 'system.php?p='.securestring('encrypt',900); ?>" class="btn m-btn m-btn--gradient-from-accent m-btn--gradient-to-info m-btn m-btn--icon active" style="cursor:unset;width:100%;height:150px;color:white;" onclick="btnFunction('fc')">
                  <span>
                    <i class="la la-info-circle"></i>
                    <span>
                      Reminders
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;"><?php echo getNotification2($_SESSION['userid']); ?></span>
                  </span>
                </a>
              </div>
              <div class="col-lg-4" style="margin-bottom:28.6px;">
                  <a href="<?php //echo 'system.php?p='.securestring('encrypt',104); ?>" class="btn m-btn m-btn--gradient-from-info m-btn--gradient-to-brand m-btn m-btn--icon active" style="cursor:unset;width:100%;height:150px;color:white;" onclick="btnFunction('fc')">
                  <span>
                    <i class="la la-warning"></i>
                    <span>
                      Condemn(<?php echo $controller->getCondemnRule($_SESSION['role']) ?>)
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;"><?php echo $controller->valueLocationCondemn(securestring('decrypt',$_SESSION['location']),$controller->getCondemnRule($_SESSION['role'])); ?></span>
                  </span>
                </a>
              </div>
              <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
    							<div class="m-portlet__head">
    								<div class="m-portlet__head-caption">
    									<div class="m-portlet__head-title">
    										<h3 class="m-portlet__head-text">
    											Assets List
    										</h3>
    									</div>
    								</div>
    								<div class="m-portlet__head-tools">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="m-input-icon m-input-icon--left">
                            <div class="col">
      												<div class="dropdown">
      													<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      														Services
      													</button>
      													<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                  <?php $sql = mysql_query("SELECT did,dataValueWrite FROM tbl_category WHERE dataType = 'Service'"); if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
                                    <button class="dropdown-item" type="button" onclick="changeDepartment('<?php echo $row['did'] ?>')">
                                      <?php echo $row['dataValueWrite']; ?>
        														</button>
                                  <?php } } ?>
      													</div>
      												</div>
      											</div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="Search..." id="mySearch">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                              <span>
                                <i class="la la-search"></i>
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>
    								</div>
    							</div>
                  <div class="m-portlet__body"  id="tableDepartment" style="height:500px;overflow-y: scroll;padding:2px 8px 18px 18px;">
                    <table class="table table-striped m-table">
											<thead>
                        <tr>
    											<th title="Field #1">
    												Code
    											</th>
    											<th title="Field #2">
    												Name
    											</th>
    											<th title="Field #3">
    												User
    											</th>
    											<th title="Field #3">
    												Location
    											</th>
    											<th title="Field #4">
    												Date
    											</th>
    											<th title="Field #4">

    											</th>
    										</tr>
											</thead>
                      <tbody id="assetTable">
                        <?php
                        $services = 1;
                        $location = $controller->getLocationChar(securestring('decrypt',$_SESSION['location']));
                        $sqldataCode = mysql_query("SELECT DISTINCT dataCode FROM tbl_master WHERE dataRefServices = '$services' AND asValue = '$location' AND active = 'Yes' OR active = 'Repair'");
                        if (mysql_num_rows($sqldataCode)) {
                          while($rowdataCode = mysql_fetch_assoc($sqldataCode)){
                            $dataCode = $rowdataCode['dataCode'];
                        ?>
    										<tr>
    											<td>
                            <?php
                              $assetCode = $controller->getValueMaster($dataCode,'code');
                              echo $assetCode;
                            ?>
    											</td>
    											<td>
    												<?php echo $controller->getValueMaster($dataCode,'name'); ?>
    											</td>
    											<td>
    												<?php echo $controller->getValueMaster($dataCode,'user'); ?>
    											</td>
    											<td>
    												<?php echo $controller->getValueMaster($dataCode,'location'); ?>
    											</td>
    											<td>
    												<?php echo $controller->getValueMaster($dataCode,'dateReceived'); ?>
    											</td>
    											<td>
                            <a href="system.php?p=<?php echo securestring('encrypt','200') ?>&r=<?php echo securestring('encrypt',$controller->getValueMaster($dataCode,'code')) ?>" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Details"><i class="la la-eye"></i></a>
    											</td>
    										</tr>
                            <?php
                          }
                        }
                      ?>
    									</tbody>
										</table>
    								<!--end: Datatable -->
    							</div>
    						</div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="col-lg-12">
              <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													Activity
                          <small>newest</small>
												</h3>
											</div>
										</div>
									</div>
        				<div class="m-portlet__body" style="height:300px;overflow-y: scroll;padding:2px 8px 18px 18px;">
                  <table class="table table-sm m-table m-table--head-bg-brand">
											<tbody>
                        <?php
                          $userid = securestring('decrypt',$_SESSION['userid']);
                          $query = mysql_query("SELECT dataStatus,byWho FROM tbl_report WHERE byWho = '$userid' ORDER BY did DESC LIMIT 6");
                          if(mysql_num_rows($query)){
                            while ($log = mysql_fetch_assoc($query)) {
                          ?>
                          <tr>
  													<th scope="row">
  														<span class="m-badge" style="background:darkgrey;min-height:10px;min-width:10px;"></span>
  													</th>
  													<td>
  														<?php echo $log['dataStatus'] ?><br>
                              <small>by You</small>
  													</td>
  													<td>
  														<!-- 3hr -->
  													</td>
  												</tr>
                          <?php
                            }
                          }
                        ?>
											</tbody>
										</table>
                </div>
        			</div>
            </div>
            <div class="col-lg-12">
              <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													User
                          <small>In Department</small>
												</h3>
											</div>
										</div>
									</div>
        				<div class="m-portlet__body" style="padding:20px;">
                  <div class="m-widget4">
                    <?php $locationUser = securestring('decrypt',$_SESSION['location']);
                      $sql = mysql_query("SELECT * FROM tbl_user WHERE location = '$locationUser'");
                      if(mysql_num_rows($sql)){
                        while($user = mysql_fetch_assoc($sql)){ ?>
                          <div class="m-widget4__item">
        										<div class="m-widget4__img m-widget4__img--pic">
        											<img src="<?php echo $user['img'] ?>" alt="" width="50">
        										</div>
        										<div class="m-widget4__info">
        											<span class="m-widget4__title">
        												<?php echo $user['name'] ?>
        											</span>
        											<br>
        											<span class="m-widget4__sub">
        												<?php echo $controller->getRoleChar($user['role']).", ".$user['department'] ?>
        											</span>
        										</div>
        										<div class="m-widget4__ext">
        											<!-- <a href="#"  class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary">
        												Follow
        											</a> -->
        										</div>
        									</div>
                    <?php } } ?>
                  </div>
                </div>
        			</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<!-- <script src="js/wizard/html-table-department.js" type="text/javascript"> </script> -->
<script type="text/javascript">
  function changeDepartment(data){
    var service = data;
    var dataString = { service : service};

    $.ajax({
      type:"post",
      url:"asset/view/ajax/tableDepartment.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('tableDepartment').innerHTML = html;
      }
    });
    return false;
  }
</script>
