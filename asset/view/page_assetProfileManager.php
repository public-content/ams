<?php
$page = securestring('decrypt',$_GET['p']);
$ref = securestring('decrypt',$_GET['r']);
$r = $_GET['r'];
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','1');
$pageDesc	= '';
$main->includephp('model','model_assetProfileManager');
// $main->includephp('view/modal','modal_formManager');
// $modal = new popFormManager();
$controller = new assetProfileManager();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
$newDir = $pageDir.'&r='.$r;


$alert = $noty;
?>

<style media="screen">
  .inactivate{
    display: none;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $newDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Profile Manager <span style="color:red;"><?php if($ref != ''){ echo $controller->valueRef($ref); }  ?></span>
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body row">
            <div class="col-lg-12" >
              <div class="form-group m-form__group row">
								<label for="example-text-input" class="col-lg-2 col-form-label">
									Services
								</label>
								<div class="col-lg-4">
                  <select class="form-control m-input" id="serviceSelect" onchange="selectFunction()">
                    <option>
											select
										</option>
                    <?php $sql = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Service'");if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
                      <option <?php if($ref == $row['did']){print 'selected'; } ?> value="<?php echo securestring('encrypt',$row['did']); ?>">
                       <?php echo $row['dataValueWrite']; ?>
  										</option>
                    <?php } } ?>
									</select>
								</div>
							</div>
              <?php if($ref != ''){ ?>
                <div class="col-lg-12 row" id="columnMenu" style="margin-top:30px;">
                  <div class="col-lg-3">
                    <a class="btn m-btn--square  m-btn m-btn--gradient-from-success m-btn--gradient-to-info" style="width:100%;height:100px;color:white;" onclick="menuOption('profileLayout')">
                      <span>
                        <i class="fa fa-list-alt"></i>
                        <span>
                          Profile Layout
                        </span>
                      </span>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a class="btn m-btn--square  m-btn m-btn--gradient-from-info m-btn--gradient-to-primary" style="width:100%;height:100px;color:white;" onclick="menuOption('toolManager')">
                      <span>
                        <i class="fa fa-magic"></i>
                        <span>
                          Tool Manager
                        </span>
                      </span>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a class="btn m-btn--square  m-btn m-btn--gradient-from-primary m-btn--gradient-to-success" style="width:100%;height:100px;color:white;" onclick="menuOption('setting')">
                      <span>
                        <i class="fa fa-gear"></i>
                        <span>
                          Setting
                        </span>
                      </span>
                    </a>
                  </div>
                </div>
              <?php } ?>
                <div class="col-lg-12" id="btnBody">

                </div>
            </div>
					</div>
				</div>
      </div>
    </div>
  </div>
  <!-- <p id="demo"></p> -->
</div>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script src="js/select2.js" type="text/javascript"></script>
<script type="text/javascript">
  function selectFunction(){
    var x = document.getElementById('serviceSelect').value;
    var link = "<?php echo $pageDir ?>" ;
    location.replace(link + "&r=" + x);

  }

  function menuOption(type){
    var dataString = { btnType : type };

    $.ajax({
      type:"post",
      url:"asset/view/ajax/assetProfile.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('btnBody').innerHTML = html;
      }
    });
    return false;
  }

  function btnOption(tool){
    var ref = "<?php echo $ref; ?>";
    var dataString = { tool : tool, ref : ref };
    $.ajax({
      type:"post",
      url:"asset/view/ajax/assetProfile.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('toolBody').innerHTML = html;
      }
    });
    return false;
  }

  function toolFieldset(tool){
    var ref = "<?php echo $ref; ?>";
    var fieldset = document.getElementById('toolFieldset').value;
    var dataString = {tool : tool, ref : ref, fieldset : fieldset};
    $.ajax({
      type:"post",
      url:"asset/view/ajax/assetProfile.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('toolBody').innerHTML = html;
      }
    });
    return false;
  }

  function deleteThis(did,tool){
    var ref = "<?php echo $ref; ?>";
    var fieldset = document.getElementById('toolFieldset').value;
    var dataString = {tool : tool, ref : ref, did : did};
    $.ajax({
      type:"post",
      url:"asset/view/ajax/assetProfile.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('toolBody').innerHTML = html;
      }
    });
    return false;
  }

  function panelValue(type){
    var value = document.getElementById('panelSelect').value;
    var dataString = { type : type, value : value };

    $.ajax({
      type:"post",
      url:"asset/view/ajax/assetProfile.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('btnBody').innerHTML = html;
      }
    });
    return false;
  }

  function switchFunction(tool,ref){
    var getValue = document.getElementById(tool).value;
    if (getValue == 'checked') {
      var value = 'off';
    }else{
      var value = 'on';
    }
    var dataString = { valueCheck : value, ref : ref, valueTools : tool };
    $.ajax({
      type:"post",
      url:"asset/view/ajax/assetProfile.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('notification').innerHTML = html;
      }
    });
    return false;
  }




</script>
