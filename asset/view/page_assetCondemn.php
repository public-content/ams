<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_assetCondemn');
$main->includephp('view/modal','modal_assetCondemn');
$controller = new assetCondemn();
$modal = new popAssetCondemn();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);

if(isset($_POST['btn-fillForm'])){
  // var_dump($_POST);
  $task = $controller->insertFormFill($_POST);
  if($task == 1){
    $noty = getMessageNormal('success','The condemn assets has been Updated');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-notValid'])) {
  $task = $controller->returnToAssets($_POST);
  $dataValue = $_POST['dataCode'];
  $getDidServices = $controller->thisMaster('dataRefServices',$dataValue);
  if($task == 1){
    createJson($getDidServices);
    $noty = getMessageNormal('success','The Asset has been return to List');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['btn-notVerified'])){
  // var_dump($_POST);
  $task = $controller->returnNewCondemns($_POST);
  if($task == 1){
    $noty = getMessageNormal('success','The asset has not been verified due to incomplete data');
  }else{
    $noty = getMessageNormal('dangeer',$task);
  }
}elseif(isset($_POST['btn-verified'])){
  $task = $controller->pushToDispossal($_POST);
  if($task == 1){
    $noty = getMessageNormal('success','The asset has been verifed');
  }else{
    $noty = getMessageNormal('dangeer',$task);
  }
}elseif(isset($_POST['btn-disposal'])) {
  // var_dump($_POST);
  $task = $controller->pushToMethod($_POST);
  if($task == 1){
    $noty = getMessageNormal('success','Condemn process has been complete');
  }else{
    $noty = getMessageNormal('dangeer',$task);
  }
}
// echo $controller->getCondemnRule($_SESSION['role']);
$alert = $noty;
?>
<style media="screen">

</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<h3 class="m-portlet__head-text">
    							Condemn List
    						</h3>
    					</div>
    				</div>
    			</div>
    			<div class="m-portlet__body" style="overflow-x: auto;">
    				<!--begin::Section-->
    				<div class="m-section">
    					<div class="m-section__content">
    						<table class="table table-bordered table-hover">
    						  	<thead>
    						    	<tr>
    						      		<th>#</th>
    						      		<th>Code</th>
    						      		<th>Services</th>
    						      		<th>Location</th>
    						      		<th>Who Responsible</th>
    						      		<th>Date Created</th>
    						      		<th>Status</th>
    						      		<th>Action</th>
    						    	</tr>
    						  	</thead>
    						  	<tbody>
                      <?php
                        if($controller->getCondemnRule($_SESSION['role']) == 'all'){
                          $query = "SELECT * FROM tbl_condemn ORDER BY did DESC";
                        }elseif ($controller->getCondemnRule($_SESSION['role']) == 'Checker') {
                          $query = "SELECT * FROM tbl_condemn WHERE dataStatus = 'New Request' ORDER BY did DESC";
                        }elseif ($controller->getCondemnRule($_SESSION['role']) == 'Verifier') {
                          $query = "SELECT * FROM tbl_condemn WHERE dataStatus = 'Complete Data' ORDER BY did DESC";
                        }elseif ($controller->getCondemnRule($_SESSION['role']) == 'Disposal') {
                          $query = "SELECT * FROM tbl_condemn WHERE dataStatus = 'Verified' ORDER BY did DESC";
                        }
                      ?>
                      <?php $count = 1; $sql = mysql_query($query); if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ $dataCode = $row['dataValue']; ?>
                        <tr>
      							      	<th scope="row"><?php echo $count; ?></th>
      							      	<td><?php echo $controller->getMaster('code',$row['dataValue']); ?></td>
      							      	<td><?php echo $controller->getMasterColumn('dataRefServices',$row['dataValue']); ?></td>
      							      	<td><?php echo $controller->getMaster('location',$row['dataValue']); ?></td>
      							      	<td><?php echo $controller->getUser($row['dataWhoRequest']); ?></td>
      							      	<td><?php echo $row['dateModified'] ?></td>
      							      	<td><?php echo $row['dataStatus']; ?></td>
      							      	<td>
                              <?php if($row['dataStatus'] == 'New Request'){ ?>
                                <button type="button" class="btn m-btn m-btn--pill m-btn--gradient-from-brand m-btn--gradient-to-info" data-toggle="modal" data-target="#c_form<?php echo $dataCode; ?>"> <i class="la la-pencil"></i> Fill Form</button>
                              <?php }elseif($row['dataStatus'] == 'Complete Data'){ ?>
                                <button type="button" class="btn m-btn m-btn--pill m-btn--gradient-from-primary m-btn--gradient-to-danger" data-toggle="modal" data-target="#c_verified<?php echo $dataCode; ?>"> <i class="la la-check-square"></i> Verified</button>
                              <?php }elseif($row['dataStatus'] == 'Verified'){ ?>
                                <button type="button" class="btn m-btn m-btn--pill m-btn--gradient-from-info m-btn--gradient-to-primary" data-toggle="modal" data-target="#c_action<?php echo $dataCode; ?>"> <i class="la la-gavel"></i> Take Action</button>
                              <?php } ?>
                            </td>
      						    	</tr>
                        <?php echo $modal->modalFillForm($dataCode); ?>
                        <?php echo $modal->modalVerified($dataCode); ?>
                        <?php echo $modal->modalTakeAction($dataCode); ?>
                      <?php $count++; } }else{ ?>
                        <tr>
      							      	<th scope="row" colspan="8" style="text-align:center;">0 number of asset condemns</th>
      						    	</tr>
                      <?php } ?>
    						  	</tbody>
    						</table>
    					</div>
    				</div>
    				<!--end::Section-->
    			</div>
    			<!--end::Form-->
    		</div>
      </div>
    </div>
  </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script type="text/javascript">


</script>
