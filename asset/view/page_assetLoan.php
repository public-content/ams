<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_assetLoan');
$controller = new loan();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);

if(isset($_POST['btn-saveNew'])){
  // var_dump($_POST);
  $task = $controller->sendThisNew($_POST);
  if($task == 1){
    $message = getMessageNormal('success','New loan has been created');
  }else{
    $message = getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btnNewable'])) {
  // var_dump($_POST);
  $task = $controller->sendNewAsset($_POST);
  if($task == 1){
    $message = getMessageNormal('success','');
  }else{
    $message = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['btneditable'])){

  $task = $controller->sendEditAsset($_POST);
  if($task == 1){
    $message = getMessageNormal('success','');
  }else{
    $message = getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btnDelable'])) {
  // var_dump($_POST);
  $task = $controller->sendDelAsset($_POST);
  if($task == 1){
    $message = getMessageNormal('success','');
  }else{
    $message = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['btnloanable'])){
  $task = $controller->sendLoanAsset($_POST);
  if($task == 1){
    $message = getMessageNormal('success','');
  }else{
    $message = getMessageNormal('danger',$task);
  }
}

$alert = $message;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12" id="tabMessage">
        <?php if($alert != ''){ echo $alert;} ?>
      </div>
      <div class="col-lg-2" style="margin-bottom:28.6px;">
        <a href="<?php echo $pageDir ?>" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-danger  m-btn m-btn--icon" style="width:100%;height:150px;color:white;">
          <span>
            <i class="la la-list"></i>
            <span>
              Available List
            </span>
            <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
            <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;" id="tbl_user"><?php echo $controller->countLoanlist('available'); ?></span>
          </span>
        </a>
      </div>
      <div class="col-lg-2" style="margin-bottom:28.6px;">
        <a class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-danger  m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnPage('listLoan')">
          <span>
            <i class="la la-list"></i>
            <span>
              Loan List
            </span>
            <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
            <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;" id="tbl_user"><?php echo $controller->countLoanlist('loan'); ?></span>
          </span>
        </a>
      </div>
      <div class="col-lg-2" style="margin-bottom:28.6px;">
        <a class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-danger  m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnPage('listLoan')">
          <span>
            <i class="la la-area-chart"></i>
            <span>
              Report
            </span>
            <!-- <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span> -->
            <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;" id="tbl_user"><i class="la la-bar-chart" style="font-size: 6rem;"></i></span>
          </span>
        </a>
      </div>
      <div class="col-lg-12" id="displayPotlet">
        <div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<h3 class="m-portlet__head-text">
    							Asset Loan <small>Availble List</small>
    						</h3>
    					</div>
    				</div>
            <div class="m-portlet__head-tools">
    					<ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <input type="text" class="form-control m-input" id="searchInput" placeholder="Search...">
                </li>
                <li class="m-portlet__nav-item">
                  <a href="#" class="btn btn-primary m-btn m-btn--icon m-btn--pill m-btn--air" data-toggle="modal" data-target="#createNew">
                   <span>
                     <i class="la la-plus"></i>
                     <span>Add New</span>
                   </span>
                 </a>
               </li>
    					</ul>
    				</div>
    			</div>
    			<div class="m-portlet__body">
            <div class="m-section">
							<div class="m-section__content">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>
												#
											</th>
											<th>
												TYPE
											</th>
											<th>
												ASSET NAME
											</th>
											<th>
												ASSET SERIAL NO
											</th>
											<th>
												ACTION
											</th>
										</tr>
									</thead>
									<tbody id="search">
                    <?php $bil=1;$sql=mysql_query("SELECT * FROM tbl_rental WHERE status = 'available' ORDER BY type ASC");if(mysql_num_rows($sql)){while($row=mysql_fetch_assoc($sql)){?>
                      <tr>
  											<th scope="row">
  												<?php echo $bil; ?>
                          <?php $did = $row['did']; ?>
  											</th>
  											<td>
  												<?php echo $row['type']; ?>
  											</td>
  											<td>
  												<?php echo $row['item']; ?>
  											</td>
  											<td>
  												<?php echo $row['serialNo']; ?>
  											</td>
  											<td>
                          <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only" data-toggle="modal" data-target="#edit<?php echo $did; ?>"> <i class="la la-edit"></i> </a>
                          <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" data-toggle="modal" data-target="#del<?php echo $did; ?>"> <i class="la la-trash"></i> </a>
                          <a href="#" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info" data-toggle="modal" data-target="#loan<?php echo $did; ?>">Loan</a>
  											</td>
                        <?php echo $controller->modalEdit($did); ?>
                        <?php echo $controller->modalDel($did); ?>
                        <?php echo $controller->modalLoan($did); ?>
  										</tr>
                    <?php $bil++; }} ?>
									</tbody>
								</table>
							</div>
						</div>
    			</div>
    		</div>
      </div>
    </div>
  </div>
</div>



<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script type="text/javascript">
function btnPage(page){
  var dataString = { page : page };
  document.getElementById('displayPotlet').innerHTML = "<div class='col-lg-12' style='text-align:center;'><img src='images/file_more/more/loading2.gif' alt='' width='200'></div>";

  $.ajax({
    type:"post",
    url:"asset/view/ajax/loanmodel.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('displayPotlet').innerHTML = html;
    }
  });
  return false;
}

function formfunction(value){
  document.getElementById(value).submit();
}

function functionReturn(value,by){
  var func = "return";
  var dataString = { func : func, value : value, by : by };
  document.getElementById('tabMessage').innerHTML = "<div class='col-lg-12' style='text-align:center;'><img src='images/file_more/more/loading2.gif' alt='' width='200'></div>";
  $.ajax({
    type:"post",
    url:"asset/view/ajax/loanmodel.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('tabMessage').innerHTML = html;
    }
  });
  return false;
}

</script>
<script>
$(document).ready(function(){
  $("#searchInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#search tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- modal start -->
<div class="modal fade" id="createNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form class="" action="" method="post">
      <div class="modal-content">
  			<div class="modal-header">
  				<h5 class="modal-title" id="exampleModalLabel">
  					New Available
  				</h5>
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  					<span aria-hidden="true">
  						&times;
  					</span>
  				</button>
  			</div>
  			<div class="modal-body">
          <div class="form-group m-form__group row">
						<label for="assetType" class="col-4 col-form-label">TYPE</label>
						<div class="col-6">
              <select class="form-control m-input m-input--air" id="assetType" name="assetType" required>
  							<option value="">Choose</option>
                <?php $sqlOpt = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '94'"); if(mysql_num_rows($sqlOpt)){ while($rowOpt = mysql_fetch_assoc($sqlOpt)){ ?>
                  <option value="<?php echo $rowOpt['dataValue'] ?>"><?php echo $rowOpt['dataValue'] ?></option>
                <?php } } ?>
  						</select>
						</div>
					</div>
          <div class="form-group m-form__group row">
						<label for="assetName" class="col-4 col-form-label">ASSET NAME</label>
						<div class="col-8">
							<input class="form-control m-input" type="text" placeholder="Assets Code / Name" id="assetName" name="assetName" required>
						</div>
					</div>
          <div class="form-group m-form__group row">
						<label for="serialNo" class="col-4 col-form-label">SERIAL NO.</label>
						<div class="col-8">
							<input class="form-control m-input" type="text" placeholder="Serial No." id="serialNo" name="assetSerial" required>
						</div>
					</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-secondary" data-dismiss="modal">
  					Close
  				</button>
  				<button type="submit" name="btnNewable" class="btn btn-brand">
  					Create
  				</button>
  			</div>
  		</div>
    </form>
	</div>
</div>

<!-- modal end -->
