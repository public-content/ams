<?php
$page = securestring('decrypt',$_GET['p']);
$ref = securestring('decrypt',$_GET['r']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID).'&r='.securestring('encrypt',$ref);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_assetList');
$controller = new assetList();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
$serviceName = $controller->getValue('tbl_category','dataValueRead','did',$ref);
$listName = $serviceName.'_masterlist';
$alert = $noty;
createJson($ref);
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    <?php echo $pageTitle; ?>
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo $pageDir ?>" class="m-nav__link">
                            <span class="m-nav__link-text">
                                <?php echo $pageName; ?>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div>
                <a href="<?php echo 'system.php?p='.securestring('encrypt',101).'&r='.securestring('encrypt',$ref) ?>" class="btn btn-focus m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    <span>
                        <i class="la la-plus"></i>
                        <span>
                            Add New
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <?php if ($alert != '') { ?>
            <div class="col-lg-12">
                <?php  echo $alert; ?>
            </div>
            <?php } ?>
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo $controller->getValue('tbl_category','dataValueWrite','did',$ref)."'s Assets List" ?>
                                    <small>
                                        <?php echo $pageDesc ?>
                                    </small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-form m-form--label-align-right ">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center" style="padding-top:15px;">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span><i class="la la-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="<?php echo $serviceName ?>_masterlist" id="json_data"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <p id="demo"></p> -->
</div>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<?php $main->includephp('controller ','assetslist'); ?>