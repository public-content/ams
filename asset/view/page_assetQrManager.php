<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageNameFile	= pageInfo($page,'pageFileName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_assetReminder');
$controller = new reminderer();
$main->includephp('view/modal','modal_assetReminder');
$modal = new reminderModal();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);

if(isset($_POST['print-selected'])){
  var_dump($_POST);
}

?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script>
$(document).ready(function(){
  $("#myInput").on("change keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--unair">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<h3 class="m-portlet__head-text">
    						  QR Print
    						</h3>
    					</div>
    				</div>
    			</div>
    			<div class="m-portlet__body">
            <form class="" action="print.php" method="post" >
              <div class="form-group m-form__group row">
    						<div class="col-lg-2">
    							<label>Services:</label>
                  <select class="form-control m-input" id="OptionServices" name="services" onchange="selectServices()">
      							<option value="">Choose</option>
                    <?php $sqlSer = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Service'"); if(mysql_num_rows($sqlSer)){ while($rowSer = mysql_fetch_assoc($sqlSer)){ ?>
                      <option value="<?php echo $rowSer['did'] ?>"><?php echo $rowSer['dataValueWrite'] ?></option>
                    <?php } } ?>
      						</select>
    						</div>
                <div class="col-lg-2">
                  <label>Location:</label>
                  <select class="form-control m-input" id="myInput" name="services" onchange="">
      							<option value="">Choose</option>
                    <?php $sqlOpt = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '1'"); if(mysql_num_rows($sqlOpt)){ while($rowOpt = mysql_fetch_assoc($sqlOpt)){ ?>
                      <option value="<?php echo $rowOpt['dataValue'] ?>"><?php echo $rowOpt['dataValue'] ?></option>
                    <?php } } ?>
      						</select>
                  <!-- <input id="myInput" type="text" placeholder="Search.."> -->
                  <!-- <select class="" name="" id="myInput">
                    <option value="">Choose</option>
                    <option value="IT TRAINING ROOM">IT Training Room</option>
                  </select> -->
                </div>
    					</div>
              <div class="row col-lg-12">
                <div class=" col-lg-6" id="print-table">

                </div>
                <div class=" col-lg-6" id="button">
                  <div class="m-portlet__body">
                    <span id="Total-Select"></span>
                    <button href="#" name="print-selected" type="submit" class="btn btn-secondary m-btn m-btn--icon">
  									<span>
  										<i class="la la-print"></i>
  										<span>Print</span>
  									</span>
  								</button>
                  </div>
                </div>
              </div>
            </form>
    			</div>
    		</div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function selectServices(){
    var services = document.getElementById("OptionServices").value;
    var pages = "table";
    // alert(services);
    var dataString = { page : pages, services : services };
    $.ajax({
      type:"post",
      url:"asset/view/ajax/qrpage.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('print-table').innerHTML = html;
      }
    });
    return false;
  }

  function checkAll(){
    var items=document.getElementsByClassName('checkedir');
    for(var i=0; i<items.length; i++){
					if(items[i].type=='checkbox')
						items[i].checked=true;
		}
    // document.getElementById("Total-Select").innerHTML = items.length;
  }
</script>
