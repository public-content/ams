<?php

$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','1');
$pageDesc	= '';
$main->includephp('model','model_categoryManager');
$main->includephp('view/modal','modal_categoryManager');
$controller = new categoryManager();
$modal = new popServices();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
$r = securestring('decrypt',$_GET['r']);
$dataCategory = $r;

if (isset($_POST['btn-addServices'])) {
  $task = $controller->getAllValue($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','You have create a new Category');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-addMain'])) {
  $task = $controller->pushMainValue($_POST,'Main');
  if ($task == 1) {
    $noty = getMessageNormal('success','You have create a new Main');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-addChild'])) {
  $task = $controller->pushMainValue($_POST,'Sub');
  if ($task == 1) {
    $noty = getMessageNormal('success','You have create a new Child');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-editChild'])) {
  $task = $controller->pushEdit($_POST,'Child');
  if ($task == 1) {
    $noty = getMessageNormal('success','Child has been Update');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-editMain'])) {
  $task = $controller->pushEdit($_POST,'Main');
  if ($task == 1) {
    $noty = getMessageNormal('success','Main has been Update');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-delMain'])) {
  $task = $controller->pushDel($_POST,'Main');
  if ($task == 1) {
    $noty = getMessageNormal('success','Main has been Delete');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-delChild'])) {
  $task = $controller->pushDel($_POST,'Child');
  if ($task == 1) {
    $noty = getMessageNormal('success','Child has been Delete');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-delSer'])) {
  $task = $controller->pushDel($_POST,'Service');
  if ($task == 1) {
    $noty = getMessageNormal('success','Service has been Delete');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}

$alert = $noty;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--mobile">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<h3 class="m-portlet__head-text">
    							Category Manager
    							<small>
    								All category setting
    							</small>
    						</h3>
    					</div>
    				</div>
            <div class="m-portlet__head-tools">
              <a class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" style="color:white;" data-toggle="modal" data-target="#add_services">
                <span>
                  <i class="la la-plus"></i>
                  <span>
                    Add
                  </span>
                </span>
              </a>
            </div>
          </div>
          <div class="m-portlet__body row">
            <div class="col-lg-4">
              <div class="m-portlet m-portlet--bordered m-portlet--unair">
  							<div class="m-portlet__head">
  								<div class="m-portlet__head-caption">
  									<div class="m-portlet__head-title">
  										<h3 class="m-portlet__head-text">
  											Services
  										</h3>
  									</div>
  								</div>
  							</div>
  							<div class="m-portlet__body" style="padding:10px;">
                  <table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th>
													Value
												</th>
												<th>
													Action
												</th>
											</tr>
										</thead>
										<tbody>
                      <?php $sql = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Service'"); if (mysql_num_rows($sql)) { while ($row = mysql_fetch_assoc($sql)) { ?>
                        <tr>
  												<th scope="row">
  													<?php echo $row['dataValueWrite'] ?>
  												</th>
  												<td>
                            <a href="<?php echo $pageDir.'&r='.securestring('encrypt',$row['did']) ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="View"> <i class="fa fa-eye"></i> </a>
                            <a href="#" data-toggle="modal" data-target="#add_main<?php echo $row['did'] ?>" class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Add Main"> <i class="fa fa-plus"></i> </a>
                            <a href="#" data-toggle="modal" data-target="#del_service<?php echo $row['did'] ?>" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete"> <i class="fa fa-trash"></i> </a>
  												</td>
  											</tr>
                      <?php
                      echo $modal->popnewMain($row['did']);
                      echo $modal->popdelSer($row['did'],$row['dataValueWrite']);
                      // echo $modal->popdelServices($row['did']);
                      } }else{ ?>
                        <tr>
                          <th colspan="2" style="text-align:center;">No Data</th>
                        </tr>
                      <?php } ?>
										</tbody>
									</table>
  							</div>
  						</div>
            </div>
            <?php if($dataCategory != ""){ ?>
              <div class="col-lg-8">
                <table class="table table-bordered table-hover">
  								<thead>
  									<tr style="background:rgba(149, 149, 149, 0.61);">
  										<th >
  											Value
  										</th>
  										<th>
  											Type
  										</th>
  										<th>
  											Action
  										</th>
  									</tr>
  								</thead>
  								<tbody>
                    <?php $sql = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Main' AND dataRefService= '$dataCategory'"); if (mysql_num_rows($sql)) { while ($row = mysql_fetch_assoc($sql)) { ?>
                      <tr style="background:rgba(176, 232, 233, 0.6);">
    										<th scope="row" style="width: 250px;">
    											<?php echo $row['dataValueWrite'] ?>
    										</th>
    										<td style="width: 120px;">
    											<?php echo $row['dataType'] ?>
    										</td>
    										<td>
                          <div class="row col-lg-12" style="padding:0px 20px;">
                            <div class="col-lg-8">
                              <a href="#" style="padding-left:20px;" data-toggle="modal" data-target="#edit_main<?php echo $row['did'] ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Edit"> <i class="fa fa-edit"></i> </a>
                              <a href="#" style="padding-left:20px;" data-toggle="modal" data-target="#add_sub<?php echo $row['did'] ?>" class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Add Child"> <i class="fa fa-plus"></i> </a>
                              <a href="#" style="padding-left:20px;" data-toggle="modal" data-target="#del_main<?php echo $row['did'] ?>" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete"> <i class="fa fa-trash"></i> </a>
                            </div>
                          </div>
    										</td>
    									</tr>
                      <?php
                      $did = $row['did'];
                      $sql2 = mysql_query("SELECT * FROM tbl_category WHERE dataRefMain = '$did' AND dataType = 'Child'");
                      if (mysql_num_rows($sql2)) {
                        while ($row2 = mysql_fetch_assoc($sql2)) {
                      ?>
                        <tr style="background:;">
      										<th scope="row" style="width: 250px; text-align:center;">
      											<?php echo $row2['dataValueWrite'] ?>
      										</th>
      										<td style="width: 120px;">
      											<?php echo $row2['dataType'] ?>
      										</td>
      										<td>
                            <div class="row col-lg-12" style="padding:0px 20px;">
                              <div class="col-lg-8">
                                <a href="#" style="padding-left:20px;" data-toggle="modal" data-target="#edit_sub<?php echo $row2['did'] ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Edit"> <i class="fa fa-edit"></i> </a>
                                <a href="#" style="padding-left:20px;" data-toggle="modal" data-target="#del_sub<?php echo $row2['did'] ?>" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete"> <i class="fa fa-trash"></i> </a>
                                <a href="<?php echo 'system.php?p='.securestring('encrypt',303); ?>" style="padding-left:20px;" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Form Manage"> <i class="fa fa-wpforms"></i> </a>
                              </div>
                            </div>
      										</td>
      									</tr>
                    <?php
                      echo $modal->popeditSub($row2['did'],$row2['dataValueWrite']);
                      echo $modal->popdelSub($row2['did'],$row2['dataValueWrite']);
                         }
                       }
                       echo $modal->popeditMain($row['did'],$row['dataValueWrite']);
                       echo $modal->popnewSub($row['did'],$row['dataRefService']);
                       echo $modal->popdelMain($row['did'],$row['dataValueWrite']);
                      }
                    }
                    ?>
  								</tbody>
  							</table>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  echo $modal->popnewServices();
?>
