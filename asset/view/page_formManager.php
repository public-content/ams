<?php
$page = securestring('decrypt',$_GET['p']);
$ref = securestring('decrypt',$_GET['r']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','1');
$pageDesc	= '';
$main->includephp('model','model_formManager');
$main->includephp('view/modal','modal_formManager');
$modal = new popFormManager();
$controller = new formManager();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);

if (isset($_POST['btn-add-new'])) {
  $task = $controller->saveFieldset($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','New Fieldset has been created');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['btn-edit'])) {
  $task = $controller->editFieldset($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','New Fieldset has been Update');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['btn-del'])) {
  $task = $controller->delFieldset($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','Fieldset has been deleted');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['add-option'])){
  $task = $controller->addOption($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','A group option has been added');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['edit-parent'])){
  $task = $controller->pushEdit($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','Parent Option has been Update');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['del-parent'])){
  $task = $controller->pushDel($_POST,'Parent');
  if ($task == 1) {
    $noty = getMessageNormal('success','Parent has been deleted');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['add-child'])){
  $task = $controller->addNewChild($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','Additional Child has been added');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['edit-child'])){
  $task = $controller->pushEdit($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','Fieldset has been deleted');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['del-child'])){
  $task = $controller->pushDel($_POST,'Child');
  if ($task == 1) {
    $noty = getMessageNormal('success','Fieldset has been deleted');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['bn'])){
  var_dump($_POST);
}

$alert = $noty;
?>

<style media="screen">
  .inactivate{
    display: none;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Form Manager <span style="color:red;"><?php if($ref != ''){ echo $controller->valueRef($ref); }  ?></span>
									<small>

									</small>
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body row">
            <div class="col-lg-12" >
              <form class="" action="" method="post">
                <div class="form-group m-form__group row">
										<label for="example-text-input" class="col-lg-2 col-form-label">
											Services
										</label>
										<div class="col-lg-4">
                      <select class="form-control m-input" id="serviceSelect" onchange="selectFunction()">
                        <option>
													select
												</option>
                        <?php $sql = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Service'");if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
                          <option <?php if($ref == $row['did']){print 'selected'; } ?> value="<?php echo securestring('encrypt',$row['did']); ?>">
                           <?php echo $row['dataValueWrite']; ?>
  												</option>
                        <?php } } ?>
											</select>
										</div>
									</div>
              </form>
              <?php if ($ref != '') { ?>
                <div class="col-lg-12 row" style="margin-top:40px;" id="first">
                  <div class="col-lg-3">
                    <a class="btn btn-success btn-md m-btn  m-btn m-btn--icon" style="width:100%;height:100px;color:white;" onclick="btnFunction('fu')">
											<span>
												<i class="fa fa-list-alt"></i>
												<span>
													Create form Unit
												</span>
											</span>
										</a>
                  </div>
                  <div class="col-lg-3">
                    <a class="btn btn-brand btn-md m-btn  m-btn m-btn--icon" style="width:100%;height:100px;color:white;" onclick="btnFunction('fc')">
											<span>
												<i class="fa fa-wpforms"></i>
												<span>
													Assign Form to category
												</span>
											</span>
										</a>
                  </div>
                  <div class="col-lg-3">
                    <a class="btn btn-warning btn-md m-btn  m-btn m-btn--icon" style="width:100%;height:100px;color:black;" onclick="btnFunction('so')">
											<span>
												<i class="fa fa-list-ul"></i>
												<span>
													Select Option
												</span>
											</span>
										</a>
                  </div>
                </div>
                <div class="col-lg-12 inactivate" style="margin-top:40px;" id="second">
                  <div class="m-portlet m-portlet--bordered m-portlet--unair">
  									<div class="m-portlet__head">
  										<div class="m-portlet__head-caption">
  											<div class="m-portlet__head-title">
  												<span class="m-portlet__head-icon">
                            <a href="#" title="Go Back" onclick="btnFunction('main')"><i class="fa fa-arrow-left"></i></a>
  												</span>
  												<h3 class="m-portlet__head-text">
  													Create form Unit
  												</h3>
  											</div>
  										</div>
  									</div>
  									<div class="m-portlet__body">
                      <table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>
														#
													</th>
													<th>
														Label Name
													</th>
													<th>
														Type
													</th>
													<th>
														Required
													</th>
													<th>
														Action
													</th>
												</tr>
											</thead>
											<tbody>
                        <?php $bil =1; $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE dataRefCategory = '$ref' AND dataType != 'CODE' AND active = 'Yes'"); if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
                          <tr>
  													<th scope="row">
  														<?php echo $bil ?>
  													</th>
  													<td>
  														<?php echo $row['dataValueLabel'] ?>
  													</td>
  													<td>
  														<?php echo $row['dataValueType'] ?>
  													</td>
  													<td>
  														<?php echo $row['dataValueRequired'] ?>
  													</td>
  													<td>
                              <a data-toggle="modal" data-target="#view_fieldset<?php echo $row['did'] ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="View"> <i class="fa fa-eye"></i> </a>
                              <a data-toggle="modal" data-target="#edit_fieldset<?php echo $row['did'] ?>" class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Edit"> <i class="fa fa-edit"></i> </a>
  														<a data-toggle="modal" data-target="#del_fieldset<?php echo $row['did'] ?>" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete"> <i class="fa fa-trash"></i> </a>
  													</td>
  												</tr>
                          <?php echo $modal->viewFieldset($row['did']); ?>
                          <?php echo $modal->editFieldset($row['did'],$row['dataValueLabel']); ?>
                          <?php echo $modal->delFieldset($row['did'],$row['dataValueLabel']); ?>

                        <?php $bil++; } } ?>
											</tbody>
										</table>
                    <div class="col-lg-12">
                      <div class="m-portlet__head-tools" style="text-align:right;">
                        <a class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" style="color:white;" data-toggle="modal" data-target="#add_fieldset">
                          <span>
                            <i class="la la-plus"></i>
                            <span>
                              Add
                            </span>
                          </span>
                        </a>
                      </div>
                    </div>
  									</div>
  								</div>
                </div>
                <div class="col-lg-12 inactivate" style="margin-top:40px;" id="third">
                  <div class="m-portlet m-portlet--bordered m-portlet--unair">
  									<div class="m-portlet__head">
  										<div class="m-portlet__head-caption">
  											<div class="m-portlet__head-title">
                          <span class="m-portlet__head-icon">
                            <a href="#" title="Go Back" onclick="btnFunction('main')"><i class="fa fa-arrow-left"></i></a>
  												</span>
  												<h3 class="m-portlet__head-text">
  													Assign Form to category
  												</h3>
  											</div>
  										</div>
  									</div>
  									<div class="m-portlet__body">
                      <div class="col-lg-12">
                        <div class="form-group m-form__group row">
      										<label class="col-form-label col-lg-3 col-sm-12">
      											Category
      										</label>
      										<div class="col-lg-4 col-md-9 col-sm-12">
      											<select class="form-control m-select2" id="m_select2_2" name="param" style="width:100%" onchange="postThis()">
                                <option value="">
                                  Choose
                                </option>
                                <?php $sqla = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Main' AND dataRefService = '$ref'"); if(mysql_num_rows($sqla)){while($rowa = mysql_fetch_assoc($sqla)){ $refdid = $rowa['did']?>
                                  <optgroup label="Main - <?php echo $rowa['dataValueWrite'] ?>">
                                    <?php $sqlb = mysql_query("SELECT * FROM tbl_category WHERE dataRefMain = '$refdid' AND dataRefService = '$ref'"); if(mysql_num_rows($sqlb)){while($rowb = mysql_fetch_assoc($sqlb)){ ?>
          													<option value="<?php echo $rowb['did'] ?>">
          														<?php echo $rowb['dataValueWrite'] ?>
                                    <?php }} ?>
          												</optgroup>
                                <?php }} ?>
                            </select>
                          </div>
    										</div>
                        <script type="text/javascript">
                          function postThis(){
                            var getRef = document.getElementById('m_select2_2').value;
                            var dataString = 'getRef='+ getRef;

                            $.ajax({
                              type:"post",
                              url:"asset/view/ajax/openTable.php",
                              data:dataString,
                              cache:false,
                              success: function(html){
                                document.getElementById('openTable').innerHTML = html;
                              }
                            });
                            return false;
                          }
                        </script>
                      </div>
                      <div class="col-lg-12" id="openTable">

                      </div>
  									</div>
  								</div>
                </div>
                <div class="col-lg-12 inactivate" style="margin-top:40px;" id="forth">
                  <div class="m-portlet m-portlet--bordered m-portlet--unair">
  									<div class="m-portlet__head">
  										<div class="m-portlet__head-caption">
  											<div class="m-portlet__head-title">
                          <span class="m-portlet__head-icon">
                            <a href="#" title="Go Back" onclick="btnFunction('main')"><i class="fa fa-arrow-left"></i></a>
  												</span>
  												<h3 class="m-portlet__head-text">
  													Select Option
  												</h3>
  											</div>
  										</div>
  									</div>
  									<div class="m-portlet__body">
                      <table class="table table-bordered m-table">
											<thead>
												<tr>
													<th>
														#
													</th>
													<th>
														Parent Value
													</th>
													<th>
														Child Value
													</th>
													<th>
														Action
													</th>
												</tr>
											</thead>
											<tbody>
                        <?php $bil=1; $sql = mysql_query("SELECT * FROM tbl_optionunit WHERE dataType = 'Parent'");if(mysql_num_rows($sql)){while($row = mysql_fetch_assoc($sql)){ $dataRefa = $row['did']?>
                          <tr style="background:rgba(206, 238, 234, 0.56);font-weight:800;">
  													<th scope="row">
  														<?php echo $bil ?>
  													</th>
  													<td colspan="1">
  													   <?php echo $row['dataValue'] ?>
  													</td>
  													<td colspan="1">

  													</td>
  													<td>
                              <a data-toggle="modal" data-target="#edit_parent<?php echo $row['did'] ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Edit"> <i class="fa fa-edit"></i> </a>
                              <a data-toggle="modal" data-target="#add_child<?php echo $row['did'] ?>" class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Add Child"> <i class="fa fa-plus"></i> </a>
                              <?php if($row['type'] != 'FIX'){ ?>
                                <a data-toggle="modal" data-target="#del_parent<?php echo $row['did'] ?>" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete"> <i class="fa fa-trash"></i> </a>
                              <?php } ?>
  													</td>
  												</tr>
                          <?php $slqc = mysql_query("SELECT * FROM tbl_optionunit WHERE dataType = 'Child' AND dataRef = '$dataRefa' "); if(mysql_num_rows($slqc)){ while($rowc = mysql_fetch_assoc($slqc)){ ?>
                            <tr>
    													<th scope="row">

    													</th>
    													<td colspan="1" >

    													</td>
    													<td colspan="1" style="background:rgba(206, 238, 216, 0.56);">
    													   <?php echo $rowc['dataValue'] ?>
    													</td>
    													<td>
                                <a data-toggle="modal" data-target="#edit_child<?php echo $rowc['did'] ?>" class="btn btn-outline-info m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Edit"> <i class="fa fa-edit"></i> </a>
                                <a data-toggle="modal" data-target="#del_child<?php echo $rowc['did'] ?>" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete"> <i class="fa fa-trash"></i> </a>
    													</td>
    												</tr>
                          <?php
                          echo $modal->editChild($rowc['did'],$rowc['dataValue']);
                          echo $modal->deleteChild($rowc['did'],$rowc['dataValue']);
                          } } $bil++;
                          echo $modal->editParent($row['did'],$row['dataValue']);
                          echo $modal->addChild($row['did'],$row['dataValue']);
                          echo $modal->deleteParent($row['did'],$row['dataValue']);
                          } } else { ?>
                            <tr>
    													<th scope="row" colspan="4" style="text-align:center;">
    														No data
    													</th>
    												</tr>
                          <?php } ?>
											</tbody>
										</table>
                    <div class="col-lg-12">
                      <div class="m-portlet__head-tools" style="text-align:right;">
                        <a class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" style="color:white;" data-toggle="modal" data-target="#add_option">
                          <span>
                            <i class="la la-plus"></i>
                            <span>
                              Add
                            </span>
                          </span>
                        </a>
                      </div>
                    </div>
  									</div>
  								</div>
                </div>
              <?php } ?>
            </div>
					</div>
				</div>
      </div>
    </div>
  </div>
  <!-- <p id="demo"></p> -->
</div>
<?php echo $modal->addFieldset($ref); ?>
<?php echo $modal->addOption(); ?>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script>
$("#valueOption").hide();
$("#valueOptionEdit").hide();
$("#selectThiss").hide();
function selectFunction() {
    var x = document.getElementById("serviceSelect").value;
    var link = "<?php echo $pageDir ?>" ;
    location.replace(link + "&r=" + x);

}
function btnFunction(x) {
  var so = document.getElementById("forth");
  var fc = document.getElementById("third");
  var fu = document.getElementById("second");
  if (x == 'fu') {
    fu.classList.remove("inactivate");
    so.classList.add("inactivate");
    fc.classList.add("inactivate");
  }else if (x == 'fc') {
    fc.classList.remove("inactivate");
    so.classList.add("inactivate");
    fu.classList.add("inactivate");
  }else if (x == 'so') {
    so.classList.remove("inactivate");
    fc.classList.add("inactivate");
    fu.classList.add("inactivate");
  }else if (x == 'main') {
    so.classList.add("inactivate");
    fc.classList.add("inactivate");
    fu.classList.add("inactivate");
  }else if (x == 'new') {
    so.classList.remove("inactivate");
    fc.classList.add("inactivate");
    fu.classList.add("inactivate");
  }
}
function myselection(){
  var x = document.getElementById("valueType").value;
  if (x == "select") {
    $("#valueOption").show();
  }else{
    $("#valueOption").hide();
  }
}
function myselection2(){
  var y = document.getElementById("valueTypeEdit").value;
  if (y == "select") {
    $("#valueOptionEdit").show();
  }else{
    $("#valueOptionEdit").hide();
  }
}
function takeValue(){
  var x = document.getElementById('selValue').value;
  var dataA = document.getElementById('getDataA').value;
  var dataB = document.getElementById('getDataB').value;
  var dataString = { refField : x, refChildCategory : dataA, refService : dataB };

  $.ajax({
    type:"post",
    url:"asset/view/ajax/openTable.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('openTable').innerHTML = html;
    }
  });
  return false;
}
function delForm(did,dataA,dataB){
  var dataString = { deldid : did, refChildCategory : dataA, refService : dataB };

  $.ajax({
    type:"post",
    url:"asset/view/ajax/openTable.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('openTable').innerHTML = html;
    }
  });
  return false;
}
</script>
<script src="js/select2.js" type="text/javascript"></script>
