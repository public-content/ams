<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageNameFile	= pageInfo($page,'pageFileName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_assetReminder');
$controller = new reminderer();
$main->includephp('view/modal','modal_assetReminder');
$modal = new reminderModal();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);

  if(isset($_POST['btn-change'])){
    // var_dump($_POST);
    $task = $controller->sendChanges($_POST);
    if($task == 1){
      $noty = getMessageNormal('success','The changes has been saved');
    }else{
      $noty = getMessageNormal('danger',$task);
    }
  }elseif(isset($_POST['btn-delete'])){
    // var_dump($_POST);
    $task = $controller->sendDelete($_POST);
    if($task == 1){
      $noty = getMessageNormal('success','The reminder has been deleted');
    }else{
      $noty = getMessageNormal('danger',$task);
    }
  }

$alert = $noty;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--unair">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<h3 class="m-portlet__head-text">
    							Reminder list
    						</h3>
    					</div>
    				</div>
    			</div>
    			<div class="m-portlet__body">
            <div class="m-section">
    					<div class="m-section__content">
    						<table class="table table-bordered table-hover">
    						  	<thead>
    						    	<tr>
    						      		<th>#</th>
                          <th>Code</th>
    						      		<th>Responsible User</th>
    						      		<th>Reminder</th>
    						      		<th>Alert Date</th>
                          <th>Remarks</th>
                          <th>Date Created</th>
    						      		<th>Action</th>
    						    	</tr>
    						  	</thead>
    						  	<tbody>
                      <?php $count = 1; $sql = mysql_query("SELECT * FROM tbl_reminder WHERE email = '$byWho' AND status != 'Delete' ORDER BY did DESC"); if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ $did = $row['did'];?>
                        <tr>
      							      	<td scope="row"><?php echo $count; ?></td>
                            <td><?php echo $controller->getCode($row['dataCode']); ?></td>
      							      	<td><?php echo $controller->getUserName($row['email']); ?></td>
                            <td><?php echo $row['dateReminder']; ?></td>
                            <td><?php echo $row['dateAlert']; ?></td>
                            <td><?php echo $row['remarks']; ?></td>
                            <td><?php echo $row['dateCreated']; ?></td>
      							      	<td>
                              <?php if($row['status'] != 'Read' || $row['status2'] == 'Read'){ ?>
                                <a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Edit" data-toggle="modal" data-target="#r_edit<?php echo $did; ?>"> <i class="la la-edit"></i> </a>
                              <?php } ?>
                              <a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Delete" data-toggle="modal" data-target="#r_delete<?php echo $did; ?>"> <i class="la la-trash"></i> </a>
                            </td>
      						    	</tr>
                        <?php echo $modal->popEdit($did); ?>
                        <?php echo $modal->deletePop($did); ?>
                      <?php $count++; } }else{ ?>
                        <tr>
      							      	<td scope="row" colspan="8" style="text-align:center;">No Reminder</td>
      						    	</tr>
                      <?php } ?>
    						  	</tbody>
    						</table>
    					</div>
    				</div>
    			</div>
    		</div>
      </div>
    </div>
  </div>
</div>
