<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_profileDetails');
$controller = new profileDetails();
$userId = securestring('decrypt',$_SESSION['userid']);

if(isset($_POST['saveChange'])){
  // var_dump($_POST);
  $task = $controller->saveChangeUser($userId,$_POST);
  if($task == 1){
    $noty = getMessageNormal('success','Done');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}elseif(isset($_POST['uploadImage'])){
  $target_dir = "images/profile/";
  $target_file = $target_dir . basename($_FILES["imgtoupload"]["name"]);
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  $check = getimagesize($_FILES["imgtoupload"]["tmp_name"]);
  if($check !== false) {
      $msg = "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
  } else {
      $msg = "File is not an image.";
      $uploadOk = 0;
  }

  if ($_FILES["fileToUpload"]["size"] > 500000) {
    $msg = "Sorry, your file is too large.";
    $uploadOk = 0;
  }

  if (file_exists($target_file)) {
    $msg = "Sorry, file already exists.";
    $uploadOk = 0;
  }

  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
  && $imageFileType != "gif" && $imageFileType != "GIF" ) {
      $msg = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
  }

  if ($uploadOk == 0) {
    $noty = getMessageNormal('danger',$msg.''.$uploadOk);
// if everything is ok, try to upload file
  } else {
      if (move_uploaded_file($_FILES["imgtoupload"]["tmp_name"], $target_file)) {
        $sql = mysql_query("UPDATE tbl_user SET img = '$target_file' WHERE did = '$userId'");
        if(!$sql){
          $noty = getMessageNormal('danger',mysql_error());
        }else {
          $noty = getMessageNormal('success','Profile image has been changed');
          $_SESSION['img'] = $target_file;
        }

      } else {
        $noty = getMessageNormal('danger',$msg);
      }
  }
}

$alert = $noty;
?>
<style media="screen">
  .edit-img{
    position: absolute;
    bottom: -5px;
    right:2px;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12">
        <?php if($alert != ''){echo $alert;}?>
      </div>
      <div class="col-lg-12">
						<div class="row">
							<div class="col-xl-3 col-lg-4">
								<div class="m-portlet m-portlet--full-height  ">
									<div class="m-portlet__body">
										<div class="m-card-profile">
											<div class="m-card-profile__title m--hide">
												Your Profile
											</div>
											<div class="m-card-profile__pic">
												<div class="m-card-profile__pic-wrapper" style="position:relative;">
													<img src="<?php echo $controller->getUserDetails('img',$userId); ?>" alt=""/>
                          <span class="edit-img">
                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#profileImg">
  														<i class="fa fa-edit"></i>
  													</a>
                          </span>
												</div>
											</div>
											<div class="m-card-profile__details">
												<span class="m-card-profile__name">
													<?php echo $controller->getUserDetails('name',$userId); ?>
												</span>
												<a href="" class="m-card-profile__email m-link">
													<?php echo $controller->getUserDetails('department',$userId); ?>
												</a>
                        <br>
												<a href="" class="m-card-profile__email m-link">
													<?php echo $controller->getUserDetails('email',$userId); ?>
												</a>
											</div>
										</div>
										<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
											<li class="m-nav__separator m-nav__separator--fit"></li>
											<li class="m-nav__section m--hide">
												<span class="m-nav__section-text">
													Section
												</span>
											</li>
											<li class="m-nav__item">
												<a href="#" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-profile-1"></i>
													<span class="m-nav__link-title">
														<span class="m-nav__link-wrap">
															<span class="m-nav__link-text">
																My Profile
															</span>
														</span>
													</span>
												</a>
											</li>
											<!-- <li class="m-nav__item">
												<a href="../header/profile&amp;demo=default.html" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-alert-1"></i>
													<span class="m-nav__link-text">
														Notification
													</span>
												</a>
											</li>
											<li class="m-nav__item">
												<a href="../header/profile&amp;demo=default.html" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-chat-1"></i>
													<span class="m-nav__link-text">
														User Logs
													</span>
												</a>
											</li>
											<li class="m-nav__item">
												<a href="../header/profile&amp;demo=default.html" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-lock"></i>
													<span class="m-nav__link-text">
														Change Password
													</span>
												</a>
											</li> -->
											<li class="m-nav__item">
												<a href="./index.php?action=<?php print securestring('encrypt','logout');?>" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-logout"></i>
													<span class="m-nav__link-text">
														Log Out
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xl-9 col-lg-8">
								<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
									<div class="tab-content" id="content">
                    <form class="m-form m-form--fit m-form--label-align-right" method="post">
												<div class="m-portlet__body">
													<div class="form-group m-form__group m--margin-top-10 m--hide">
														<div class="alert m-alert m-alert--default" role="alert">
															The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-10 ml-auto">
															<h3 class="m-form__section">
															  Personal Details
															</h3>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-2 col-form-label">
															Full Name
														</label>
														<div class="col-7">
															<input class="form-control m-input" type="text" name="name" value="<?php echo $controller->getUserDetails('name',$userId); ?>">
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-2 col-form-label">
															Department
														</label>
														<div class="col-7">
                              <p class="form-control-static" style="margin-top:6px;font-weight:400;">
  															<?php echo $controller->getUserDetails('department',$userId); ?>
      												</p>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-2 col-form-label">
															Email Address
														</label>
														<div class="col-7">
															<input class="form-control m-input" type="email" name="email" value="<?php echo $controller->getUserDetails('email',$userId); ?>">
															<span class="m-form__help">
															  Purpose for reminder or notify the user through email.
															</span>
														</div>
													</div>
                          <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                          <div class="form-group m-form__group row">
														<div class="col-10 ml-auto">
															<h3 class="m-form__section">
																Login Details
															</h3>
														</div>
													</div>
                          <div class="form-group m-form__group row">
														<label for="example-text-input" class="col-2 col-form-label">
															Username
														</label>
														<div class="col-7">
                              <p class="form-control-static" style="margin-top:6px;font-weight:400;">
  															<?php echo $controller->getUserDetails('username',$userId); ?>
      												</p>
														</div>
													</div>
                          <div class="form-group m-form__group row">
														<label for="example-text-input" class="col-2 col-form-label">
															Password
														</label>
														<div class="col-7">
															<input class="form-control m-input" type="password" value="" name="password" autocomplete="OFF">
                              <span class="m-form__help">
															  Fill this input if want to change password.
															</span>
														</div>
													</div>
												</div>
												<div class="m-portlet__foot m-portlet__foot--fit">
													<div class="m-form__actions">
														<div class="row">
															<div class="col-2"></div>
															<div class="col-7">
																<button type="submit" name="saveChange" class="btn btn-accent m-btn m-btn--air m-btn--custom">
																	Save changes
																</button>
																&nbsp;&nbsp;
																<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
																	Cancel
																</button>
															</div>
														</div>
													</div>
												</div>
											</form>
									</div>
								</div>
							</div>
						</div>
      </div>
    </div>
  </div>
</div>

<!--begin::Modal-->
						<div class="modal fade" id="profileImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-md" role="document">
							  <form class="" action="" method="post" enctype="multipart/form-data">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											Profile Image
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      <div class="form-group m-form__group">
        								<label for="exampleInputEmail1">
        									File Browser
        								</label>
        								<div></div>
        								<div class="custom-file">
        									<input type="file" class="custom-file-input" id="customFile" name="imgtoupload">
        									<label class="custom-file-label" for="customFile">
        										Choose file
        									</label>
        								</div>
        							</div>
  									</div>
  									<div class="modal-footer">
  										<button type="button" class="btn btn-secondary" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="uploadImage" class="btn btn-primary">
  											Save Change
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
						<!--end::Modal-->
