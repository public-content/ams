<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_dashboard');
$controller = new Dashboard();
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    <?php echo $pageTitle; ?>
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo $pageDir ?>" class="m-nav__link">
                            <span class="m-nav__link-text">
                                <?php echo $pageName; ?>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div>
                <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <?php $alertMe = $alert; ?>
            </div>
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Operating System Report
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="form-group m-form__group col-lg-6">
                                <label for="exampleSelect1">
                                    Operating System
                                </label>
                                <select class="form-control m-input" id="thisCategory" onchange="changeCategory()">
                                    <option>
                                        Select
                                    </option>
                                    <?php $sql = mysql_query("SELECT dataValue FROM tbl_optionunit WHERE dataRef = '78'"); if(mysql_num_rows($sql)){while($row = mysql_fetch_assoc($sql)){ ?>
                                    <option value="<?php echo $row['dataValue'] ?>">
                                        <?php echo $row['dataValue']; ?>
                                    </option>
                                    <?php }} ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group col-lg-6" id="osDetails">

                            </div>
                            <div class="col-lg-12" id="openTable">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script type="text/javascript">
    function changeCategory() {
        var category = document.getElementById("thisCategory").value;
        var dataString = {
            category: category
        };
        // document.getElementById('osDetails').innerHTML = '';
        document.getElementById('osDetails').innerHTML = "<div class='col-lg-12' style='text-align:center;'><img src='images/file_more/more/loading.gif' alt='' width='100'></div>";

        $.ajax({
            type: "post",
            url: "asset/view/ajax/winReport.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('osDetails').innerHTML = html;
            }
        });
        return false;
    }

    function changeComponent() {
        var component = document.getElementById("thisComponent").value;
        var category = document.getElementById("thisCategory").value;
        var dataString = {
            categorys: category,
            component: component
        };

        // document.getElementById('openTable').innerHTML = '';
        document.getElementById('openTable').innerHTML = "<div class='col-lg-12' style='text-align:center;'><img src='images/file_more/more/loading.gif' alt='' width='200'></div>";

        $.ajax({
            type: "post",
            url: "asset/view/ajax/winReport.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('openTable').innerHTML = html;
            }
        });
        return false;
    }
</script>