<?php
  class reminderModal{

    public function getReminder($column,$did){
      $sql = mysql_query("SELECT $column FROM tbl_reminder WHERE did = '$did'");
      $row = mysql_fetch_assoc($sql);
      return $row[$column];
    }

    public function popEdit($did){ ?>
      <div class="modal fade" id="r_edit<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  			<div class="modal-dialog " role="document">
  			  <form method="post">
            <div class="modal-content">
    					<div class="modal-header">
    						<h5 class="modal-title" id="exampleModalLabel">
    							New Reminder for <span style="color:red;"><?php echo $r ?></span>
    						</h5>
    						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    							<span aria-hidden="true">
    								&times;
    							</span>
    						</button>
    					</div>
              <div class="modal-body">
                <div class="form-group m-form__group">
      						<label for="whom">Reminds Who</label>
                  <select class="form-control m-input m-input--solid" id="whom" name="whom" required>
                    <option value="">Choose</option>
                    <?php $son = mysql_query("SELECT did,name FROM tbl_user"); if(mysql_num_rows($son)){ while($bon = mysql_fetch_assoc($son)){ ?>
                      <option <?php if($this->getReminder('email',$did) == $bon['did']){print 'selected';} ?> value="<?php echo $bon['did'] ?>"><?php echo $bon['name'] ?></option>
                    <?php } } ?>
      						</select>
                  <input type="hidden" name="dataCode" value="<?php echo $this->getReminder('dataCode',$did); ?>" required>
                  <input type="hidden" name="did" value="<?php echo $did ?>" required>
      					</div>
                <div class="form-group m-form__group">
      						<label for="dateActual">Actual date</label>
        					<input type="date" class="form-control m-input m-input--solid" id="dateActual" name="dateActual" value="<?php echo $this->getReminder('dateReminder',$did); ?>" required>
      					</div>
                <div class="form-group m-form__group">
      						<label for="notifyMe">Notify me</label>
                  <select class="form-control m-input m-input--solid" id="notifyMe" name="notifyMe" required>
      							<option value="">Choose</option>
                    <option <?php if($this->getReminder('notifyValue',$did) == '-3 months'){ print 'selected'; } ?> value="-3 months">3 Months Before</option>
      							<option <?php if($this->getReminder('notifyValue',$did) == '-2 months'){ print 'selected'; } ?> value="-2 months">2 Months Before</option>
      							<option <?php if($this->getReminder('notifyValue',$did) == '-1 month'){ print 'selected'; } ?> value="-1 month">1 Month Before</option>
      							<option <?php if($this->getReminder('notifyValue',$did) == '-2 weeks'){ print 'selected'; } ?> value="-2 weeks">2 Week Before</option>
      						</select>
      					</div>
                <div class="form-group m-form__group">
      						<label for="remarks">Label/ Remarks</label>
        					<textarea class="form-control m-input m-input--solid" id="remarks" name="remarks" rows="3"><?php echo $this->getReminder('remarks',$did); ?></textarea>
      					</div>
    					</div>
    					<div class="modal-footer">
    						<button type="button" class="btn btn-secondary" data-dismiss="modal">
    							Close
    						</button>
    						<button type="submit" name="btn-change" class="btn btn-brand">
    							<i class="la la-clock-o"></i> Save
    						</button>
    					</div>
    				</div>
          </form>
  			</div>
  		</div>
    <?php }

    public function deletePop($did){ ?>
      <div class="modal fade" id="r_delete<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Delete this reminder</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Do you wish to delete this reminder ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <form method="post">
                <input type="hidden" name="did" value="<?php echo $did ?>">
                <button type="submit" name="btn-delete" class="btn btn-danger">Yes</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php }

  }
?>
