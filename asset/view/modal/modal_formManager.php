<?php
class popFormManager{
  public function viewFieldset($did){ ?>
    <div class="modal fade" id="view_fieldset<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Preview <span style="color:red;"><?php echo $this->findValue($did,'dataValueLabel'); ?></span>
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						<?php echo $this->showFieldset($did); ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
						<!-- <button type="button" class="btn btn-primary">
							Save changes
						</button> -->
					</div>
				</div>
			</div>
		</div>
  <?php }

  public function editFieldset($did,$dataValue){
    $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    ?>
    <div class="modal fade" id="edit_fieldset<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Edit Fieldset <span style="color:red"><?php echo $dataValue ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              <input type="hidden" name="dataRefEdit" value="<?php echo $did ?>">
              <div class="form-group m-form__group">
								<label for="valueLabelEdit">
									Label <span style="color:red">*</span>
								</label>
								<input type="text" class="form-control m-input" id="valueLabelEdit" name="valueLabelEdit" value="<?php echo $row['dataValueLabel'] ?>" required>
							</div>
              <div class="form-group m-form__group">
								<label for="valuePlaceholderEdit">
									Placeholder
								</label>
								<input type="text" class="form-control m-input" id="valuePlaceholderEdit" name="valuePlaceholderEdit" value="<?php echo $row['dataValuePlaceholder'] ?>">
							</div>
              <div class="form-group m-form__group">
								<label for="valueTypeEdit">
									Type <span style="color:red">*</span>
								</label>
                <div class="col-lg-12 row">
                  <select class="form-control m-input col-lg-5" name="valueTypeEdit" id="valueTypeEdit" onchange="myselection2()">
                    <option <?php if($row['dataValueType'] == "text"){ print 'selected'; } ?> value="text">
      								text
      							</option>
                    <option <?php if($row['dataValueType'] == "number"){ print 'selected'; } ?> value="number">
      								number
      							</option>
                    <option <?php if($row['dataValueType'] == "date"){ print 'selected'; } ?> value="date">
      								date
      							</option>
                    <option <?php if($row['dataValueType'] == "checkbox"){ print 'selected'; } ?> value="checkbox">
      								checkbox
      							</option>
                    <option <?php if($row['dataValueType'] == "select"){ print 'selected'; } ?> value="select">
      								select
      							</option>
                    <option <?php if($row['dataValueType'] == "file browser"){ print 'selected'; } ?> value="file browser">
      								file browser
      							</option>
                  </select>
                  <div class="col-lg-2">

                  </div>
                  <select class="form-control m-input col-lg-5" name="valueOptionEdit" id="valueOptionEdit">
                    <option value="">
      								select
      							</option>
                    <?php $sqlop = mysql_query("SELECT * FROM tbl_optionunit WHERE dataType = 'parent'");if(mysql_num_rows($sqlop)){ while($rowop = mysql_fetch_assoc($sqlop)){ ?>
                    <option <?php if($row['dataValueDropdown'] == $rowop['did'] ){ print 'selected'; } ?> value="<?php echo $rowop['did'] ?>">
        							<?php echo $rowop['dataValue'] ?>
        						</option>
                    <?php } } ?>
                  </select>
                </div>
							</div>
              <div class="form-group m-form__group">
								<label for="valueRequired">
									Required <span style="color:red">*</span>
								</label>
                <div class="m-checkbox-list">
                  <label class="m-checkbox">
  								<input type="checkbox" <?php if($row['dataValueRequired'] == "on"){print 'checked'; } ?> name="valueRequiredEdit">
                  <span></span>
                  </label>
                </div>
							</div>
  					</div>
  					<div class="modal-footer">
  						<button type="reset" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="btn-edit" class="btn btn-brand">
  							<i class="la la-edit"></i> Edit
  						</button>
  					</div>
  				</div>
        </form>
			</div>
		</div>
  <?php }

  public function delFieldset($did,$dataValue){ ?>
    <div class="modal fade" id="del_fieldset<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <form class="" action="" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Delete Fieldset <span style="color:red"><?php echo $dataValue ?></span>
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                  &times;
                </span>
              </button>
            </div>
            <div class="modal-body">
              Do you wish to delete this Fieldset
            </div>
            <div class="modal-footer">
              <input type="hidden" name="did" value="<?php echo $did; ?>">
              <button type="button" class="btn btn-link" data-dismiss="modal">
                Close
              </button>
              <button type="submit" name="btn-del" class="btn btn-danger">
                <i class="la la-trash"></i> Yes
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  <?php }

  public function addFieldset($dataRef){ ?>
    <div class="modal fade" id="add_fieldset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							New Fieldset
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              <input type="hidden" name="dataRef" value="<?php echo $dataRef ?>">
              <div class="form-group m-form__group">
								<label for="valueLabel">
									Label <span style="color:red">*</span>
								</label>
								<input type="text" class="form-control m-input" id="valueLabel" name="valueLabel" required>
							</div>
              <!-- <div class="form-group m-form__group">
								<label for="valueName">
									Name <span style="color:red">*</span>
								</label>
								<input type="text" class="form-control m-input" id="valueName" name="valueName" required>
							</div> -->
              <div class="form-group m-form__group">
								<label for="valueName">
									Placeholder
								</label>
								<input type="text" class="form-control m-input" id="valuePlaceholder" name="valuePlaceholder">
							</div>
              <div class="form-group m-form__group">
								<label for="valueType">
									Type <span style="color:red">*</span>
								</label>
                <div class="col-lg-12 row">
                  <select class="form-control m-input col-lg-5" name="valueType" id="valueType" onchange="myselection()">
                    <option value="text">
      								text
      							</option>
                    <option value="number">
      								number
      							</option>
                    <option value="date">
      								date
      							</option>
                    <option value="checkbox">
      								checkbox
      							</option>
                    <option value="select">
      								select
      							</option>
                    <option value="file browser">
      								file browser
      							</option>
                  </select>
                  <div class="col-lg-2">

                  </div>
                  <select class="form-control m-input col-lg-5" name="valueOption" id="valueOption" onchange="btnFunction('new')">
                    <option value="">
      								select
      							</option>
                    <?php $sqlop = mysql_query("SELECT * FROM tbl_optionunit WHERE dataType = 'parent'");if(mysql_num_rows($sqlop)){ while($rowop = mysql_fetch_assoc($sqlop)){ ?>
                    <option value="<?php echo $rowop['did'] ?>">
        							<?php echo $rowop['dataValue'] ?>
        						</option>
                    <?php } } ?>
                    <option value="new">
      								new **
      							</option>
                  </select>
                </div>
							</div>
              <div class="form-group m-form__group">
								<label for="valueRequired">
									Required <span style="color:red">*</span>
								</label>
                <div class="m-checkbox-list">
                  <label class="m-checkbox">
  								<input type="checkbox" name="valueRequired">
                  <span></span>
                  </label>
                </div>
							</div>
  					</div>
  					<div class="modal-footer">
  						<button type="reset" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="btn-add-new" class="btn btn-brand">
  							<i class="la la-plus"></i> Add New
  						</button>
  					</div>
  				</div>
        </form>
			</div>
		</div>
  <?php }

  public function findValue($did,$column){
    $sql = mysql_query("SELECT $column FROM tbl_fieldsetunit WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row[$column];
  }

  public function showFieldset($did){
    $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    $type = $row['dataValueType'];
    $drop = $row['dataValueDropdown'];
    if ($row['dataValueRequired'] == "on") {
      $required = "<span style='color:red;'>*</span>";
    }else{
      $required = "";
    }

    if ($type == "text" || $type == "date" || $type == "number") { ?>
      <div class="form-group m-form__group row">
        <label for="<?php echo $row['dataValueName'] ?>" class="col-lg-4 col-form-label">
          <?php echo $row['dataValueLabel'] ?> <?php echo $required; ?>
        </label>
        <div class="col-lg-8">
          <input class="form-control m-input" type="<?php echo $type ?>" placeholder="<?php echo $row['dataValuePlaceholder'] ?>" name="<?php echo $row['dataValueName'] ?>" id="<?php echo $row['dataValueName'] ?>">
        </div>
      </div>
    <?php }elseif($type == "checkbox"){ ?>
        <div class="form-group m-form__group row">
          <label for="<?php echo $row['dataValueName'] ?>" class="col-lg-4 col-form-label">
            <?php echo $row['dataValueLabel'] ?> <?php echo $required; ?>
          </label>
          <div class="col-lg-8">
            <label class="m-checkbox">
							<input type="checkbox" name="<?php echo $row['dataValueName'] ?>">
							<span></span>
						</label>
          </div>
        </div>
      <?php }elseif($type == "select"){ ?>
        <div class="form-group m-form__group row">
          <label for="<?php echo $row['dataValueName'] ?>" class="col-lg-4 col-form-label">
            <?php echo $row['dataValueLabel'] ?> <?php echo $required; ?>
          </label>
          <div class="col-lg-8">
            <select class="form-control m-input" name="<?php echo $row['dataValueName'] ?>" id="<?php echo $row['dataValueName'] ?>">
							<option>
								select
							</option>
              <?php $sqlop = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '$drop'");if(mysql_num_rows($sqlop)){ while($rowop = mysql_fetch_assoc($sqlop)){ ?>
                <option value="<?php echo $rowop['dataValue'] ?>">
                  <?php echo $rowop['dataValue']; ?>
                </option>
              <?php } } ?>
						</select>
          </div>
        </div>
      <?php }elseif($type == "file browser"){ ?>
        <div class="form-group m-form__group row">
          <label for="<?php echo $row['dataValueName'] ?>" class="col-lg-4 col-form-label">
            <?php echo $row['dataValueLabel'] ?> <?php echo $required; ?>
          </label>
          <div class="col-lg-8">
            <div class="custom-file">
							<input type="file" class="custom-file-input" id="customFile">
							<label class="custom-file-label" for="customFile">
								Choose file
							</label>
						</div>
          </div>
        </div>
      <?php
    }
  }

  public function addOption(){ ?>
    <div class="modal fade" id="add_option" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Add Option
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              <div class="form-group m-form__group">
								<label for="valueParent">
									Parent Option <span style="color:red">*</span>
								</label>
								<input type="text" class="form-control m-input" id="valueParent" name="valueParent" required>
							</div>
              <div class="form-group m-form__group">
								<div class="col-lg-12 row">
                  <div class="col-lg-4">
                    <input type="text" class="form-control m-input" id="numRows" name="" value="4" required>
                  </div>
                  <label for="numRows" class="col-lg-2 col-form-label">
										Rows
									</label>
                  <div class="col-lg-4">
                    <a class="btn btn-metal" onclick="sendThis()" style="color:black;">
        							Go
        						</a>
                  </div>
                </div>
							</div>
              <div class="" id="result">

              </div>
              <input type="hidden" name="totalrow" id="totalrow" value="">
  					</div>
            <script type="text/javascript">
              var x = document.getElementById("numRows").value;
              var field = "";
              var i;
              for ( i = 1; i <= x; i++) {
                field += "<div class='form-group m-form__group row'> <label for='example-text-input' class='col-lg-4 col-form-label'> Child Value </label> <div class='col-lg-8'> <input class='form-control m-input' type='text' name='child"+ i +"' id='example-text-input'> </div> </div>";
                //field += i;
              }
              document.getElementById("result").innerHTML = field;
              document.getElementById("totalrow").value = x;

              function sendThis(){
                var x = document.getElementById("numRows").value;
                var field = "";
                var i;
                for ( i = 1; i <= x; i++) {
                  field += "<div class='form-group m-form__group row'> <label for='example-text-input' class='col-lg-4 col-form-label'> Child Value </label> <div class='col-lg-8'> <input class='form-control m-input' type='text' name='child"+ i +"' id='example-text-input'> </div> </div>";
                  //field += i;
                }
                document.getElementById("result").innerHTML = field;
                document.getElementById("totalrow").value = x;
              }
            </script>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="add-option" class="btn btn-brand">
  							<i class="la la-plus"></i> Add
  						</button>
  					</div>
  				</div>
        </form>
			</div>
		</div>
  <?php }

  public function editChild($did,$dataValue){ ?>
    <div class="modal fade" id="edit_child<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Edit Child <span style="color:red"><?php echo $dataValue ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              <div class="form-group m-form__group">
								<label for="value">
									Child Option <span style="color:red">*</span>
								</label>
                <input type="text" class="form-control m-input" id="value" name="value" value="<?php echo $dataValue ?>" required>
								<input type="hidden" class="form-control m-input" id="did" name="did" value="<?php echo $did ?>" required>
							</div>
            </div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="edit-child" class="btn btn-info">
  							<i class="fa fa-edit"></i> Edit
  						</button>
  					</div>
  				</div>
        </form>
			</div>
    </div>
  <?php }
  public function deleteChild($did,$dataValue){ ?>
    <div class="modal fade" id="del_child<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Delete Child <span style="color:red"><?php echo $dataValue ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              Do you wish to delete this ?
              <input type="hidden" name="did" value="<?php echo $did ?>">
            </div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="del-child" class="btn btn-danger">
  							<i class="fa fa-trash"></i> Delete
  						</button>
  					</div>
  				</div>
        </form>
			</div>
    </div>

  <?php }
  public function editParent($did,$dataValue){ ?>
    <div class="modal fade" id="edit_parent<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Edit Parent <span style="color:red"><?php echo $dataValue ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              <div class="form-group m-form__group">
								<label for="value">
									Parent Option <span style="color:red">*</span>
								</label>
                <input type="text" class="form-control m-input" id="value" name="value" value="<?php echo $dataValue ?>" required>
								<input type="hidden" class="form-control m-input" id="did" name="did" value="<?php echo $did ?>" required>
							</div>
            </div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="edit-parent" class="btn btn-info">
  							<i class="fa fa-edit"></i> Edit
  						</button>
  					</div>
  				</div>
        </form>
			</div>
    </div>
  <?php }
  public function addChild($did,$dataValue){ ?>
    <div class="modal fade" id="add_child<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form class="" action="" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Add Child for <span style="color:red;"><?php echo $dataValue ?></span>
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                  &times;
                </span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group m-form__group">
                <label for="valueParent">
                  Parent Option <span style="color:red">*</span>
                </label>
                <input type="text" class="form-control m-input" id="valueParent" name="valueParent" value="<?php echo $dataValue ?>" readonly>
                <input type="hidden" class="form-control m-input" id="did" name="did" value="<?php echo $did ?>" readonly>
              </div>
              <div class="form-group m-form__group">
                <label for="valueParent">
                </label>
              </div>
              <div class="form-group m-form__group">
                <label for="childValue1">
                  Child Value 1
                </label>
                <input type="text" class="form-control m-input" id="childValue1" name="childValue1" value="">
              </div>
              <div class="form-group m-form__group">
                <label for="childValue2">
                  Child Value 2
                </label>
                <input type="text" class="form-control m-input" id="childValue2" name="childValue2" value="">
              </div>
              <div class="form-group m-form__group">
                <label for="childValue3">
                  Child Value 3
                </label>
                <input type="text" class="form-control m-input" id="childValue3" name="childValue3" value="">
              </div>
              <div class="form-group m-form__group">
                <label for="childValue4">
                  Child Value 4
                </label>
                <input type="text" class="form-control m-input" id="childValue4" name="childValue4" value="">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-link" >
                reset
              </button>
              <button type="submit" name="add-child" class="btn btn-brand">
                <i class="la la-plus"></i> Add
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  <?php }
  public function deleteParent($did,$dataValue){ ?>
    <div class="modal fade" id="del_parent<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<form class="" action="" method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Delete Parent <span style="color:red"><?php echo $dataValue ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              Do you wish to delete this ?
              <input type="hidden" name="did" value="<?php echo $did ?>">
            </div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-link" >
  							reset
  						</button>
  						<button type="submit" name="del-parent" class="btn btn-danger">
  							<i class="fa fa-trash"></i> Delete
  						</button>
  					</div>
  				</div>
        </form>
			</div>
    </div>
  <?php }

  public function viewForm(){ ?>

  <?php }

}
?>
