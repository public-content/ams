<?php
class popProfile{

  public function getServices($dataCode){
    $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE datacode = '$dataCode'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataRefServices'];
  }

  public function findValue($code,$name){
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$code' AND asKey = '$name'");
    $row = mysql_fetch_assoc($sql);
    return $row['asValue'];
  }

  public function findDatacode($code){
    $sql = mysql_query("SELECT dataCode FROM tbl_master WHERE asValue = '$code'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataCode'];
  }

  public function callFieldset($fieldset,$didServices){
    $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE dataValueLabel = '$fieldset' AND dataRefCategory = '$didServices'");
    $row = mysql_fetch_assoc($sql);
    $label = $row['dataValueLabel'];
    $name = $row['dataValueName'];
    $type = $row['dataValueType'];
    $option = $row['dataValueDropdown'];

    if ($type == 'text' || $type == 'number' || $type == 'date') { ?>
      <div class="form-group">
        <label for="message-text" class="form-control-label">
          <?php echo $label; ?>
        </label>
        <input type="<?php echo $type ?>"  class="form-control m-input " name="<?php echo $name ?>"  required>
      </div>
    <?php }elseif ($type == 'select') { ?>
      <div class="form-group">
        <label for="message-text" class="form-control-label">
          <?php echo $label; ?>
        </label>
        <select class="form-control m-input" name="<?php echo $name ?>" required>
            <option value=""> Select </option>
            <?php $sqlchild = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '$option'"); if (mysql_num_rows($sqlchild)) { while ($rowchild = mysql_fetch_assoc($sqlchild)) { ?>
              <option value="<?php echo $rowchild['dataValue']; ?>" >
                <?php echo $rowchild['dataValue']; ?>
              </option>
            <?php } } ?>
        </select>
      </div>
    <?php
    }
  }

  public function callToolField($tool,$services){
    $sqlArray = mysql_query("SELECT dataFieldset FROM tbl_toolunit WHERE dataTool = '$tool' AND dataRefCategory = '$services'");
    if (mysql_num_rows($sqlArray)) {
      while ($rowArray = mysql_fetch_assoc($sqlArray)) {
        $this->showFieldset($rowArray['dataFieldset']);
      }
    }
  }

  public function getDataTag($r){
    $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE asValue = '$r'");
    $row = mysql_fetch_assoc($sql);
    $service = $row['dataRefServices'];
    $sqldata = mysql_query("SELECT dataTag FROM tbl_category WHERE did = '$service'");
    $rowdata = mysql_fetch_assoc($sqldata);
    return $rowdata['dataTag'];
  }

  public function showFieldset($fieldset){
    $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE did = '$fieldset'");
    $row = mysql_fetch_assoc($sql);
    $label = $row['dataValueLabel'];
    $name = $row['dataValueName'];
    $type = $row['dataValueType'];
    $option = $row['dataValueDropdown'];

    if ($type == 'text' || $type == 'number' || $type == 'date') { ?>
      <div class="form-group">
        <label for="message-text" class="form-control-label">
          <?php echo $label; ?>
        </label>
        <input type="<?php echo $type ?>"  class="form-control m-input " name="<?php echo $name ?>"  required>
      </div>
    <?php }elseif ($type == 'select') { ?>
      <div class="form-group">
        <label for="message-text" class="form-control-label">
          <?php echo $label; ?>
        </label>
        <select class="form-control m-input" name="<?php echo $name ?>" required>
            <option value=""> Select </option>
            <?php $sqlchild = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '$option'"); if (mysql_num_rows($sqlchild)) { while ($rowchild = mysql_fetch_assoc($sqlchild)) { ?>
              <option value="<?php echo $rowchild['dataValue']; ?>" >
                <?php echo $rowchild['dataValue']; ?>
              </option>
            <?php } } ?>
        </select>
      </div>
    <?php
    }
  }

  public function showCode($dataTag,$didServices){
    $sql = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE dataValueLabel = '$dataTag' AND dataRefCategory = '$didServices'");
    $row = mysql_fetch_assoc($sql);
    $label = $row['dataValueLabel'];
    $name = $row['dataValueName'];
    $type = $row['dataValueType'];
    $option = $row['dataValueDropdown'];

    return $row;
    }




  public function toolEdit($r){ ?>

  <?php }


  public function toolTransfer($r,$dataCode){ ?>
    <?php $didServices = $this->getServices($dataCode); ?>
    <div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
			  <form method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Transfer Asset <span style="color:red;"><?php echo $r ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
              <?php $this->callFieldset('Location',$didServices); ?>
              <?php $this->callFieldset('User',$didServices); ?>
              <?php $this->callToolField('transfer',$didServices); ?>
  					</div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-secondary" data-dismiss="modal">
  							Close
  						</button>
  						<button type="submit" name="btn-transfer-ass" class="btn btn-info">
  							<i class="la la-reply"></i> Transfer
  						</button>
  					</div>
  				</div>
        </form>
			</div>
		</div>
  <?php }

  public function toolReminder($r,$dataCode){ ?>
    <?php $didServices = $this->getServices($dataCode); ?>
    <div class="modal fade" id="reminder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
			  <form method="post">
          <div class="modal-content">
  					<div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							New Reminder for <span style="color:red;"><?php echo $r ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
            <div class="modal-body">
              <div class="form-group m-form__group">
    						<label for="whom">Reminds Who</label>
                <select class="form-control m-input m-input--solid" id="whom" name="whom" required>
                  <option value="">Choose</option>
                  <?php $son = mysql_query("SELECT did,name FROM tbl_user"); if(mysql_num_rows($son)){ while($bon = mysql_fetch_assoc($son)){ ?>
                    <option value="<?php echo $bon['did'] ?>"><?php echo $bon['name'] ?></option>
                  <?php } } ?>
    						</select>
      					<input type="hidden" class="form-control m-input m-input--solid" id="dataCode" name="dataCode" value="<?php echo $dataCode ?>" required>
    					</div>
              <div class="form-group m-form__group">
    						<label for="dateActual">Actual date</label>
      					<input type="date" class="form-control m-input m-input--solid" id="dateActual" name="dateActual" required>
    					</div>
              <div class="form-group m-form__group">
    						<label for="notifyMe">Notify me</label>
                <select class="form-control m-input m-input--solid" id="notifyMe" name="notifyMe" required>
    							<option value="">Choose</option>
                  <option value="-3 months">3 Month Before</option>
    							<option value="-2 months">2 Months Before</option>
    							<option value="-1 month">1 Months Before</option>
    							<option value="-2 weeks">2 Week Before</option>
    						</select>
    					</div>
              <div class="form-group m-form__group">
    						<label for="remarks">Label/ Remarks</label>
      					<textarea class="form-control m-input m-input--solid" id="remarks" name="remarks" rows="3"></textarea>
    					</div>
  					</div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-secondary" data-dismiss="modal">
  							Close
  						</button>
  						<button type="submit" name="btn-reminder" class="btn btn-focus">
  							<i class="la la-clock-o"></i> Save
  						</button>
  					</div>
  				</div>
        </form>
			</div>
		</div>
  <?php }

  public function toolSwap($r,$dataCode){ ?>
    <?php $didServices = $this->getServices($dataCode); ?>
    <style media="screen">
      .bold{
        font-weight: 800;
      }
    </style>
    <div class="modal fade" id="swap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <form method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Swap or Replace Asset <span style="color:red;"><?php echo $r ?></span>
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                  &times;
                </span>
              </button>
            </div>
            <div class="modal-body row">
              <div class="col-lg-6">
                <div class="form-group">
  								<label for="recipient-name" class="form-control-label">
  									Code:
  								</label>
  								<input type="hidden" class="form-control" value="<?php echo $r ?>" name="oldCode">
                  <p class="form-control-static bold"><?php echo $r; ?></p>
  							</div>
                <div class="form-group">
  								<label for="recipient-name" class="form-control-label">
  									Location:
  								</label>
  								<input type="hidden" class="form-control" value="<?php echo $this->findValue($dataCode,'location'); ?>" name="oldLocation">
                  <p class="form-control-static bold"><?php echo $this->findValue($dataCode,'location'); ?></p>
  							</div>
                <div class="form-group">
  								<label for="recipient-name" class="form-control-label">
  									User:
  								</label>
  								<input type="hidden" class="form-control" value="<?php echo $this->findValue($dataCode,'user'); ?>" name="oldUser">
                  <p class="form-control-static bold"><?php echo $this->findValue($dataCode,'user'); ?></p>
  							</div>

              </div>

              <div class="col-lg-6">
                <?php $dataTag = $this->getDataTag($r);?>
                <?php $thisvalue = $this->showCode($dataTag,$didServices); ?>
                <div class="form-group">
                  <label for="message-text" class="form-control-label">
                    <?php echo $thisvalue['dataValueLabel']; ?> - CODE (replace Asset)
                  </label>
                  <input type="text"  class="form-control m-input col-lg-12" onkeyup="callForSwap(this.value,'<?php echo $r ?>')" required name="newCode">
                </div>
                <div class="" id="swapArea">

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
              </button>
              <button type="submit" name="btn-swap-ass" class="btn btn-brand">
                <i class="la la-retweet"></i> Swap
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  <?php }

  public function toolRepair($r,$dataCode){ ?>
    <div class="modal fade" id="repair" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
				<div class="modal-content">
					<form method="post">
            <div class="modal-header">
  						<h5 class="modal-title" id="exampleModalLabel">
  							Send to repair assets <span style="color:red;"><?php echo $r ?></span>
  						</h5>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span aria-hidden="true">
  								&times;
  							</span>
  						</button>
  					</div>
  					<div class="modal-body">
  							<div class="form-group">
  								<label for="assetcode" class="form-control-label">
  									Code:
  								</label>
                  <input type="text" class="form-control" id="assetcode" value="<?php echo $r ?>" name="assetcode" readonly>
  								<input type="hidden" class="form-control" id="datacode" value="<?php echo $dataCode ?>" name="datacode">
  							</div>
  							<div class="form-group">
  								<label for="repairVendor" class="form-control-label">
  									Vendor:
  								</label>
  								<input type="text" class="form-control" id="repairVendor" name="repairVendor" required>
  							</div>
  							<div class="form-group">
  								<label for="reasonRepair" class="form-control-label">
  									Reason:
  								</label>
  								<input type="text" class="form-control" id="reasonRepair" name="reasonRepair" required>
  							</div>
  							<!-- <div class="form-group">
  								<label for="sendBy" class="form-control-label">
  									Send By:
  								</label>
  								<input type="text" class="form-control" id="sendBy" name="sendBy" required>
  							</div> -->
  							<!-- <div class="form-group">
  								<label for="dateSend" class="form-control-label">
  									Date Send :
  								</label>
  								<input type="date" class="form-control" name="dateSend" id="dateSend" required>
  							</div> -->
  					</div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-secondary" data-dismiss="modal">
  							Close
  						</button>
  						<button type="submit" name="btn-send-repair" class="btn btn-metal">
  							<i class="la la-gear"></i> Send
  						</button>
  					</div>
          </form>
				</div>
			</div>
		</div>
  <?php }

  public function repairReceived($did,$dataCode){ ?>
  <div class="modal fade" id="received<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <form method="post">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              Receive repair assets
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                &times;
              </span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="price" class="form-control-label">
                  Price (RM):
                </label>
                <input type="text" class="form-control" id="price" name="price" required >
                <input type="hidden" class="form-control" id="did" value="<?php echo $did ?>" name="did">
                <input type="hidden" class="form-control" id="datacode" value="<?php echo $dataCode ?>" name="datacode">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              Close
            </button>
            <button type="submit" name="btn-receive-repair" class="btn btn-metal">
              </i> Received
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php }

  public function toolCondemn($r,$dataCode){ ?>
    <div class="modal fade" id="condemn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Condemn assets <span style="color:red;"><?php echo $r ?></span>
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						Do you wish to condemn this Asset ?
					</div>
					<div class="modal-footer">
						<form method="post">
              <input type="hidden" name="dataCode" value="<?php echo $dataCode ?>">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">
  							Close
  						</button>
  						<button type="submit" name="btn-condemn" class="btn btn-warning">
  							<i class="la la-warning"></i> Yes
  						</button>
            </form>
					</div>
				</div>
			</div>
		</div>
  <?php }

  public function toolDelete($r){ ?>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Condemn assets <span style="color:red;"><?php echo $r ?></span>
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						Do you wish to delete this Asset permenantly ?
					</div>
					<div class="modal-footer">
						<form method="post">
              <input type="hidden" name="code" value="<?php echo $r ?>">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">
  							Close
  						</button>
  						<button type="submit" name="btn-del-ass" class="btn btn-danger">
  							<i class="la la-trash"></i> Yes
  						</button>
            </form>
					</div>
				</div>
			</div>
		</div>
  <?php }

}
?>
