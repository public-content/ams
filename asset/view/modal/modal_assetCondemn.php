<?php
  class popAssetCondemn {

    public function getMaster($asKey,$dataCode){
      $sql = mysql_query("SELECT asValue FROM tbl_master WHERE asKey = '$asKey' AND dataCode = '$dataCode'");
      $row = mysql_fetch_assoc($sql);
      return $row['asValue'];
    }

    public function getCodemns($column,$dataValue){
      $sql = mysql_query("SELECT $column FROM tbl_condemn WHERE dataValue = '$dataValue'");
      $row = mysql_fetch_assoc($sql);
      return $row[$column];
    }

    public function modalFillForm($data){ ?>
      <div class="modal fade" id="c_form<?php echo $data; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <form class="m-form m-form--fit m-form--label-align-right" method="POST">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Condemn Form</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
        				<div class="m-portlet__body">
                  <div class="form-group m-form__group">
        						<label for="code">Code</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid" id="code" name="code" value="<?php echo $this->getMaster('code',$data); ?>">
                    <input type="hidden" class="form-control m-input m-input--square m-input--solid" id="dataCode" name="dataCode" value="<?php echo $data; ?>">
        					</div>
                  <div class="form-group m-form__group">
        						<label for="tagging">Asset No. of tagging</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid" id="tagging" name="tagging" value="<?php echo $this->getMaster('assetTag',$data); ?>">
        					</div>
        					<div class="form-group m-form__group">
        						<label for="cost">Purchase Cost</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid" id="cost" name="cost" value="<?php echo "RM ".$this->getMaster('price',$data); ?>">
        					</div>
        					<div class="form-group m-form__group">
        						<label for="dateReceived">Purchase Date</label>
        						<input type="date" class="form-control m-input m-input--square m-input--solid" id="dateReceived" name="dateReceived" value="<?php echo $this->getMaster('dateReceived',$data); ?>">
        					</div>
        					<div class="form-group m-form__group">
        						<label for="remarks">Remarks</label>
        						<textarea class="form-control m-input m-input--solid" id="remarks" name="remarks" rows="3"></textarea>
        					</div>
        					<div class="form-group m-form__group">
        						<label for="others">Others</label>
        						<textarea class="form-control m-input m-input--solid" id="others" name="others" rows="3"></textarea>
        					</div>
        				</div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-warning" name="btn-notValid">Not Valid for Condemn</button>
              <button type="submit" name="btn-fillForm" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    <?php }

    public function modalVerified($data){ ?>
      <div class="modal fade" id="c_verified<?php echo $data; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <form class="m-form m-form--fit m-form--label-align-right" method="POST">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Verified</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
        				<div class="m-portlet__body">
                  <div class="form-group m-form__group">
        						<label for="code">Code</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid"  id="code" name="code" value="<?php echo $this->getCodemns('code',$data); ?>" readonly>
                    <input type="hidden" class="form-control m-input m-input--square m-input--solid" id="dataCode" name="dataCode" value="<?php echo $data; ?>">
        					</div>
                  <div class="form-group m-form__group">
        						<label for="tagging">Asset No. of tagging</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid"  id="tagging" name="tagging" value="<?php echo $this->getCodemns('tagging',$data); ?>" readonly>
        					</div>
        					<div class="form-group m-form__group">
        						<label for="cost">Purchase Cost</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid"  id="cost" name="cost" value="<?php echo "RM ".$this->getCodemns('cost',$data); ?>" readonly>
        					</div>
        					<div class="form-group m-form__group">
        						<label for="dateReceived">Purchase Date</label>
        						<input type="date" class="form-control m-input m-input--square m-input--solid"  id="dateReceived" name="dateReceived" value="<?php echo $this->getCodemns('dateReceived',$data); ?>" readonly>
        					</div>
        					<div class="form-group m-form__group">
        						<label for="remarks">Remarks</label>
        						<textarea class="form-control m-input m-input--solid" id="remarks" name="remarks"  rows="3"><?php echo $this->getCodemns('remarks',$data); ?></textarea>
        					</div>
        					<div class="form-group m-form__group">
        						<label for="others">Others</label>
        						<textarea class="form-control m-input m-input--solid" id="others" name="others"  rows="3" valu="qwesad"><?php echo $this->getCodemns('others',$data); ?></textarea>
        					</div>
        				</div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-warning" name="btn-notVerified">Not Verified</button>
              <button type="submit" name="btn-verified" class="btn btn-success">Verified</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    <?php }

    public function modalTakeAction($data){ ?>
      <div class="modal fade" id="c_action<?php echo $data; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <form class="m-form m-form--fit m-form--label-align-right" method="POST">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Disposal Method</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
        				<div class="m-portlet__body">
        						<input type="hidden" class="form-control m-input m-input--square m-input--solid" id="code" name="code" value="<?php echo $this->getMaster('code',$data); ?>">
                    <input type="hidden" class="form-control m-input m-input--square m-input--solid" id="dataCode" name="dataCode" value="<?php echo $data; ?>">
                  <div class="form-group m-form__group">
        						<label for="tagging">Disposal Method</label>
                    <select class="form-control m-input m-input--solid" id="" name="disposalMethod">
        							<option value="">Select</option>
        							<option value="Auction">Auction</option>
        							<option value="Scrap/Sold">Scrap/Sold</option>
        							<option value="Recycle">Recycle</option>
        							<option value="Trade-in">Trade-in</option>
        						</select>
        					</div>
                  <div class="form-group m-form__group">
        						<label for="receiptNo">Receipt No. / P.O Number (RM)</label>
        						<input type="text" class="form-control m-input m-input--square m-input--solid" id="receiptNo" name="receiptNo" placeholder="Receipt No. / P.O Number">
        					</div>
        				</div>
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-link">reset</button>
              <button type="submit" name="btn-disposal" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    <?php }
  }
?>
