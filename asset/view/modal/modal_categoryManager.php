<?php
  class popServices{
    public function popnewServices(){
    ?>
    <div class="modal fade" id="add_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form class="" action="" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                New Services
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                  &times;
                </span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group m-form__group">
  							<label for="valueService">
  								Services Name <span class="redStar">*</span>
  							</label>
  							<input type="text" class="form-control m-input m-input--solid col-lg-8" id="valueService" name="valueService" placeholder="" required>
  						</div>
              <div class="form-group m-form__group">
                <label for="tagChar">
                  Tagging Character <span class="redStar">*</span>
                </label>
                <input type="text" class="form-control m-input m-input--solid col-lg-4" id="tagChar" name="tagChar" placeholder="" pattern="[A-Za-z]{2,}" title="Only Letter" required>
              </div>
              <div class="col-lg-12" style="padding:0;">
                <div class="m-accordion m-accordion--borderless" id="m_accordion_3" role="tablist" style="padding:0;">
    							<!--begin::Item-->
    							<div class="m-accordion__item">
    								<div class="m-accordion__item-head"  role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="true"  style="padding:0;">
    									<span class="m-accordion__item-title">
    										<span style="color:#2200cc;font-weight:0;font-size:1rem;">Show Advanced Option </span>
    									</span>
    								</div>
    								<div class="m-accordion__item-body collapse show" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
    									<div class="m-accordion__item-content">
                        <div class="form-group m-form__group row">
                          <label class="col-5 col-form-label">
  													Optional Fieldset
  												</label>
  												<div class="col-lg-7">
  													<div class="m-checkbox-list row">
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="imageFieldset">
    														    Images
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Input Fieldset" data-content="5 images fieldset"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="vendorFieldset">
    														    Vendor
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="3 Input Fieldset" data-content="1 Vendor Name fieldset, 1 Date received, 1 Warranty Valid date, 1 Invoice no"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="assetFieldset">
    														    Asset Tagging
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Input Fieldset" data-content="1 asset tagging fieldset"> </i>
                              </div>
  													</div>
  												</div>
            						</div>
                        <div class="form-group m-form__group row">
                          <label class="col-5 col-form-label">
  													Tools Selection
  												</label>
  												<div class="col-lg-7">
  													<div class="m-checkbox-list row">
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="toolEdit">
    														    Edit
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Tool info" data-content="This tools allow end-user to Edit the asset info. Please refer to the TOOL MANAGER"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="toolCondemn">
    														    Condemned
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Tool info" data-content="This tools allow end-user to condemn the asset. Please refer to the TOOL MANAGER"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="toolDelete">
    														    Delete
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Tool info" data-content="This tools allow end-user to permenantly delete the asset. Please refer to the TOOL MANAGER"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="tooltransfer">
    														    Transfer
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Tool info" data-content="This tools allow end-user to transfer the asset. Please refer to the TOOL MANAGER"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="toolreplace">
    														    Swap/Replace
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Tool info" data-content="This tools allow end-user to swap or replace the asset. Please refer to the TOOL MANAGER"> </i>
                              </div>
                              <div class="col-lg-7">
                                <label class="m-checkbox">
    															<input type="checkbox" name="toolrepair">
    														    Repair
    															<span></span>
    														</label>
                              </div>
                              <div class="col-lg-3">
                                <i class="la la-info-circle" data-toggle="m-popover" title="Tool info" data-content="This tools allow end-user to send the asset to repair. Please refer to the TOOL MANAGER"> </i>
                              </div>
  													</div>
  												</div>
            						</div>
    									</div>
    								</div>
                  </div>
    						</div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-link">
                Reset
              </button>
              <button type="submit" name="btn-addServices" class="btn btn-brand">
                <i class="la la-plus"></i> Add New
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php
    }

    public function popnewMain($x){ ?>
      <div class="modal fade" id="add_main<?php echo $x ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											New Main
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      <div class="form-group m-form__group">
          							<label for="valueService">
          								Main Category <span class="redStar">*</span>
          							</label>
          							<input type="text" class="form-control m-input m-input--solid col-lg-8" id="valueService" name="valueMain" required>
          						</div>
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="dataRefCategory" value="<?php echo $x; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-addMain" class="btn btn-brand">
  											<i class="la la-plus"></i> Add New
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }

    public function popnewSub($x,$y){ ?>
      <div class="modal fade" id="add_sub<?php echo $x ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											New Sub
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      <div class="form-group m-form__group">
          							<label for="valueService">
          								Child Category <span class="redStar">*</span>
          							</label>
          							<input type="text" class="form-control m-input m-input--solid col-lg-8" id="valueService" name="valueSub"  required>
          						</div>
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="dataRefService" value="<?php echo $y; ?>">
                      <input type="hidden" name="dataRefMain" value="<?php echo $x; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-addChild" class="btn btn-brand">
  											<i class="la la-plus"></i> Add New
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }

    public function popeditSub($did,$dataValue){ ?>
      <div class="modal fade" id="edit_sub<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											Edit Sub <?php echo $dataValue ?>
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      <div class="form-group m-form__group">
          							<label for="valueService">
          								Child Category
          							</label>
          							<input type="text" class="form-control m-input m-input--solid col-lg-8" id="value" name="valueSub" value="<?php echo $dataValue ?>" required>
          						</div>
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="didChild" value="<?php echo $did; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-editChild" class="btn btn-brand">
  											<i class="la la-plus"></i> Edit
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }

    public function popeditMain($did,$dataValue){ ?>
      <div class="modal fade" id="edit_main<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											Edit Sub <?php echo $dataValue ?>
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      <div class="form-group m-form__group">
          							<label for="valueService">
          								Child Category
          							</label>
          							<input type="text" class="form-control m-input m-input--solid col-lg-8" id="value" name="valueMain" value="<?php echo $dataValue ?>" required>
          						</div>
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="didMain" value="<?php echo $did; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-editMain" class="btn btn-brand">
  											<i class="la la-plus"></i> Edit
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }

    public function popdelSub($did,$dataValue){ ?>
      <div class="modal fade" id="del_sub<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-sm" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											Delete Child <?php echo $dataValue ?>
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      Do you wish to delete this child
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="didChild" value="<?php echo $did; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-delChild" class="btn btn-danger">
  											<i class="la la-trash"></i> Yes
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }

    public function popdelMain($did,$dataValue){ ?>
      <div class="modal fade" id="del_main<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-sm" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											Delete Main <?php echo $dataValue ?>
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      Do you wish to delete this child
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="didMain" value="<?php echo $did; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-delMain" class="btn btn-danger">
  											<i class="la la-trash"></i> Yes
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }

    public function popdelSer($did,$dataValue){ ?>
      <div class="modal fade" id="del_service<?php echo $did ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-sm" role="document">
								<form class="" action="" method="post">
                  <div class="modal-content">
  									<div class="modal-header">
  										<h5 class="modal-title" id="exampleModalLabel">
  											Delete Services <?php echo $dataValue ?>
  										</h5>
  										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  											<span aria-hidden="true">
  												&times;
  											</span>
  										</button>
  									</div>
  									<div class="modal-body">
                      Do you wish to delete this Service
  									</div>
  									<div class="modal-footer">
                      <input type="hidden" name="did" value="<?php echo $did; ?>">
  										<button type="button" class="btn btn-link" data-dismiss="modal">
  											Close
  										</button>
  										<button type="submit" name="btn-delSer" class="btn btn-danger">
  											<i class="la la-trash"></i> Yes
  										</button>
  									</div>
  								</div>
                </form>
							</div>
						</div>
    <?php }
  }
?>
