<?php
include('../../controller/database.php');

 function getValueMaster($code,$key){
  $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$code' AND asKey = '$key'");
  $row = mysql_fetch_assoc($sql);
  return $row['asValue'];
}

 function getValueCategory($getMain){
  $sql = mysql_query("SELECT dataValueWrite FROM tbl_category WHERE did = '$getMain'");
  $row = mysql_fetch_assoc($sql);
  return $row['dataValueWrite'];
}

function getValue($table,$need,$columnName,$value){
  $sql = mysql_query("SELECT $need FROM $table WHERE $columnName = '$value'");
  $row = mysql_fetch_assoc($sql);
  return $row[$need];
}

function getServicesdid($dataCode){
  $sql = mysql_query("SELECT dataRefServices FROM tbl_master WHERE dataCode = '$dataCode'");
  $row = mysql_fetch_assoc($sql);
  return $row['dataRefServices'];
}

function getAssetCode($dataCode){
  $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$dataCode' AND asKey = 'code'");
  $row = mysql_fetch_assoc($sql);
  return $row['asValue'];
}

function getIpaddress(){
  if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
  }
  $client  = @$_SERVER['HTTP_CLIENT_IP'];
  $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
  $remote  = $_SERVER['REMOTE_ADDR'];

  if(filter_var($client, FILTER_VALIDATE_IP))
  {
      $ip = $client;
  }
  elseif(filter_var($forward, FILTER_VALIDATE_IP))
  {
      $ip = $forward;
  }
  else
  {
      $ip = $remote;
  }

  return $ip;
}


if (isset($_POST['datatype'])) {
  $type = mysql_real_escape_string(trim($_POST['datatype']));
  $dataCode = mysql_real_escape_string(trim($_POST['dataCode']));
  $ref = mysql_real_escape_string(trim($_POST['dataRef']));
  $by = mysql_real_escape_string(trim($_POST['by']));
  $getDidServices = getServicesdid($dataCode);
  $assetcode = getAssetCode($dataCode);
  $ipaddress = getIpaddress();
  $sendBy = $by;

  if ($type == 'yay') {
    $sql = mysql_query("UPDATE tbl_master SET active = 'Condemn' WHERE datacode = '$dataCode'");
    if(!$sql){
      echo mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataCode','assets','Assets Condemn [Confirm]: $assetcode','$ipaddress','$sendBy',NOW())");
      $message = "<div class='alert alert-success alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Successfully! </strong> The Asset has been confirm as condemn item</div>";
    }
  }elseif ($type == 'nay') {
    $sql = mysql_query("UPDATE tbl_master SET active = 'Yes' WHERE datacode = '$dataCode'");
    if(!$sql){
      echo mysql_error();
    }else{
      $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                             VALUES('$getDidServices','$dataCode','assets','Assets Condemn [Cancelled]: $assetcode','$ipaddress','$sendBy',NOW())");
      $message = "<div class='alert alert-warning alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Successfully! </strong> The Asset status back to normal</div>";
    }
  }
  ?>
  <div class="col-lg-12">
    <?php if($message != ''){
      echo $message;
    } ?>
  </div>

  <?php
}elseif (isset($_POST['ref'])) {
  $ref = mysql_real_escape_string(trim($_POST['ref']));
  $dcode = mysql_real_escape_string(trim($_POST['dcode']));
?>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          <?php echo getValue('tbl_category','dataValueWrite','did',$ref)."'s Assets Condemn Approval List" ?>
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body">
    <table class="table table-bordered m-table m-table--border-secondary m-table--head-bg-metal">
      <thead>
        <tr>
          <th>
            #
          </th>
          <th>
            Code
          </th>
          <th>
            Category
          </th>
          <th>
            Name
          </th>
          <th>
            Brand
          </th>
          <th>
            Serial
          </th>
          <th>
            Date
          </th>
          <th>
            Action
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        $bil=1;
        $sql = mysql_query("SELECT DISTINCT dataCode FROM tbl_master WHERE dataRefServices = '$ref' AND active = 'In List'");
        if (mysql_num_rows($sql)) {
          while ($row = mysql_fetch_assoc($sql)) {
            $dataCode = $row['dataCode'];
            ?>
            <tr>
              <th scope="row">
                <?php echo $bil; ?>
              </th>
              <td>
              <?php echo getValueMaster($dataCode,'code'); ?>
              </td>
              <td>
              <?php
              $child = getValueMaster($dataCode,'child');
              echo getValueCategory($child);
              ?>
              </td>
              <td>
              <?php echo getValueMaster($dataCode,'name'); ?>
              </td>
              <td>
              <?php echo getValueMaster($dataCode,'brand'); ?>
              </td>
              <td>
              <?php echo getValueMaster($dataCode,'serialNo'); ?>
              </td>
              <td>
              <?php echo getValueMaster($dataCode,'dateReceived'); ?>
              </td>
              <td>
                <a class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="View" onclick="isCondemn('yay','<?php echo $dataCode; ?>','<?php echo $ref ?>')"> <i class="fa fa-check"></i> </a>
                <a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="View" onclick="isCondemn('nay','<?php echo $dataCode; ?>','<?php echo $ref ?>')"> <i class="fa fa-close"></i> </a>
              </td>
            </tr>
            <?php
            $bil++;
          }
        }else{ ?>
          <tr>
            <th colspan="8" style="text-align:center;">No Data</th>
          </tr>
        <?php } ?>

      </tbody>
    </table>
  </div>
</div>
<?php
}
?>
