<?php
  include('../../controller/database.php');
  function thisRole($page,$role){
    $sql = mysql_query("SELECT did FROM tbl_role WHERE dataValue = '$page' AND dataRef = '$role' AND status = 'active'");
    $row = mysql_num_rows($sql);
    $result = $row;

    if($result != "1"){
      return "<i class='la la-close'></i>";
    }else{
      return "<i class='la la-check'></i>";
    }
  }

  function getThisPage($page,$didRole){
    $sql = mysql_query("SELECT did FROM tbl_role WHERE dataValue = '$page' AND dataRef = '$didRole' AND dataType = 'page' AND status = 'active'");
    $row = mysql_num_rows($sql);

    if($row != 1){
      return '';
    }else{
      return 'checked';
    }
  }

  function getThisTools($tools,$didRole){
    $sql = mysql_query("SELECT $tools FROM tbl_role WHERE did = '$didRole'");
    $row = mysql_fetch_assoc($sql);
    $result = $row[$tools];

    if($result == 'Yes'){
      return 'checked';
    }else{
      return 'No';
    }
  }

  function getCondemnRule($did,$value){
    $sql = mysql_query("SELECT condemnRule FROM tbl_role WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    $result = $row['condemnRule'];

    if($result == $value){
      return 'checked';
    }else{
      return 'No';
    }
  }

  function getRole($column,$did){
    $sql = mysql_query("SELECT $column FROM tbl_role WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row[$column];
  }

  function getServicesInRole($role){
    $sql = mysql_query("SELECT services FROM tbl_role WHERE did = '$role'");
    $row = mysql_fetch_assoc($sql);
    return $row['services'];
  }

  function displayCheck($did,$page){
    $sql = mysql_query("SELECT $page FROM tbl_role WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    $check = $row[$page];
    if($check != 'Yes'){
      return '';
    }else{
      return 'checked';
    }
  }

  function getThis($table,$column,$data){
    $sql = mysql_query("SELECT $column FROM $table WHERE did = '$data'");
    $row = mysql_fetch_assoc($sql);
    return $row[$column];
  }

  function displayValueCheck($did,$page){
    $sql = mysql_query("SELECT $page FROM tbl_role WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    $check = $row[$page];
    if($check != 'Yes'){
      return 'Yes';
    }else{
      return 'No';
    }
  }

  function getThisData($column,$did){
    $sql = mysql_query("SELECT $column FROM tbl_user WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row[$column];
  }



  if(isset($_POST['page'])){
    $page = $_POST['page'];

    if ($page == "user") {
      if ($_POST['deldid'] != '') {
        $didid = $_POST['deldid'];
        $sql = mysql_query("DELETE FROM tbl_user WHERE did = '$didid'");
        if (!$sql) {
          echo mysql_error();
        }else{
          $alert = "<div class='alert alert-success alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Successfully! </strong>User has been Deleted </div>";
        }
      }
?>
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              User List
            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <a class="btn btn-secondary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" style="color:black;border:1px solid black;" onclick="tapToAddUser('addUser')">
            <span>
              <i class="la la-plus"></i>
              <span>
                New User
              </span>
            </span>
          </a>
        </div>
      </div>
      <div class="m-portlet__body">
        <table class="table">
          <tdead class="thead-inverse">
            <tr>
              <td>
                #
              </td>
              <td>
                Img
              </td>
              <td>
                Name
              </td>
              <td>
                Username
              </td>
              <td>
                User level
              </td>
              <td>
                Department
              </td>
              <td>
                Location
              </td>
              <td>
                Action
              </td>
            </tr>
          </thead>
          <tbody id="myTable">
            <?php $bil = 1; $sqlUser = mysql_query("SELECT * FROM tbl_user");if(mysql_num_rows($sqlUser)){ while($rowUser = mysql_fetch_assoc($sqlUser)){ ?>
              <tr>
                <td scope="row">
                  <?php echo $bil ?>
                </td>
                <td style="padding-top: 4.872px;">
                  <div class="m-card-user__pic">
                    <img src="<?php echo $rowUser['img'] ?>" alt="" class="m--img-rounded m--marginless" alt="" width="30"/>
                  </div>
                </td>
                <td>
                  <?php echo $rowUser['name']; ?>
                </td>
                <td>
                  <?php echo $rowUser['username']; ?>
                </td>
                <td>
                  <?php echo getThis('tbl_role','dataValue',$rowUser['role']);  ?>
                </td>
                <td>
                  <?php echo $rowUser['department']; ?>
                </td>
                <td>
                  <?php echo getThis('tbl_optionunit','dataValue',$rowUser['location']);  ?>
                </td>
                <td>
                  <a href="#" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill" title="Edit User Profile" onclick="tapToEditUser('editUser','<?php echo $rowUser['did'] ?>')"> <i class="la la-edit"></i> </a>
                  <a href="#" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill" title="Delete User Profile" onclick="tapToDel('user','<?php echo $rowUser['did'] ?>')"> <i class="la la-trash"></i> </a>
                </td>
              </tr>
            <?php $bil++; } }else{ ?>
              <tr>
                <td scope="row" rowspan="7">
                  No Data
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
<?php
    }elseif ($page == "role") {
      if ($_POST['deldid'] != '') {
        $didid = $_POST['deldid'];
        $sql = mysql_query("UPDATE tbl_role SET status = 'deactive' WHERE did = '$didid'");
        if (!$sql) {
          echo mysql_error();
        }else{
          $alert = "<div class='alert alert-success alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Successfully! </strong>User Role has been Deleted </div>";
        }
      }
?>
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              User Level
            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <a class="btn btn-secondary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" style="color:black;border:1px solid black;" onclick="tapToAdd('add')">
            <span>
              <i class="la la-plus"></i>
              <span>
                New Role
              </span>
            </span>
          </a>
        </div>
      </div>
      <div class="">
        <?php if ($alert != '') {
          echo $alert;
        } ?>
      </div>
      <div class="m-portlet__body" style="overflow-x:scroll;">
        <table class="table table-bordered table-hover custom-table">
          <thead>
            <tr>
              <th>
                User Level
              </th>
              <th>Dashboard Tab</th>
              <th>Asset Tab</th>
              <td>Asset Register</td>
              <td>Asset List</td>
              <td>Asset Condemn</td>
              <td>Asset Reminder</td>
              <td>Asset Loan</td>
              <td>Asset Inventory</td>
              <th>Report Tab</th>
              <td>Master Logs</td>
              <th>Admin Tab</th>
              <td>Admin Area</td>
              <td>User Manager</td>
              <td>All Manager</td>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $sqlRole =mysql_query("SELECT did,dataValue FROM tbl_role WHERE dataType = 'role' AND status = 'active'"); if(mysql_num_rows($sqlRole)){ while($rowRole = mysql_fetch_assoc($sqlRole)){ ?>
                <tr>
                  <th scope="row">
                    <?php echo $rowRole['dataValue'] ?>
                  </th>
                  <td>
                    <?php echo thisRole('page_dashboard',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_asset',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_assetRegister',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_assetList',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_assetCondemn',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_assetReminder',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_assetLoan',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_assetInventory',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_report',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_logsMaster',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_admin',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_adminArea',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_userManager',$rowRole['did']); ?>
                  </td>
                  <td>
                    <?php echo thisRole('page_allManager',$rowRole['did']); ?>
                  </td>
                    <td>
                      <a href="#" class="btn btn-secondary m-btn m-btn--icon m-btn--pill" onclick="tapToEdit('<?php echo $rowRole['did'] ?>')">
    										<span>
    											<i class="la la-edit"></i>
    											<span>
    												Edit
    											</span>
    										</span>
    									</a>
                      <a href="#" class="btn btn-secondary m-btn m-btn--icon m-btn--pill" onclick="tapToDel('role','<?php echo $rowRole['did'] ?>')">
    										<span>
    											<i class="la la-trash"></i>
    											<span>
    												Delete
    											</span>
    										</span>
    									</a>
                    </td>
                </tr>

            <?php } } ?>

          </tbody>
        </table>
      </div>
    </div>
<?php
}elseif ($page == 'edit') {
?>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          Edit User Level
        </h3>
      </div>
    </div>
  </div>
  <form class="" action="" method="post">
    <div class="m-portlet__body">
      <div class="form-group m-form__group">
        <label for="newRole">
          Role Name
        </label>
        <input type="text" class="form-control m-input" id="newRole" name="newRole" value="<?php echo getRole("dataValue",$_POST['did']) ?>" required>
        <input type="hidden" class="form-control m-input" id="newRole" name="newRoleID" value="<?php echo $_POST['did']?>" required>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Dashboard</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_dashboard',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_dashboard','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Asset</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_asset',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_asset','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Register
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_assetRegister',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_assetRegister','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset List
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_assetList',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_assetList','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Condemn
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_assetCondemn',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_assetCondemn','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Reminder
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_assetReminder',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_assetReminder','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Loan
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_assetLoan',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_assetLoan','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Inventory
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_assetInventory',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_assetInventory','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Report</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_report',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_report','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Master logs
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_logsMaster',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_logsMaster','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Admin</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_admin',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_admin','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Admin Area
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_adminArea',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_adminArea','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> User Manager
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_userManager',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_userManager','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> All manager
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisPage('page_allManager',$_POST['did']);
              if($check == 'checked'){
                $ans = 'deactive';
              }else{
                $ans = 'active';
              }
              ?>
              <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','page_allManager','<?php echo $ans ?>')">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
					  System can able this role to control over service. Choose a service to control by this role
					</span>
        </div>
      </div>
      <div class="m-form__group form-group row">
         <label class="col-lg-3 col-form-label">
          Services
         </label>
         <div class="col-lg-3">
             <?php $sql=mysql_query("SELECT dataValueWrite FROM tbl_category WHERE dataType = 'Service'");if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
               <span class="m-switch m-switch--sm m-switch--icon col-lg-12">
                 <label>
                   <?php
                   $idi = $_POST['did'];
                   $serviceName = $row['dataValueWrite'];
                   $check = getThisPage($serviceName,$_POST['did']);
                   if($check == 'checked'){
                     $ans = 'deactive';
                   }else{
                     $ans = 'active';
                   }
                   ?>
                   <input type="checkbox" <?php echo $check ?> onclick="changePageStatus('<?php echo $idi ?>','<?php echo $serviceName ?>','<?php echo $ans ?>')">
                   <span></span>
                   <label class="col-lg-4 col-form-label"><?php echo $row['dataValueWrite'] ?></label>
                 </label>
               </span>
             <?php } } ?>
         </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
            System Tools, able user to use functional button to handle an asset.
          </span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Tools</b>
        </label>
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Edit
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisTools('btnEdit',$_POST['did']);
              if($check == 'checked'){
                $ans = 'No';
              }else{
                $ans = 'Yes';
              }
              ?>
              <input type="checkbox" <?php echo $check ?>  onclick="changeBtnStatus('<?php echo $idi ?>','btnEdit','<?php echo $ans ?>')" name="btnEdit">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Transfer
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisTools('btnTransfer',$_POST['did']);
              if($check == 'checked'){
                $ans = 'No';
              }else{
                $ans = 'Yes';
              }
              ?>
              <input type="checkbox" <?php echo $check ?>  onclick="changeBtnStatus('<?php echo $idi ?>','btnTransfer','<?php echo $ans ?>')"  name="btnTransfer">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Swap
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisTools('btnSwap',$_POST['did']);
              if($check == 'checked'){
                $ans = 'No';
              }else{
                $ans = 'Yes';
              }
              ?>
              <input type="checkbox" <?php echo $check ?>  onclick="changeBtnStatus('<?php echo $idi ?>','btnSwap','<?php echo $ans ?>')"  name="btnSwap">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Repair
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisTools('btnRepair',$_POST['did']);
              if($check == 'checked'){
                $ans = 'No';
              }else{
                $ans = 'Yes';
              }
              ?>
              <input type="checkbox" <?php echo $check ?>  onclick="changeBtnStatus('<?php echo $idi ?>','btnRepair','<?php echo $ans ?>')"  name="btnRepair">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Condemn
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisTools('btnCondemn',$_POST['did']);
              if($check == 'checked'){
                $ans = 'No';
              }else{
                $ans = 'Yes';
              }
              ?>
              <input type="checkbox" <?php echo $check ?>  onclick="changeBtnStatus('<?php echo $idi ?>','btnCondemn','<?php echo $ans ?>')"  name="btnCondemn">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Delete
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <?php
              $idi = $_POST['did'];
              $check = getThisTools('btnDelete',$_POST['did']);
              if($check == 'checked'){
                $ans = 'No';
              }else{
                $ans = 'Yes';
              }
              ?>
              <input type="checkbox" <?php echo $check ?>  onclick="changeBtnStatus('<?php echo $idi ?>','btnDelete','<?php echo $ans ?>')"  name="btnDelete">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
					  Condemn Rule, there three difference type of rule.
					</span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Condemn Rule</b>
        </label>
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> All
        </label>
        <div class="col-lg-3">
          <?php
          $idi = $_POST['did'];
          $check = getCondemnRule($_POST['did'],'all');
          if($check == 'checked'){
            $ans = 'No';
          }else{
            $ans = 'Yes';
          }
          ?>
          <label class="m-radio">
					<input <?php echo $check; ?> type="radio" name="condemnRule" value="all" onclick="changeCondemnRule('all','<?php echo $idi ?>')">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Checker
        </label>
        <div class="col-lg-3">
          <?php
          $idi = $_POST['did'];
          $check = getCondemnRule($_POST['did'],'Checker');
          if($check == 'checked'){
            $ans = 'No';
          }else{
            $ans = 'Yes';
          }
          ?>
          <label class="m-radio">
					<input <?php echo $check; ?> type="radio" name="condemnRule" value="Checker" onclick="changeCondemnRule('Checker','<?php echo $idi ?>')">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Verifier
        </label>
        <div class="col-lg-3">
          <?php
          $idi = $_POST['did'];
          $check = getCondemnRule($_POST['did'],'Verifier');
          if($check == 'checked'){
            $ans = 'No';
          }else{
            $ans = 'Yes';
          }
          ?>
          <label class="m-radio">
					<input <?php echo $check; ?> type="radio" name="condemnRule" value="Verifier" onclick="changeCondemnRule('Verifier','<?php echo $idi ?>')">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Disposal
        </label>
        <div class="col-lg-3">
          <?php
          $idi = $_POST['did'];
          $check = getCondemnRule($_POST['did'],'Disposal');
          if($check == 'checked'){
            $ans = 'No';
          }else{
            $ans = 'Yes';
          }
          ?>
          <label class="m-radio">
					<input <?php echo $check; ?> type="radio" name="condemnRule" value="Disposal" onclick="changeCondemnRule('Disposal','<?php echo $idi ?>')">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
    </div>
    <div class="m-portlet__foot">
     <div class="row">
       <div class="col-lg-6 m--valign-middle">
       </div>
       <div class="col-lg-6 m--align-right">
         <button type="reset" class="btn btn-link">
           reset
         </button>
         <button type="submit" name="btn-edit-role" class="btn btn-secondary">
           <i class="la la-edit"></i> Edit
         </button>
       </div>
     </div>
  </div>
</form>
</div>
<?php
}elseif($page == 'add'){
?>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          Add User Level
        </h3>
      </div>
    </div>
  </div>
  <form class="" action="" method="post">
    <div class="m-portlet__body">
      <div class="form-group m-form__group">
         <label for="newRole">
           Role Name
         </label>
         <input type="text" class="form-control m-input col-lg-6" id="newRole" name="newRole" placeholder="" required>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
					  System can control user access that assign to a role by able/disable
					</span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Dashboard</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_dashboard">
              <span></span>
            </label>
          </span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Asset</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_asset">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Register
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_assetRegister">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset List
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_assetList">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Condemn
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_assetCondemn">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
            <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Asset Reminder
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_assetReminder">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Inventory
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_assetInventory">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Report</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_report">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Master logs
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_logsMaster">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Admin</b>
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_admin">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Admin Area
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_adminArea">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> User Manager
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_userManager">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> All manager
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" name="page_allManager">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
					  System can able this role to control over services. Choose a service to control by this role
					</span>
        </div>
      </div>
      <div class="m-form__group form-group row">
         <label class="col-lg-3 col-form-label">
          Services
         </label>
         <div class="col-lg-3">
             <?php $sql=mysql_query("SELECT dataValueWrite FROM tbl_category WHERE dataType = 'Service'");if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
               <span class="m-switch m-switch--sm m-switch--icon col-lg-12">
                 <label>
                   <input type="checkbox" name="<?php echo $row['dataValueWrite'] ?>">
                   <span></span>
                   <label class="col-lg-4 col-form-label"><?php echo $row['dataValueWrite'] ?></label>
                 </label>
               </span>
             <?php } } ?>
         </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
					  System Tools, able user to use functional button to handle an asset.
					</span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Tools</b>
        </label>
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Edit
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnEdit">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Transfer
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnTransfer">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Swap
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnSwap">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Repair
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnRepair">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Reminder
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnRepair">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Condemn
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnCondemn">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Delete
        </label>
        <div class="col-lg-3">
          <span class="m-switch m-switch--sm m-switch--icon">
            <label>
              <input type="checkbox" value="Yes" name="btnDelete">
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
      <div class="m-form__group form-group row">
        <div class="col-lg-12">
          <span class="m-section__sub">
					  Condemn Rule, there three difference type of rule.
					</span>
        </div>
      </div>
      <div class="m-form__group form-group row">
        <label class="col-lg-3 col-form-label">
         <b>Condemn Rule</b>
        </label>
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> All
        </label>
        <div class="col-lg-3">
          <label class="m-radio">
					<input type="radio" name="condemnRule" value="all">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Checker
        </label>
        <div class="col-lg-3">
          <label class="m-radio">
					<input type="radio" name="condemnRule" value="Checker">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Verifier
        </label>
        <div class="col-lg-3">
          <label class="m-radio">
					<input type="radio" name="condemnRule" value="Verifier">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
        <div class="col-lg-1">
        </div>
        <label class="col-lg-2 col-form-label">
          <span class="m-badge m-badge--brand m-badge--dot m-badge--dot-small"></span> Disposal
        </label>
        <div class="col-lg-3">
          <label class="m-radio">
					<input type="radio" name="condemnRule" value="Disposal">
					<span></span>
					</label>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
    </div>
    <div class="m-portlet__foot">
     <div class="row">
       <div class="col-lg-6 m--valign-middle">
       </div>
       <div class="col-lg-6 m--align-right">
         <button type="reset" class="btn btn-link">
           reset
         </button>
         <button type="submit" name="btn-add-role" class="btn btn-secondary">
           <i class="la la-plus"></i> Add
         </button>
       </div>
     </div>
    </div>
  </form>
</div>
<?php
}elseif($page == 'addUser'){
?>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          Add User
        </h3>
      </div>
    </div>
  </div>
  <form class="" action="" method="post" enctype="multipart/form-data">
    <div class="m-portlet__body">
      <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
  				<h5 class="m-form__heading-title">1. User Info:</h5>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="UserName" class="col-lg-2 col-form-label">Name</label>
  				<div class="col-lg-6">
  					<input class="form-control m-input" type="text" id="UserName" name="UserName">
  				</div>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="UserRole" class="col-lg-2 col-form-label">Role</label>
  				<div class="col-lg-4">
            <select class="form-control m-input" id="UserRole" name="UserRole">
              <option value="">select</option>
              <?php $sqlListRole = mysql_query("SELECT did,dataValue FROM tbl_role WHERE dataType = 'role' AND status = 'active'");if(mysql_num_rows($sqlListRole)){ while($rowListRole = mysql_fetch_assoc($sqlListRole)){ ?>
                <option value="<?php echo $rowListRole['did'] ?>"><?php echo $rowListRole['dataValue'] ?></option>
              <?php } } ?>
						</select>
  				</div>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="UserDepartment" class="col-lg-2 col-form-label">Department</label>
  				<div class="col-lg-6">
  					<input class="form-control m-input" type="text" id="UserDepartment" name="UserDepartment">
  				</div>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="UserLoc" class="col-lg-2 col-form-label">Location</label>
  				<div class="col-lg-4">
            <select class="form-control m-input" id="UserLoc" name="UserLoc">
              <option value="">select</option>
              <?php $sqlListLoc = mysql_query("SELECT did,dataValue FROM tbl_optionunit WHERE dataRef = '1'");if(mysql_num_rows($sqlListLoc)){ while($rowListLoc = mysql_fetch_assoc($sqlListLoc)){ ?>
                <option value="<?php echo $rowListLoc['did'] ?>"><?php echo $rowListLoc['dataValue'] ?></option>
              <?php } } ?>
						</select>
  				</div>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="filepathe" class="col-lg-2 col-form-label">User Image</label>
  				<div class="col-lg-6">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="filepathe" name="profileImg" onchange="filePath('filepathe')">
              <label class="custom-file-label" for="">
                <div class="" id="path">
                  Choose File
                </div>
              </label>
            </div>
  				</div>
  			</div>
        <div class="m-form__heading">
  				<h5 class="m-form__heading-title" style="margin-top:60px;">2. User Login:</h5>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="UserUsername" class="col-lg-2 col-form-label">Username</label>
  				<div class="col-lg-4">
  					<input class="form-control m-input" type="text" id="UserUsername" name="UserUsername">
  				</div>
  			</div>
        <div class="form-group m-form__group row">
  				<label for="Userpassword" class="col-lg-2 col-form-label">Password</label>
  				<div class="col-lg-4">
  					<input class="form-control m-input" type="password" id="Userpassword" name="Userpassword">
  				</div>
  			</div>
      </div>
    </div>
    <div class="m-portlet__foot">
      <div class="row">
        <div class="col-lg-6 m--valign-middle">
        </div>
        <div class="col-lg-6 m--align-right">
          <button type="reset" class="btn btn-link">
            reset
          </button>
          <button type="submit" name="btn-add-user" class="btn btn-secondary">
            <i class="la la-plus"></i> Add
          </button>
        </div>
      </div>
    </div>
  </form>
</div>
<?php
}elseif( $page == 'editUser' ){
?>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          Edit User
        </h3>
      </div>
    </div>
  </div>
  <form class="" action="" method="post" enctype="multipart/form-data">
    <div class="m-portlet__body">
      <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
          <h5 class="m-form__heading-title">1. User Info:</h5>
        </div>
        <div class="form-group m-form__group row">
          <label for="UserName" class="col-lg-2 col-form-label">Name</label>
          <div class="col-lg-6">
            <input class="form-control m-input" type="text" id="UserName" name="UserName" value="<?php echo getThisData('name',$_POST['did']); ?>">
          </div>
        </div>
        <div class="form-group m-form__group row">
          <label for="UserRole" class="col-lg-2 col-form-label">Role</label>
          <div class="col-lg-4">
            <select class="form-control m-input" id="UserRole" name="UserRole">
              <option value="">select</option>
              <?php $sqlListRole = mysql_query("SELECT did,dataValue FROM tbl_role WHERE dataType = 'role' AND status = 'active'");if(mysql_num_rows($sqlListRole)){ while($rowListRole = mysql_fetch_assoc($sqlListRole)){ ?>
                <option <?php $didi = getThisData('role',$_POST['did']); if($didi == $rowListRole['did']){ print 'selected'; } ?> value="<?php echo $rowListRole['did'] ?>"><?php echo $rowListRole['dataValue'] ?></option>
              <?php } } ?>
            </select>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <label for="UserDepartment" class="col-lg-2 col-form-label">Department</label>
          <div class="col-lg-6">
            <input class="form-control m-input" type="text" id="UserDepartment" name="UserDepartment" value="<?php echo getThisData('department',$_POST['did']); ?>">
          </div>
        </div>
        <div class="form-group m-form__group row">
          <label for="UserLoc" class="col-lg-2 col-form-label">Location</label>
          <div class="col-lg-4">
            <select class="form-control m-input" id="UserLoc" name="UserLoc">
              <option value="">select</option>
              <?php $sqlListLoc = mysql_query("SELECT did,dataValue FROM tbl_optionunit WHERE dataRef = '1'");if(mysql_num_rows($sqlListLoc)){ while($rowListLoc = mysql_fetch_assoc($sqlListLoc)){ ?>
                <option <?php $didi = getThisData('location',$_POST['did']); if($didi == $rowListLoc['did']){ print 'selected'; } ?> value="<?php echo $rowListLoc['did'] ?>"><?php echo $rowListLoc['dataValue'] ?></option>
              <?php } } ?>
            </select>
          </div>
        </div>
        <!-- <div class="form-group m-form__group row">
          <label for="filepathe" class="col-lg-2 col-form-label">User Image</label>
          <div class="col-lg-6">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="filepathe" name="profileImg" onchange="filePath('filepathe')">
              <label class="custom-file-label" for="">
                <div class="" id="path">
                  Choose File
                </div>
              </label>
            </div>
          </div>
        </div> -->

        <div class="m-form__heading">
          <h5 class="m-form__heading-title" style="margin-top:60px;">2. User Login:</h5>
        </div>
        <div class="form-group m-form__group row">
          <label for="UserUsername" class="col-lg-2 col-form-label">Username</label>
          <div class="col-lg-4">
            <input class="form-control m-input" type="text" id="UserUsername" name="UserUsername" value="<?php echo getThisData('username',$_POST['did']); ?>" readonly>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <label for="Userpassword" class="col-lg-2 col-form-label">Password</label>
          <div class="col-lg-4">
            <input class="form-control m-input" type="password" id="Userpassword" name="Userpassword" value="<?php echo getThisData('password',$_POST['did']); ?>">
          </div>
        </div>
      </div>
    </div>
    <div class="m-portlet__foot">
      <div class="row">
        <div class="col-lg-6 m--valign-middle">
        </div>
        <div class="col-lg-6 m--align-right">
          <button type="reset" class="btn btn-link">
            reset
          </button>
          <input type="hidden" name="UserDid" value="<?php echo $_POST['did'] ?>">
          <button type="submit" name="btn-edit-user" class="btn btn-secondary">
            <i class="la la-edit"></i> Edit
          </button>
        </div>
      </div>
    </div>
  </form>
</div>
<?php
}elseif ($page == 'rolePage') {
  $roleId = $_POST['role'];
  $rolePage = $_POST['pages'];
  $data = $_POST['data'];

  $sql = mysql_query("SELECT did FROM tbl_role WHERE dataValue = '$rolePage' AND dataRef = '$roleId' AND dataType = 'page'");
  $row = mysql_num_rows($sql);

  if ($row != 1) {
    mysql_query("INSERT INTO tbl_role(dataType,dataValue,dataRef,dateCreated,dateModified,status)VALUES('page','$rolePage','$roleId',NOW(),NOW(),'active')");
  }else{
    mysql_query("UPDATE tbl_role SET status = '$data' WHERE dataRef = '$roleId' AND dataType = 'page' AND dataValue = '$rolePage'");
  }
}elseif ($page == 'btnPage') {
  $did = $_POST['role'];
  $btn = $_POST['btn'];
  $data = $_POST['data'];
  $sql = mysql_query("UPDATE tbl_role SET $btn = '$data' WHERE did = '$did'");
  if(!$sql){
    echo mysql_error();
  }
}elseif ($page == 'btnPagesatu') {
  $pages = $_POST['pages'];
  $rule = $_POST['rule'];
  $did = $_POST['did'];
  $sql = mysql_query("UPDATE tbl_role SET condemnRule = '$rule' WHERE did = '$did'");
  if(!$sql){
    echo mysql_error();
  }
}
}
?>
