<?php
include('../../controller/database.php');

if (isset($_POST['formid'])) {
  $formid = $_POST['formid'];
  $sql = mysql_query("SELECT dataRefFieldset FROM tbl_form WHERE dataRefCategory = '$formid'");
   if(mysql_num_rows($sql)){
     while ($row = mysql_fetch_assoc($sql)) {
      $getfieldsetid = $row['dataRefFieldset'];
      $sqlfield = mysql_query("SELECT * FROM tbl_fieldsetunit WHERE did = '$getfieldsetid'");
      if (mysql_num_rows($sqlfield)) {
        while ($rowfield = mysql_fetch_assoc($sqlfield)) {
          $label = $rowfield['dataValueLabel'];
          $type = $rowfield['dataValueType'];
          $name = $rowfield['dataValueName'];
          $placeholder = $rowfield['dataValuePlaceholder'];
          $required= $rowfield['dataValueRequired'];
          $option = $rowfield['dataValueDropdown'];

          if ($required == 'on') {
            $required = "<span class='red'>*</span>";
            $req = "required";
          }else{
            $required = "";
            $req = "";
          }

          if($type == 'text' || $type == 'number' || $type == 'date'){ ?>
            <div class="form-group m-form__group row">
              <label class="col-form-label col-lg-3 col-sm-12">
                <?php echo $label; ?> <?php echo $required; ?>
              </label>
              <div class="col-lg-4 col-md-9 col-sm-12">
                <input type="<?php echo $type ?>" class="form-control m-input m-input--solid" name="<?php echo $name ?>" placeholder="<?php echo $placeholder ?>" <?php echo $req ?>>
              </div>
            </div>
          <?php }elseif ($type == 'select') { ?>
            <div class="form-group m-form__group row">
              <label class="col-form-label col-lg-3 col-sm-12">
                <?php echo $label; ?> <?php echo $required; ?>
              </label>
              <div class="col-lg-4 col-md-9 col-sm-12">
                <select class="form-control m-input m-input--solid" name="<?php echo $name ?>" <?php echo $req ?>>
                    <option value=""> Select </option>
                    <?php $sqlchild = mysql_query("SELECT * FROM tbl_optionunit WHERE dataRef = '$option'"); if (mysql_num_rows($sqlchild)) { while ($rowchild = mysql_fetch_assoc($sqlchild)) { ?>
                      <option value="<?php echo $rowchild['dataValue']; ?>" >
                        <?php echo $rowchild['dataValue']; ?>
                      </option>
                    <?php } } ?>
                </select>
              </div>
            </div>
          <?php }elseif ($type == 'checkbox') { ?>
            <div class="form-group m-form__group row">
              <label class="col-form-label col-lg-3 col-sm-12">
                <?php echo $label; ?> <?php echo $required; ?>
              </label>
              <div class="col-lg-4 col-md-9 col-sm-12">
                <label class="m-checkbox">
    							<input type="<?php echo $type ?>" name="<?php echo $name ?>" <?php echo $req ?>>
    							<span></span>
    						</label>
              </div>
            </div>
          <?php }elseif ($type == 'file browser') { ?>
            <div class="form-group m-form__group row">
              <label class="col-form-label col-lg-3 col-sm-12">
                <?php echo $label; ?> <?php echo $required; ?>
              </label>
              <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="custom-file">
    							<input type="file" class="custom-file-input" id="<?php echo $name ?>" name="<?php echo $name ?>" <?php echo $req ?> onchange="filePath('<?php echo $name ?>')">
    							<label class="custom-file-label" for="<?php echo $name ?>">
                    <div class="" id="path<?php echo $name ?>">
                      Choose File
                    </div>
    							</label>
    						</div>
              </div>
            </div>
          <?php
          }

        }

      }

     }
     ?>
     <div class="form-group m-form__group row">
       <label class="col-form-label col-lg-3 col-sm-12">
       </label>
       <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
         <div class="m-form__actions ">
           <button type="submit" name="btn-register-assets" class="btn btn-success">
             Submit
           </button>
           <button type="reset" class="btn btn-secondary">
             Cancel
           </button>
         </div>
       </div>
     </div>
     <?php
   }
}

?>
