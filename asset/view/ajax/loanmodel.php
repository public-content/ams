<?php
include('../../controller/database.php');

  function getTbl_rental($did,$col){
    $sql = mysql_query("SELECT $col FROM tbl_rental WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row[$col];
  }

  function rental($did,$col){
   $sql = mysql_query("SELECT $col FROM tbl_rental WHERE did = '$did'");
   $row = mysql_fetch_assoc($sql);
   return $row[$col];
  }

  function getIpaddress(){
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
  }

  function securestring($action,$string){
      $output = false;

      $encrypt_method = "AES-256-CBC";
      $secret_key = 'Nor Amirun Hazwan';
      $secret_iv = 'deBiens NahAhmad';

      // hash
      $key = hash('sha256', $secret_key);

      // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
      $iv = substr(hash('sha256', $secret_iv), 0, 16);

      if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
      }
      else if( $action == 'decrypt' ){
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
      }

      return $output;
    }



  if (isset($_POST['page'])) {
    $page = $_POST['page'];

  if($page == 'listLoan'){
    ?>
    <div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Asset Loan <small>Loan List</small>
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">
				<div class="m-section">
					<div class="m-section__content">
						<table class="table table-bordered table-hover">
						  	<thead>
						    	<tr>
						      		<th>#</th>
						      		<th>Assets Name</th>
						      		<th>Serial Number</th>
						      		<th>Who request</th>
						      		<th>Date Loan</th>
						      		<th>Location</th>
						      		<th>Action</th>
						    	</tr>
						  	</thead>
						  	<tbody>
                  <?php $bil = 1;$sql = mysql_query("SELECT * FROM tbl_rental WHERE status = 'loan'");if(mysql_num_rows($sql)){while($row = mysql_fetch_assoc($sql)){ ?>
                  <tr>
                    <td><?php echo $bil; $did=$row['did']; ?></td>
                    <td><?php echo $row['item']; ?></td>
                    <td><?php echo $row['serialNo']; ?></td>
                    <td><?php echo $row['who']; ?></td>
                    <td><?php echo date("d-m-Y", strtotime($row['startDate']));?></td>
                    <td><?php echo $row['location'];?></td>
                    <td>
                      <!-- <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"> <i class="la la-edit"></i> </a> -->
                      <!-- <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"> <i class="la la-trash"></i> </a> -->
                      <a href="#" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info" data-toggle="modal" data-target="#return<?php echo $did; ?>">Return</a>
                      <!-- <a href="#" class="btn m-btn m-btn--gradient-from-warning m-btn--gradient-to-warning">Lost</a> -->
                      <!-- <a href="#" class="btn m-btn m-btn--gradient-from-warning m-btn--gradient-to-danger">Disposal</a> -->
                    </td>
                    <div class="modal fade" id="return<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  		<div class="modal-dialog modal-sm" role="document">
                  			<div class="modal-content">
                  				<div class="modal-header">
                  					<h5 class="modal-title" id="exampleModalLabel">
                  						Delete Available Asset
                  					</h5>
                  					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  						<span aria-hidden="true">
                  							&times;
                  						</span>
                  					</button>
                  				</div>
                  				<div class="modal-body">
                  					<p>You wish to delete this ? <?php echo $row['item'] ?></p>
                  				</div>
                  				<div class="modal-footer">
                  					<form class="" action="" method="post">
                              <?php $userid = securestring('decrypt',$_SESSION['userid']); ?>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    						Close
                    					</button>
                    					<button type="button" name="btnreturn" onclick="functionReturn(<?php echo $did ?>,<?php echo $userid ?>)" class="btn btn-danger">
                    						Delete
                    					</button>
                            </form>
                  				</div>
                  			</div>
                  		</div>
                  	</div>
                  </tr>
                  <?php $bil++; }} ?>
						  	</tbody>
						</table>
					</div>
				</div>
			</div>
    <?php
    }
  }elseif(isset($_POST['func'])){
    $func = $_POST['func'];

    if($func == 'return'){
      $did = $_POST['value'];
      $ipaddress = getIpaddress();
      $by = $_POST['by'];
      $assetName = rental($did,'item');
      $assetSerial = rental($did,'serialNo');

      $sql = mysql_query("UPDATE tbl_rental SET status = 'available' WHERE did = '$did'");
      if(!$sql){
        echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Ohh No! </strong></div>";
      }else{
        $report = mysql_query("INSERT INTO tbl_report(dataServices,dataCode,dataType,dataStatus,byIP,byWho,dateCreated)
                               VALUES('','','Loan','Return Asset: $assetName($assetSerial)','$ipaddress','$by',NOW())");
        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> Successfully! </strong> </div>";
      }


    }
  }
?>
