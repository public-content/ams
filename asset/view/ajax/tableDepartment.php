<?php
include('../../controller/database.php');
include('../../class/class_general.php');

  function getValueMaster($code,$key){
    $sql = mysql_query("SELECT asValue FROM tbl_master WHERE dataCode = '$code' AND asKey = '$key'");
    $row = mysql_fetch_assoc($sql);
    return $row['asValue'];
  }

  function getLocationChar($location){
    $sql = mysql_query("SELECT dataValue FROM tbl_optionunit WHERE did = '$location'");
    $row = mysql_fetch_assoc($sql);
    return $row['dataValue'];
  }

  if(isset($_POST['service'])){
    $service = $_POST['service'];
    $display = 1;
  }

  if($display == 1){
?>
<table class="table table-striped m-table">
  <thead>
    <tr>
      <th title="Field #1">
        Code
      </th>
      <th title="Field #2">
        Name
      </th>
      <th title="Field #3">
        User
      </th>
      <th title="Field #3">
        Location
      </th>
      <th title="Field #4">
        Date
      </th>
    </tr>
  </thead>
  <tbody id="assetTable">
    <?php
    $location = getLocationChar(securestring('decrypt',$_SESSION['location']));
    $sqldataCode = mysql_query("SELECT DISTINCT dataCode FROM tbl_master WHERE dataRefServices = '$service' AND asValue = '$location' AND active = 'Yes' OR active = 'Repair'");
    if (mysql_num_rows($sqldataCode)) {
      while($rowdataCode = mysql_fetch_assoc($sqldataCode)){
        $dataCode = $rowdataCode['dataCode'];
    ?>
    <tr>
      <td>
        <?php
          $assetCode = getValueMaster($dataCode,'code');
          echo $assetCode;
        ?>
      </td>
      <td>
        <?php echo getValueMaster($dataCode,'name'); ?>
      </td>
      <td>
        <?php echo getValueMaster($dataCode,'user'); ?>
      </td>
      <td>
        <?php echo getValueMaster($dataCode,'location'); ?>
      </td>
      <td>
        <?php echo getValueMaster($dataCode,'dateReceived'); ?>
      </td>
    </tr>
        <?php
      }
    }else{
    ?>
    <tr>
      <td colspan="5" style="text-align:center;">
        No Data
      </td>
    </tr>
    <?php
    }
  ?>
  </tbody>
</table>
<?php
  }
?>
