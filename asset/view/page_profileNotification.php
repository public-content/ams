<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageNameFile	= pageInfo($page,'pageFileName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_'.$pageNameFile);
$controller = new profile();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);

function getThisCode($did){
  $sql = mysql_query("SELECT dataCode FROM tbl_reminder WHERE did = '$did'");
  $row = mysql_fetch_assoc($sql);
  $dataCode = $row['dataCode'];

  $sql2 = mysql_query("SELECT asValue FROM tbl_master WHERE asKey = 'code' AND dataCode = '$dataCode'");
  $row2 = mysql_fetch_assoc($sql2);
  return $row2['asValue'];
}

function getThisNormal($did,$column){
  $sql = mysql_query("SELECT $column FROM tbl_reminder WHERE did = '$did'");
  $row = mysql_fetch_assoc($sql);
  return $row[$column];
}

if(isset($_POST['btn-read'])){
  // var_dump($_POST);
  $task = $controller->readReminder($_POST);
  if($task == 1){
    $noty = getMessageNormal('success','Yeay');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
}

?>
<style media="screen">
  .boldel{
    font-weight: 800;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--unair">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<h3 class="m-portlet__head-text">
    							Reminder list
    						</h3>
    					</div>
    				</div>
    			</div>
    			<div class="m-portlet__body">
            <div class="m-section">
    					<div class="m-section__content">
    						<table class="table table-bordered table-hover">
    						  	<thead>
    						    	<tr>
    						      		<th>#</th>
                          <th>Code</th>
    						      		<th>Reminder</th>
    						      		<th>Action</th>
    						    	</tr>
    						  	</thead>
    						  	<tbody>
                      <?php $count = 1; $sql = mysql_query("SELECT did,dataValue,classer,dataType FROM tbl_notification WHERE userID = '$byWho' ORDER BY did DESC"); if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){ ?>
                        <tr class="<?php echo $row['classer'] ?>">
      							      	<td scope="row"><?php echo $count; $dide = $row['did']; ?></td>
                            <td><?php echo getThisCode($row['dataValue']); ?></td>
      							      	<td><?php echo getThisNormal($row['dataValue'],'remarks').": [ ".$row['dataType']." ]"; ?></td>
      							      	<td>
                              <?php if($row['classer'] == 'boldel'){ ?>
                                <a class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" title="Read" data-toggle="modal" data-target="#n_read<?php echo $dide; ?>"> <i class="la la-edit"></i> </a>
                              <?php } ?>
                            </td>
      						    	</tr>

                        <div class="modal fade" id="n_read<?php echo $dide; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                mark this reminder as read ?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                <form method="post">
                                  <input type="hidden" name="did" value="<?php echo $dide ?>">
                                  <button type="submit" name="btn-read" class="btn btn-brand">Yes</button>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php $count++; } }else{ ?>
                        <tr>
      							      	<td scope="row" colspan="8" style="text-align:center;">No Reminder</td>
      						    	</tr>
                      <?php } ?>
    						  	</tbody>
    						</table>
    					</div>
    				</div>
    			</div>
    		</div>
      </div>
    </div>
  </div>
</div>
