<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
  <div class="m-stack__item m-topbar__nav-wrapper">
    <ul class="m-topbar__nav m-nav m-nav--inline">
      <!-- <li class=" m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light m-list-search m-list-search--skin-light" m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown" m-dropdown-persistent="1">
        <a href="#" class="m-nav__link m-dropdown__toggle">
          <span class="m-nav__link-icon">
            <span class="m-nav__link-icon-wrapper">
              <i class="flaticon-search-1"></i>
            </span>
          </span>
        </a>
        <div class="m-dropdown__wrapper">
          <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
          <div class="m-dropdown__inner ">
            <div class="m-dropdown__header">
              <form  class="m-list-search__form">
                <div class="m-list-search__form-wrapper">
                  <span class="m-list-search__form-input-wrapper">
                    <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
                  </span>
                  <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                    <i class="la la-remove"></i>
                  </span>
                </div>
              </form>
            </div>
            <div class="m-dropdown__body">
              <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-max-height="300" data-mobile-max-height="200">
                <div class="m-dropdown__content"></div>
              </div>
            </div>
          </div>
        </div>
      </li> -->
      <!-- <li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  m-dropdown-toggle="click">
        <a href="#" class="m-nav__link m-dropdown__toggle">
          <span class="m-nav__link-icon">
            <span class="m-nav__link-icon-wrapper">
              <i class="flaticon-share"></i>
            </span>
          </span>
        </a>
        <div class="m-dropdown__wrapper">
          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
          <div class="m-dropdown__inner">
            <div class="m-dropdown__header m--align-center">
              <span class="m-dropdown__header-title">
                User Level Switch
              </span>
              <span class="m-dropdown__header-subtitle">
                Shortcuts
              </span>
            </div>
            <div class="m-dropdown__body m-dropdown__body--paddingless">
              <div class="m-dropdown__content">
                <div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
                  <div class="m-nav-grid m-nav-grid--skin-light">
                    <div class="m-nav-grid__row">
                      <a href="<?php echo 'system.php?p='.securestring('encrypt',101); ?>"  class="m-nav-grid__item" <?php if($_SESSION['pageload'] == 'Administrator'){?>style="background-color:#e6f0ff;"<?php }?>>
                        <i class="m-nav-grid__icon flaticon-file"></i>
                        <span class="m-nav-grid__text">
                          Administrator
                        </span>
                      </a>
                      <a href="system.php?p=<?php echo securestring('encrypt',1);?>&reload=Yes&load=Finance" class="m-nav-grid__item" <?php if($_SESSION['pageload'] == 'Finance'){?>style="background-color:#e6f0ff;"<?php }?>>
                        <i class="m-nav-grid__icon flaticon-time"></i>
                        <span class="m-nav-grid__text">
                          Finance
                        </span>
                      </a>
                    </div>
                    <div class="m-nav-grid__row">
                      <a href="<?php echo 'system.php?p='.securestring('encrypt',1); ?>" class="m-nav-grid__item" <?php if($_SESSION['pageload'] == 'Staff'){?>style="background-color:#e6f0ff;"<?php }?>>
                        <i class="m-nav-grid__icon flaticon-folder"></i>
                        <span class="m-nav-grid__text">
                          Client: Staff
                        </span>
                      </a>
                      <a href="system.php?p=<?php echo securestring('encrypt',1);?>&reload=Yes&load=Management" class="m-nav-grid__item" <?php if($_SESSION['pageload'] == 'Management'){?>style="background-color:#e6f0ff;"<?php }?>>
                        <i class="m-nav-grid__icon flaticon-clipboard"></i>
                        <span class="m-nav-grid__text">
                          Client: Management
                        </span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </li> -->
      <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
        <a href="#" class="m-nav__link m-dropdown__toggle">
          <span class="m-topbar__userpic">
            <img src="<?php echo $_SESSION['img'] ?>" class="m--img-rounded m--marginless m--img-centered" alt="<?php echo securestring('decrypt',$_SESSION['username']) ?>"/>
          </span>
          <span class="m-nav__link-icon m-topbar__usericon  m--hide">
            <span class="m-nav__link-icon-wrapper">
              <i class="flaticon-user-ok"></i>
            </span>
          </span>
          <!-- <span class="m-topbar__username m--hide">
            Shafiq
          </span> -->
        </a>
        <div class="m-dropdown__wrapper">
          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
          <div class="m-dropdown__inner">
            <div class="m-dropdown__header m--align-center">
              <div class="m-card-user m-card-user--skin-light">
                <div class="m-card-user__pic">
                  <img src="<?php echo $_SESSION['img'] ?>" class="m--img-rounded m--marginless" alt=""/>
                </div>
                <div class="m-card-user__details">
                  <span class="m-card-user__name m--font-weight-500">
                    <?php echo securestring('decrypt',$_SESSION['name']); ?>
                  </span>
                  <a href="" class="m-card-user__email m--font-weight-300 m-link">
                    <?php echo securestring('decrypt',$_SESSION['department']); ?>
                  </a>
                </div>
              </div>
            </div>
            <div class="m-dropdown__body">
              <div class="m-dropdown__content">
                <ul class="m-nav m-nav--skin-light">
                  <li class="m-nav__section m--hide">
                    <span class="m-nav__section-text">
                      Section
                    </span>
                  </li>
                  <!-- <li class="m-nav__item">
                    <a href="<?php echo 'system.php?p='.securestring('encrypt',24); ?>" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-profile-1"></i>
                      <span class="m-nav__link-title">
                        <span class="m-nav__link-wrap">
                          <span class="m-nav__link-text">
                            My Profile
                          </span>
                          <span class="m-nav__link-badge">
                            <span class="m-badge m-badge--success">
                              2
                            </span>
                          </span>
                        </span>
                      </span>
                    </a>
                  </li> -->
                  <!-- <li class="m-nav__item">
                    <a href="profile.html" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-share"></i>
                      <span class="m-nav__link-text">
                        Activity
                      </span>
                    </a>
                  </li> -->
                  <li class="m-nav__item">
                    <a href="<?php echo 'system.php?p='.securestring('encrypt',901); ?>" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-avatar"></i>
                      <span class="m-nav__link-title">
                        <span class="m-nav__link-wrap">
                          <span class="m-nav__link-text">
                            Profile
                          </span>
                        </span>
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__item">
                    <a href="<?php echo 'system.php?p='.securestring('encrypt',900); ?>" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-alert-1"></i>
                      <span class="m-nav__link-title">
                        <span class="m-nav__link-wrap">
                          <span class="m-nav__link-text">
                            Notification
                          </span>
                          <span class="m-nav__link-badge">
                            <?php echo getNotification($_SESSION['userid']); ?>
                            <!-- <span class="m-badge m-badge--success">

                            </span> -->
                          </span>
                        </span>
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__item">
                    <a href="" data-toggle="modal" data-target="#changePass" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-lock"></i>
                      <span class="m-nav__link-title">
                        <span class="m-nav__link-wrap">
                          <span class="m-nav__link-text">
                            Change Password
                          </span>
                          <span class="m-nav__link-badge">
                            <?php echo getNotification($_SESSION['userid']); ?>
                          </span>
                        </span>
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__separator m-nav__separator--fit"></li>
                  <!-- <li class="m-nav__item">
                    <a href="<?php echo 'system.php?p='.securestring('encrypt','23'); ?>" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-info"></i>
                      <span class="m-nav__link-text">
                        FAQ
                      </span>
                    </a>
                  </li> -->
                  <!-- <li class="m-nav__item">
                    <a href="profile.html" class="m-nav__link">
                      <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                      <span class="m-nav__link-text">
                        Support
                      </span>
                    </a>
                  </li> -->
                  <li class="m-nav__separator m-nav__separator--fit"></li>
                  <li class="m-nav__item">
                    <a href="./index.php?action=<?php print securestring('encrypt','logout');?>" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                      Logout
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
