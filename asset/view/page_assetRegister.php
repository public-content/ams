<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','100');
$pageDesc	= '';
$main->includephp('model','model_assetRegister');
$controller = new assetRegister();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
$ref = securestring('decrypt',$_GET['r']);
$main = securestring('decrypt',$_GET['c']);
$r = $_GET['r'];
$m = $_GET['c'];
$newdir = $pageDir.'&r='.$r;

if (isset($_POST['btn-register-assets'])) {
  //trying to call all images name in the services
  $imgName = $controller->getImgName($ref);
  //check if image name != 0
  if($imgName != ""){
  //check if images name value in array
    if(is_array($imgName)){
      // try to define by foreach the value and key
      foreach($imgName as &$value){
        $filename = $_FILES[$value]['name'];
        $filesize = $_FILES[$value]['size'];
        $filetemp = $_FILES[$value]['tmp_name'];
        // check if the filename != 0
        if($filename != ""){
          $saveName = $controller->getGenNum($ref);
          $target_dir = "images/file_assets/";
          $target_file = $target_dir . basename($filename);
          $uploadOk = 1;
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          //new name
          $new_target_file = $target_dir.$saveName.".".$imageFileType;
          // check files size
          if($filesize > 500000){
            $noty = getMessageNormal('danger','Your file is to large');
            $uploadOk = 0;
          }
          // check image type
          if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG" && $imageFileType != "GIF" && $imageFileType != "pdf" && $imageFileType != "PDF"){
            $noty = getMessageNormal('danger','Sorry, only JPG, JPEG, PNG & GIF files are allowed');
            $uploadOk = 0;
          }
          if($uploadOk == 0){
            $noty = getMessageNormal('danger','Sorry, you got an error (code 99)');
          }else{
            if (move_uploaded_file($filetemp,$new_target_file)) {
              $imgDoneUpload = 1;
              $controller->addGenNum($ref);
              $file[] = $new_target_file;
              $keyValue[] = $value;
            }else{
              $noty = getMessageNormal('danger','Sorry, you got an error (code 98)');
              $imgDoneUpload = 0;
            }
          }
        }else{
          // $noty = getMessageNormal('danger','Sorry, you got an error (code 97)');
          $imgDoneUpload = 1;
          $file[] = '';
          $keyValue[] = $value;
        }
      }
    }else{
      // an error for if arrays
      $noty = getMessageNormal('danger','Sorry, you got an error (code 96)');
    }
  }else{
    $imgDoneUpload = 1;
    $file[] = '';
    $keyValue[] = $value;
  }
  $massArray = array_combine($keyValue,$file);
  if($imgDoneUpload == 1){
    $task = $controller->saveAsset($_POST,$ref,$massArray);
    if ($task == 1) {
      // var_dump($massArray);
      createJson($ref);
      $noty = getMessageNormal('success','New Asset has been Created');
    }else{
      // var_dump($massArray);
      $noty = getMessageNormal('danger',$task);
    }
  }else{

  }
}


$alert = $noty;
?>
<style media="screen">
  .childDiv{
    display: inline-block;
    width: 500px;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $newdir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
				<div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
                  <?php echo $controller->getValue('tbl_category','dataValueWrite','did',$ref)."'s Assets Form" ?>
								</h3>
							</div>
						</div>
					</div>
          <div class="m-portlet__body">
            <form class="m-form m-form--fit m-form--label-align-right" action="" method="post" enctype="multipart/form-data">
              <div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">
									<?php echo $controller->getValueSpecial($ref); ?> - Code <span class="red">*</span>
								</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
                  <input type="hidden" name="codenum" value="<?php echo $controller->codeValue($ref); ?>">
                  <!-- <input type="hidden" name="category" value="<?php echo $controller->codeValue($ref); ?>">
                  <input type="hidden" name="subCategory" value="<?php echo $controller->codeValue($ref); ?>"> -->
									<input type="text" class="form-control m-input m-input--solid" name="code" value="<?php echo $controller->getValue('tbl_category','dataTag','did',$ref).'-'.$controller->codeValue($ref) ?>" readonly>
								</div>
							</div>
              <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                  Category <span class="red">*</span>
                </label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                  <select class="form-control m-input m-input--solid" name="eqcategory" id="eqcategory" onchange="childSelect('<?php echo $r ?>')">
                      <option value=""> Select </option>
                      <?php $sqlcat = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Main' AND dataRefService = '$ref'"); if (mysql_num_rows($sqlcat)) { while ($rowcat = mysql_fetch_assoc($sqlcat)) { ?>
                        <option value="<?php echo securestring('encrypt',$rowcat['did']) ?>" <?php if($main == $rowcat['did']){ print 'selected'; }else{ print '';} ?>>
                          <?php echo $rowcat['dataValueWrite']; ?>
                        </option>
                      <?php } } ?>
                  </select>
                </div>
              <?php if ($m != "") { ?>
                <div class="col-lg-2 col-md-9 col-sm-12" id="childSelect">
                  <select class="form-control m-input m-input--solid" name="eqchild" id="eqchild" onchange="childSelectForm()" required>
                      <option value=""> Select </option>
                      <?php $sqlchild = mysql_query("SELECT * FROM tbl_category WHERE dataType = 'Child' AND dataRefService = '$ref' AND dataRefMain = '$main'"); if (mysql_num_rows($sqlchild)) { while ($rowchild = mysql_fetch_assoc($sqlchild)) { ?>
                        <option value="<?php echo $rowchild['did']; ?>" <?php if($rs == $rowchild['did']){print 'selected';} ?>>
                          <?php echo $rowchild['dataValueWrite']; ?>
                        </option>
                      <?php } } ?>
                  </select>
                </div>
              <?php } ?>
              </div>
              <div class="m-form__seperator m-form__seperator--dashed" style="margin-bottom:18px;"></div>
              <div class="" id="thisForm">
                <!-- do not do anything here -->
              </div>
              <!-- <div class="m-form__seperator m-form__seperator--dashed"></div> -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script type="text/javascript">
  function childSelect(r){
    var c = document.getElementById('eqcategory').value;
    var url = "<?php echo $pageDir; ?>";
    location.replace(url + '&r=' + r + '&c=' + c);
  }
  function childSelectForm(){
    var did = document.getElementById('eqchild').value;
    var dataString = { formid : did };

    $.ajax({
      type:"post",
      url:"asset/view/ajax/openForm.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('thisForm').innerHTML = html;
      }
    });
    return false;
  }
  function filePath(filepathe){
    var id = "path"+filepathe;
    var value = document.getElementById(filepathe).value;
    var res = value.replace("C:\\fakepath\\", "C:\\");
    var asd = document.getElementById(id).innerHTML = ""+res;
  }
</script>
