<?php
	$page = securestring('decrypt',$_GET['p']);
	$h = securestring('decrypt',$_GET['h']);
	$pageID	= pageInfo($page,'did');
	$pageTitle = pageInfo($page,'pageTitle');
	$pageName	= pageInfo($page,'pageName');
	$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
	$homeDir = 'system.php?p='.securestring('encrypt','2');
	$pageDesc	= '';
	$main->includephp('model','model_assetInventory');
	$controller = new assetInventory();
	$byWho = securestring('decrypt',$_SESSION['userid']);
	$controller->pushWho($byWho);
	$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
	
	if (isset($_POST['btn-category-add'])) {
		$task = $controller->saveThis($_POST,'parent');
		if ($task == 1) {
			$noty = getMessageNormal('success','New Parent has been created');
			}else{
			$noty = getMessageNormal('danger',$task);
		}
		}elseif (isset($_POST['btn-subcategory-add'])) {
		$task = $controller->saveThis($_POST,'child');
		if ($task == 1) {
			$noty = getMessageNormal('success','New Parent has been created');
			}else{
			$noty = getMessageNormal('danger',$task);
		}
		}elseif (isset($_POST['del'])) {
		$task = $controller->deleteThis($_POST);
		if ($task == 1) {
			$noty = getMessageNormal('success','The data has been deleted');
			}else{
			$noty = getMessageNormal('danger',$task);
		}
		}elseif (isset($_POST['btn-add-stock'])) {
		$task = $controller->addingStock($_POST);
		if ($task == 1) {
			$noty = getMessageNormal('success','New stock has been added');
			}else{
			$noty = getMessageNormal('danger',$task);
		}
		}elseif (isset($_POST['stock-out'])) {
		$task = $controller->stockOut($_POST);
		if ($task == 1) {
			$noty = getMessageNormal('success','A stock has been take out');
			}else{
			$noty = getMessageNormal('danger',$task);
		}
	}
	
	$alert = $noty;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					<?php echo $pageTitle; ?>
				</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="<?php echo $pageDir ?>" class="m-nav__link">
							<span class="m-nav__link-text">
								<?php echo $pageName; ?>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<div>
				<!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
					<i class="la la-plus m--hide"></i>
					<i class="la la-ellipsis-h"></i>
					</a>
				</div> -->
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="row">
			<?php if ($alert != '') { ?>
				<div class="col-lg-12">
					<?php  echo $alert; ?>
				</div>
			<?php } ?>
			<div class="col-lg-3">
				<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
					<div class="m-portlet__head">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
										<i class="flaticon-share m--hide"></i>
										Category
									</a>
								</li>
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
										Sub Category
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="m_user_profile_tab_1">
							<div class="m-portlet m-portlet--mobile">
								<form class="m-form m-form--fit m-form--label-align-right" method="post">
									<div class="m-portlet__body">
										<div class="form-group m-form__group">
											<label for="">
												Category
											</label>
											<input type="text" name="category" class="form-control m-input m-input--square" required>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<button type="submit" name="btn-category-add" class="btn btn-success">
												Add
											</button>
											<button type="reset" class="btn btn-secondary">
												Cancel
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="tab-pane " id="m_user_profile_tab_2">
							<form class="m-form m-form--fit m-form--label-align-right" method="post">
								<div class="m-portlet__body">
									<div class="form-group m-form__group">
										<label for="">
											Category Name
										</label>
										<select class="form-control m-input m-input--square" id="" name="category" required>
											<option value="">
												select
											</option>
											<?php $sql = mysql_query("SELECT * FROM tbl_inventory WHERE dataType = 'MAIN'"); if (mysql_num_rows($sql)) { while ($row = mysql_fetch_assoc($sql)) { ?>
												<option value="<?php echo $row['did'] ?>">
													<?php echo $row['dataValue']; ?>
												</option>
											<?php } } ?>
										</select>
									</div>
									<div class="form-group m-form__group">
										<label for="">
											Sub-Category
										</label>
										<input type="text" name="sub-category" class="form-control m-input m-input--square" required>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<button type="submit" name="btn-subcategory-add" class="btn btn-success">
											Add
										</button>
										<button type="reset" class="btn btn-secondary">
											Cancel
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-9">
				<div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									<?php echo $pageName ?>
									<small>
										<?php echo $pageDesc ?>
									</small>
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<div class="m-section">
							<div class="m-section__content">
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>
													#
												</th>
												<th>
													Category
												</th>
												<th>
													Sub Category
												</th>
												<th>
													Stock
												</th>
												<th>
													Out
												</th>
												<th>
													Balance
												</th>
												<th>
													Total Spent
												</th>
												<th>
													Action
												</th>
											</tr>
										</thead>
										<tbody>
											<?php $bil=1; $sql = mysql_query("SELECT * FROM tbl_inventory WHERE dataType = 'MAIN'"); if (mysql_num_rows($sql)) { while ($row = mysql_fetch_assoc($sql)) { ?>
												<tr style="background:rgba(127, 208, 208, 0.49)">
													<td colspan="1">
														<?php echo $bil ?>
													</td>
													<td colspan="6">
														<b><?php echo $row['dataValue']; ?></b>
													</td>
													<td>
														<a href="#" data-toggle="modal" data-target="#del<?php echo $row['did']; ?>" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill"> <i class="la la-trash"></i> </a>
													</td>
												</tr>
												<?php $did = $row['did']; ?>
												<?php $bill=1; $sqls = mysql_query("SELECT * FROM tbl_inventory WHERE dataRef = '$did' AND dataType = 'CHILD'"); if (mysql_num_rows($sqls)) { while ($rows = mysql_fetch_assoc($sqls)) { ?>
													<tr>
														<th scope="row">
															
														</th>
														<td>
															<?php // echo $controller->findThis($rows['dataRef'],'dataRef','dataValue','tbl_inventory'); ?>
														</td>
														<td>
															<?php echo $rows['dataValue'] ?>
														</td>
														<td>
															<?php echo $rows['stock'] ?>
														</td>
														<td>
															<?php echo $rows['stockout'] ?>
														</td>
														<td>
															<?php echo $rows['stock'] - $rows['stockout'] ?>
														</td>
														<td style="background:rgba(164, 127, 164, 0.31);color:black;">
															<?php echo 'RM'.$rows['totalrm'] ?>
														</td>
														<td>
															<a href="#" data-toggle="modal" data-target="#add_stock<?php echo $rows['did']; ?>" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill"> <i class="la la-plus"></i> </a>
															<a href="#" data-toggle="modal" data-target="#out_stock<?php echo $rows['did']; ?>" class="btn btn-sm btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill"> <i class="la la-sign-out"></i> </a>
															<a href="<?php echo $pageDir."&h=".securestring('encrypt',$rows['did']) ?>" class="btn btn-sm btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill"> <i class="la la-history"></i> </a>
															<a href="#" data-toggle="modal" data-target="#del2<?php echo $rows['did']; ?>" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill"> <i class="la la-trash"></i> </a>
														</td>
														
														<div class="modal fade" id="del2<?php echo $rows['did']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
															<div class="modal-dialog modal-sm" role="document">
																<div class="modal-content">
																	<form class="" action="" method="post">
																		<div class="modal-header">
																			<h5 class="modal-title" id="exampleModalLabel">
																				Delete <?php echo $rows['dataValue'] ?>
																			</h5>
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																				<span aria-hidden="true">
																					&times;
																				</span>
																			</button>
																		</div>
																		<div class="modal-body">
																			<p>Do you wish to delete <?php echo $rows['dataValue'] ?></p>
																		</div>
																		<input type="hidden" name="did" value="<?php echo $rows['did'] ?>">
																		<input type="hidden" name="name" value="<?php echo $rows['dataValue'] ?>">
																		<div class="modal-footer">
																			<button type="button" class="btn btn-secondary" data-dismiss="modal">
																				Close
																			</button>
																			<button type="submit" name="del" class="btn btn-danger">
																				Yes
																			</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
														
														<div class="modal fade" id="add_stock<?php echo $rows['did']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<form class="" action="" method="post">
																		<div class="modal-header">
																			<h5 class="modal-title" id="exampleModalLabel">
																				Add Stock (<?php echo $rows['dataValue']; ?>)
																			</h5>
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																				<span aria-hidden="true">
																					&times;
																				</span>
																			</button>
																		</div>
																		<div class="modal-body">
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Total New
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="newitem" type="text" required>
																				</div>
																			</div>
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Purchase (RM)
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="totalrm" type="text" required>
																				</div>
																			</div>
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Vendor
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="vendor" type="text" required>
																				</div>
																			</div>
																		</div>
																		<div class="modal-footer">
																			<input type="hidden" name="did" value="<?php echo $rows['did'] ?>">
																			<button type="button" class="btn btn-secondary" data-dismiss="modal">
																				Close
																			</button>
																			<button type="submit" name="btn-add-stock" class="btn btn-primary">
																				Add
																			</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
														
														<div class="modal fade" id="out_stock<?php echo $rows['did']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<form class="" action="" method="post">
																		<div class="modal-header">
																			<h5 class="modal-title" id="exampleModalLabel">
																				Take Out Stock (<?php echo $rows['dataValue']; ?>)
																			</h5>
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																				<span aria-hidden="true">
																					&times;
																				</span>
																			</button>
																		</div>
																		<div class="modal-body">
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Stock Out
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="stockout" type="text" required>
																				</div>
																			</div>
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Reason
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="reason" type="text" required>
																				</div>
																			</div>
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Person Take Out
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="personr" type="text" required>
																				</div>
																			</div>
																			<div class="form-group m-form__group row">
																				<label class="col-4 col-form-label">
																					Person Request
																				</label>
																				<div class="col-8">
																					<input class="form-control m-input" name="persont" type="text" value="<?php echo $controller->getName($byWho); ?>" required>
																				</div>
																			</div>
																		</div>
																		<div class="modal-footer">
																			<input type="hidden" name="did" value="<?php echo $rows['did'] ?>">
																			<button type="button" class="btn btn-secondary" data-dismiss="modal">
																				Close
																			</button>
																			<button type="submit" name="stock-out" class="btn btn-primary">
																				Take Out
																			</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
														
													</tr>
												<?php } } ?>
												
												<div class="modal fade" id="del<?php echo $row['did']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-sm" role="document">
														<div class="modal-content">
															<form class="" action="" method="post">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">
																		Delete <?php echo $row['dataValue'] ?>
																	</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">
																			&times;
																		</span>
																	</button>
																</div>
																<div class="modal-body">
																	<p>Do you wish to delete <?php echo $row['dataValue'] ?></p>
																</div>
																<input type="hidden" name="did" value="<?php echo $row['did'] ?>">
																<input type="hidden" name="name" value="<?php echo $row['dataValue'] ?>">
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">
																		Close
																	</button>
																	<button type="submit" name="del" class="btn btn-danger">
																		Yes
																	</button>
																</div>
															</form>
														</div>
													</div>
												</div>
											<?php $bil++; } } ?>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<?php if ($h != "") { $name = $controller->findThis($h,'did','dataValue','tbl_inventory'); ?>
				<div class="col-lg-12">
					<div class="m-portlet m-portlet--mobile">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Inventory Report (<?php echo $name ?>)
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="m-section">
								<div class="m-section__content">
									<label>
										Add Stock - <?php echo $name ?>
									</label>
									<table class="table table-bordered m-table m-table--border-metal">
										<thead>
											<tr style="background:#f2f2f2;">
												<th>
													#
												</th>
												<th>
													Date Add
												</th>
												<th>
													Quantity New
												</th>
												<th>
													Price Purchase (RM)
												</th>
												<th>
													Vendor
												</th>
											</tr>
										</thead>
										<tbody>
											<?php $bil=1; $sql1 = mysql_query("SELECT * FROM rpt_inventory WHERE dataRef = '$h'AND dataType = 'ADDSTOCK'"); if (mysql_num_rows($sql1)) { while ($row1 = mysql_fetch_assoc($sql1)) { ?>
												<tr>
													<th scope="row">
														<?php echo $bil ?>
													</th>
													<td>
														<?php echo $row1['dateCreated']; ?>
													</td>
													<td>
														<?php echo $row1['dataValueQtyNew']; ?>
													</td>
													<td>
														<?php echo 'RM'.$row1['dataValueRm']; ?>
													</td>
													<td>
														<?php echo $row1['dataValueVendor']; ?>
													</td>
												</tr>
											<?php $bil++; } ?>
											<tr style="background:#ccfff5;font-weight:600;">
												<td colspan="2" style="text-align:center;">
													Total
												</td>
												<td>
													<?php echo $controller->findThis($h,'did','stock','tbl_inventory'); ?>
												</td>
												<td colspan="2">
													<?php echo 'RM'.$controller->findThis($h,'did','totalrm','tbl_inventory'); ?>
												</td>
											</tr>
											<?php }else{ ?>
											<tr>
												<th scope="row" colspan="5" style="text-align:center;">
													No Data
												</th>
											</tr>
											<?php }?>
											
											
										</tbody>
									</table>
									
									<label>
										Take Out Stock - <?php echo $name ?>
									</label>
									<table class="table table-bordered m-table m-table--border-metal">
										<thead>
											<tr style="background:#f2f2f2;">
												<th>
													#
												</th>
												<th>
													Date Out
												</th>
												<th>
													Quantity Out
												</th>
												<th>
													Reason
												</th>
												<th>
													Person Take Out
												</th>
												<th>
													Person Request
													
												</th>
											</tr>
										</thead>
										<?php $bil=1; $sql2 = mysql_query("SELECT * FROM rpt_inventory WHERE dataRef = '$h' AND dataType = 'TAKEOUT'"); if (mysql_num_rows($sql2)) { while ($row2 = mysql_fetch_assoc($sql2)) { ?>
											<tbody>
												<tr>
													<th scope="row">
														<?php echo $bil ?>
													</th>
													<td>
														<?php echo $row2['dateCreated']; ?>
													</td>
													<td>
														<?php echo $row2['dataValueQtyOut']; ?>
													</td>
													<td>
														<?php echo $row2['dataValueReason']; ?>
													</td>
													<td>
														<?php echo $row2['dataValuePersonRequest']; ?>
													</td>
													<td>
														<?php echo $row2['dataValuePersonTakeOut']; ?>
													</td>
												</tr>
											<?php $bil++; } ?>
											<tr style="background:#ccfff5;font-weight:600;">
												<th scope="row" colspan="2" style="text-align:center;">
													Total
												</th>
												<td colspan="4">
												<?php echo $controller->findThis($h,'did','stockout','tbl_inventory'); ?>
												</td>
												</tr>
												<?php }else{ ?>
												<tr style="">
    											<th colspan="6" style="text-align:center;">
												No Data
    											</th>
												</tr>
												<?php } ?>
												</tbody>
												</table>
												</div>
												</div>
												</div>
												</div>
												</div>
												<?php } ?>
												
												<div class="col-lg-12">
												<div class="m-portlet m-portlet--mobile">
												<div class="m-portlet__head">
												<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
												Inventory Report
												</h3>
												</div>
												</div>
												</div>
												<div class="m-portlet__body">
												<div class="m-scrollable" data-scrollable="true" data-max-height="500" data-scrollbar-shown="true">
												<div class="form-group m-form__group row">
												<label for="example-text-input" class="col-lg-8 col-form-label">
												</label>
												<label for="example-text-input" class="col-lg-1 col-form-label">
												Search
												</label>
												<div class="col-lg-3">
												<input class="form-control m-input" type="text" placeholder="Search..." id="myInput">
												</div>
												</div>
												<table class="table table-bordered m-table m-table--border-metal">
												<thead>
												<tr style="background:#f2f2f2;">
												<th>
												#
												</th>
												<th>
												Type
												</th>
												<th>
												Description
												</th>
												<th>
												Date
												</th>
												<th>
												User
												</th>
												</tr>
												</thead>
												<tbody id="myTable">
												<?php $bil=1; $sql = mysql_query("SELECT * FROM rpt_inventory ORDER BY did DESC"); if (mysql_num_rows($sql)) { while ($row = mysql_fetch_assoc($sql)) { ?>
												<tr>
												<th scope="row">
												<?php echo $bil ?>
												</th>
												<td>
												<?php echo $row['dataType'] ?>
												</td>
												<td>
												<?php echo $row['dataValueDesc'] ?>
												</td>
												<td>
												<?php echo $row['dateCreated'] ?>
												</td>
												<td>
												<?php echo $row['dataValuePersonRequest'] ?>
												</td>
												</tr>
												<?php $bil++; } }else{ ?>
												<tr>
												<th scope="row" colspan="5" style="text-align:center;">
												No Data
												</th>
												</tr>
												<?php } ?>
												</tbody>
												</table>
												</div>
												</div>
												</div>
												</div>
												</div>
												</div>
												</div>
<!--												<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
												<script>
												$(document).ready(function(){
												$("#myInput").on("keyup", function() {
												var value = $(this).val().toLowerCase();
												$("#myTable tr").filter(function() {
												$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
												});
												});
												});
												</script>
																								