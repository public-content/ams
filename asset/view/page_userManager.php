<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','2');
$pageDesc	= '';
$main->includephp('model','model_userManager');
$controller = new userManager();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);

if(isset($_POST['btn-add-role'])){
  $task = $controller->sendNewRoleData($_POST);
  if($task == 1){
    $noty = getMessageNormal('success',$_POST['newRole'].' has been registerd');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
  // var_dump($_POST);
}elseif(isset($_POST['btn-edit-role'])) {
  $task = $controller->sendEditRoleData($_POST);
  if($task == 1){
    $noty = getMessageNormal('success',$_POST['newRole'].' has been Updated');
  }else{
    $noty =  getMessageNormal('danger',$task);
  }
}elseif (isset($_POST['btn-add-user'])) {
  $UserUsername = mysql_real_escape_string(trim($_POST['UserUsername']));
  $checkUsername = $controller->checkUsername($UserUsername);
  // var_dump($_POST);
  $filename = $_FILES['profileImg']['name'];
  $filesize = $_FILES['profileImg']['size'];
  $filetemp = $_FILES['profileImg']['tmp_name'];

  if ($checkUsername == 0) {
    if($filename != ''){
      $newFilename = strtotime('now');
      $target_dir = "images/profile/";
      $target_file = $target_dir . basename($filename);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $newTargetfile = $target_dir.$newFilename.".".$imageFileType;

      if($filesize > 500000){
        $noty = getMessageNormal('danger','Your file is to large');
        $uploadOk = 0;
      }
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG" && $imageFileType != "GIF" && $imageFileType != "pdf" && $imageFileType != "PDF"){
        $noty = getMessageNormal('danger','Sorry, only JPG, JPEG, PNG & GIF files are allowed');
        $uploadOk = 0;
      }
      if($uploadOk == 0){
        $noty = getMessageNormal('danger','Sorry, you got an error (code 99)');
      }else{
        if (move_uploaded_file($filetemp,$newTargetfile)) {
          $imgDoneUpload = 1;
          $img = $newTargetfile;
        }else{
          $noty = getMessageNormal('danger','Sorry, you got an error (code 98)');
          $imgDoneUpload = 0;
        }
      }
    }else{
      $imgDoneUpload = 1;
      $img = "";
    }
  }else{
    $noty = getMessageNormal('danger','Sorry, Username Exist');
  }

  if($imgDoneUpload == 1){
    $task = $controller->sendProfileData($_POST,$img);
    if ($task == 1) {
      $noty = getMessageNormal('success','New User has been Created');
    }else{
      $noty = getMessageNormal('danger',$task);
    }
  }

}elseif(isset($_POST['btn-edit-user'])){
  $task = $controller->sendProfileDataEdit($_POST);
  if ($task == 1) {
    $noty = getMessageNormal('success','User has been Update');
  }else{
    $noty = getMessageNormal('danger',$task);
  }
  // var_dump($_POST);
}


$alert = $noty;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#mySearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#assetTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
        <!-- height: -moz-max-content; -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <?php if ($alert != '') { ?>
      <div class="col-lg-12">
        <?php  echo $alert; ?>
      </div>
      <?php } ?>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-3" style="margin-bottom:28.6px;">
                <a class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-danger  m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnPage('user')">
                  <span>
                    <i class="la la-user"></i>
                    <span>
                      User
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;" id="tbl_user"><?php echo $controller->findTotal('tbl_user'); ?></span>
                    <!-- <?php echo $controller->findTotal('tbl_user'); ?> -->
                  </span>
                </a>
              </div>
              <div class="col-lg-3" style="margin-bottom:28.6px;">
                  <a class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-primary m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnPage('role')">
                  <span>
                    <i class="la la-users"></i>
                    <span>
                      User Level
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <!-- <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;"><?php echo $controller->findTotal('tbl_role'); ?></span> -->
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;" id="tbl_role"><?php echo $controller->findTotal2('tbl_role'); ?></span>
                  </span>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-12" id="displayPageHere">
                <div class="m-portlet m-portlet--mobile">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													User List
												</h3>
											</div>
										</div>
                    <div class="m-portlet__head-tools">
                      <a class="btn btn-secondary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" style="color:black;border:1px solid black;" onclick="tapToAddUser('addUser')">
                        <span>
                          <i class="la la-plus"></i>
                          <span>
                            New User
                          </span>
                        </span>
                      </a>
                    </div>
									</div>
									<div class="m-portlet__body">
                    <table class="table">
        							<tdead class="thead-inverse">
        								<tr>
        									<td>
        										#
        									</td>
        									<td>
        										Img
        									</td>
        									<td>
        										Name
        									</td>
        									<td>
        										Username
        									</td>
        									<td>
        										User level
        									</td>
        									<td>
        										Department
        									</td>
        									<td>
        										Location
        									</td>
        									<td>
        										Action
        									</td>
        								</tr>
        							</thead>
        							<tbody id="myTable">
                        <?php $bil = 1; $sqlUser = mysql_query("SELECT * FROM tbl_user");if(mysql_num_rows($sqlUser)){ while($rowUser = mysql_fetch_assoc($sqlUser)){ ?>
                          <tr>
          									<td scope="row">
          										<?php echo $bil ?>
          									</td>
          									<td style="padding-top: 4.872px;">
                              <div class="m-card-user__pic">
          											<img src="<?php echo $rowUser['img'] ?>" alt="" class="m--img-rounded m--marginless" alt="" width="30"/>
          										</div>
                    				</td>
          									<td>
          										<?php echo $rowUser['name']; ?>
          									</td>
          									<td>
          										<?php echo $rowUser['username']; ?>
          									</td>
          									<td>
          										<?php echo $controller->getThis('tbl_role','dataValue',$rowUser['role']);  ?>
          									</td>
          									<td>
          										<?php echo $rowUser['department']; ?>
          									</td>
          									<td>
          									  <?php echo $controller->getThis('tbl_optionUnit','dataValue',$rowUser['location']);  ?>
          									</td>
                            <td>
                              <a href="#" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill" title="Edit User Profile" onclick="tapToEditUser('editUser','<?php echo $rowUser['did'] ?>')"> <i class="la la-edit"></i> </a>
                              <a href="#" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline m-btn--pill" title="Delete User Profile" onclick="tapToDel('user','<?php echo $rowUser['did'] ?>')"> <i class="la la-trash"></i> </a>
                            </td>
                          </tr>
                        <?php $bil++; } }else{ ?>
                          <tr>
          									<td scope="row" rowspan="7">
          										No Data
          									</td>
          								</tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
								</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script type="text/javascript">
// runThis('tbl_role');
// runThis('tbl_user');
function btnPage(page){
  var dataString = { page : page };

  $.ajax({
    type:"post",
    url:"asset/view/ajax/userManager.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('displayPageHere').innerHTML = html;
    }
  });
  return false;
}

function tapToAdd(page){
  var dataString = { page : page };

  $.ajax({
    type:"post",
    url:"asset/view/ajax/userManager.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('displayPageHere').innerHTML = html;
    }
  });
  return false;
}

function tapToAddUser(page){
  var dataString = { page : page };

  $.ajax({
    type:"post",
    url:"asset/view/ajax/userManager.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('displayPageHere').innerHTML = html;
    }
  });
  return false;
}

function tapToEditUser(page,did){
  var dataString = { page : page, did : did };

  $.ajax({
    type:"post",
    url:"asset/view/ajax/userManager.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('displayPageHere').innerHTML = html;
    }
  });
  return false;
}

function tapToDel(page,did){
  var dataString = { page : page, deldid : did };
  var xuser = document.getElementById('tbl_user').innerHTML;
  var xrole = document.getElementById('tbl_role').innerHTML;

  if(page == 'role'){
    // alert('role');
     document.getElementById('tbl_role').innerHTML = xrole - 1;
  }else if (page == 'user') {
    // alert('user');
    document.getElementById('tbl_user').innerHTML = xuser - 1;
  }

  $.ajax({
    type:"post",
    url:"asset/view/ajax/userManager.php",
    data:dataString,
    cache:false,
    success: function(html){
      document.getElementById('displayPageHere').innerHTML = html;
    }
  });
  return false;
}

function runThis(value){
  setInterval(function() {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST","asset/view/ajax/getRole.php?get="+value,false);
    xmlhttp.send(null);
    document.getElementById(value).innerHTML=xmlhttp.responseText;
    console.log('value');
  }, 3000);
}

  function tapToEdit(did){
    var page = "edit";
    var dataString = { page : page, did : did };

    $.ajax({
      type:"post",
      url:"asset/view/ajax/userManager.php",
      data:dataString,
      cache:false,
      success: function(html){
        document.getElementById('displayPageHere').innerHTML = html;
      }
    });
    return false;
  }
  function filePath(filepathe){
    var id = "path";
    var value = document.getElementById(filepathe).value;
    var res = value.replace("C:\\fakepath\\", "C:\\");
    var asd = document.getElementById(id).innerHTML = ""+res;
  }
  function changePageStatus(role,page,data){
     //alert(role);
    var pages = "rolePage";
   var dataString = { page : pages, role : role, pages : page, data : data };
    $.ajax({
      type:"post",
      url:"asset/view/ajax/userManager.php",
      data:dataString,
      cache:false,
      success: function(html){
         document.getElementById('displayPageHere').innerHTML = html;
      }
    });
    return false;
  }

  function changeBtnStatus(role,btn,data){
    // alert(data);
    var pages = "btnPage";
    var dataString = { page : pages, role : role, btn : btn, data : data };
    $.ajax({
      type:"post",
      url:"asset/view/ajax/userManager.php",
      data:dataString,
      cache:false,
      success: function(html){
        // document.getElementById('displayPageHere').innerHTML = html;
      }
    });
    return false;
  }

  function changeCondemnRule(rule,did){
    var pages = "btnPagesatu";
    var dataString = { page : pages, rule : rule, did : did };
    $.ajax({
      type:"post",
      url:"asset/view/ajax/userManager.php",
      data:dataString,
      cache:false,
      success: function(html){
        // document.getElementById('displayPageHere').innerHTML = html;
      }
    });
    return false;
  }
</script>
