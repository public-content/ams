<?php
$page = securestring('decrypt',$_GET['p']);
$pageID	= pageInfo($page,'did');
$pageTitle = pageInfo($page,'pageTitle');
$pageName	= pageInfo($page,'pageName');
$pageDir = 'system.php?p='.securestring('encrypt',$pageID);
$homeDir = 'system.php?p='.securestring('encrypt','2');
$pageDesc	= '';
$main->includephp('model','model_dashboard');
$controller = new Dashboard();
$byWho = securestring('decrypt',$_SESSION['userid']);
$controller->pushWho($byWho);
$controller->checkSession($_SESSION['sessionID'],$_SESSION['loginType']);
?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script>
$(document).ready(function(){
  $("#mySearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#assetTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          <?php echo $pageTitle; ?>
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="<?php echo $pageDir ?>" class="m-nav__link">
              <span class="m-nav__link-text">
                <?php echo $pageName; ?>
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div>
        <!-- <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
          <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
            <i class="la la-plus m--hide"></i>
            <i class="la la-ellipsis-h"></i>
          </a>
        </div> -->
        <!-- height: -moz-max-content; -->
      </div>
    </div>
  </div>
  <div class="m-content">
  	<div class="row">
      <div class="col-lg-12">
        <?php $alertMe = $alert; ?>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-3" style="margin-bottom:28.6px;">
                <a class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning  m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnFunction('fc')">
                  <span>
                    <i class="la la-folder-open"></i>
                    <span>
                      Assets
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;">3432</span>
                  </span>
                </a>
              </div>
              <div class="col-lg-3" style="margin-bottom:28.6px;">
                  <a class="btn m-btn m-btn--gradient-from-warning m-btn--gradient-to-danger m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnFunction('fc')">
                  <span>
                    <i class="la la-warning"></i>
                    <span>
                      Condemn
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;">420</span>
                  </span>
                </a>
              </div>
              <div class="col-lg-3" style="margin-bottom:28.6px;">
                  <a class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn m-btn--icon" style="width:100%;height:150px;color:white;" onclick="btnFunction('fc')">
                  <span>
                    <i class="la la-user"></i>
                    <span>
                      User
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;">3</span>
                  </span>
                </a>
              </div>
              <div class="col-lg-3" style="margin-bottom:28.6px;">
                  <a href="<?php echo 'system.php?p='.securestring('encrypt',4); ?>" class="btn m-btn m-btn--gradient-from-warning m-btn--gradient-to-danger m-btn m-btn--icon" style="width:100%;height:150px;color:white;">
                  <span>
                    <i class="flaticon-open-box"></i>
                    <span>
                      Inventory
                    </span>
                    <span style="position: absolute;top: 2rem;font-size: 1.5rem;right:3.5rem;">Total</span>
                    <span style="position: absolute;font-size: 6rem;right: 3rem;top: 3rem;">2</span>
                  </span>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-8">
                <div class="m-portlet m-portlet--tab">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Total Bar Chart <small>yearly</small>
												</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body">
										<div id="m_morris_3" style="height:500px;"></div>
									</div>
								</div>
              </div>
              <div class="col-lg-4">
                <div class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
									<div class="m-portlet__head m-portlet__head--fit-">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text m--font-light">
												Total Expenditure <small>Yearly</small>
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
													<a href="#" class="m-portlet__nav-link btn btn--sm m-btn--pill m-btn btn-outline-light m-btn--hover-light">
														2018
													</a>
													<!-- <div class="m-dropdown__wrapper">
														<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
														<div class="m-dropdown__inner">
															<div class="m-dropdown__body">
																<div class="m-dropdown__content">
																	<ul class="m-nav">
																		<li class="m-nav__section m-nav__section--first">
																			<span class="m-nav__section-text">
																				Orders
																			</span>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div> -->
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="m-widget27 m-portlet-fit--sides">
											<div class="m-widget27__pic">
												<img src="images/file_more/file_background/bg-4.jpg" alt="">
												<h3 class="m-widget27__title m--font-light" style="top:50%">
													<span>
														<span>
															RM
														</span>
                            256,100
													</span>
												</h3>
												<div class="m-widget27__btn">
													<button type="button" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--bolder">
														Inclusive All Inventory
													</button>
												</div>
											</div>
											<div class="m-widget27__container">
												<!-- begin::Nav pills -->
												<ul class="m-widget27__nav-items nav nav-pills nav-fill" role="tablist">
													<li class="m-widget27__nav-item nav-item">
														<a class="nav-link active" data-toggle="pill" href="#m_personal_income_quater_1">
															Assets
														</a>
													</li>
													<li class="m-widget27__nav-item nav-item">
														<a class="nav-link" data-toggle="pill" href="#m_personal_income_quater_2">
															Inventory
														</a>
													</li>
												</ul>

												<div class="m-widget27__tab tab-content m-widget27--no-padding">
													<div id="m_personal_income_quater_1" class="tab-pane active">
														<div class="row  align-items-center">
															<div class="col">
																<div id="m_chart_personal_income_quater_1" class="m-widget27__chart" style="height: 160px">
																	<div class="m-widget27__stat">
																		78%
																	</div>
																</div>
															</div>
															<div class="col">
																<div class="m-widget27__legends">
																	<div class="m-widget27__legend">
																		<span class="m-widget27__legend-bullet m--bg-accent"></span>
																		<span class="m-widget27__legend-text">
																			37% New
																		</span>
																	</div>
																	<div class="m-widget27__legend">
																		<span class="m-widget27__legend-bullet m--bg-warning"></span>
																		<span class="m-widget27__legend-text">
																			63% Send Repair
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="m_personal_income_quater_2" class="tab-pane fade">
														<div class="row  align-items-center">
															<div class="col">
																<div id="m_chart_personal_income_quater_2" class="m-widget27__chart" style="height: 160px">
																	<div class="m-widget27__stat">
																		22%
																	</div>
																</div>
															</div>
															<div class="col">
																<div class="m-widget27__legends">
																	<div class="m-widget27__legend">
																		<span class="m-widget27__legend-bullet m--bg-focus"></span>
																		<span class="m-widget27__legend-text">
																			100% New
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script src="js/morris-chartsC.js" type="text/javascript"> </script>
