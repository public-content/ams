<?php
class Engine {

	public function __construct(){
		$this->includephp('class','class_general');
		$this->includephp('controller','database');
		$this->includephp('controller','message');
		$this->includephp('controller','session');
	}

	public function includephp($folder,$filename) {
	include('asset/'.$folder.'/'.$filename.'.php');
	}

	public function callingScript($folder,$filename) {
		$data = "<script src='asset/".$folder."/".$filename.".js'></script>";
		return $data;
	}

	public function getUserIPP(){
		// Get real visitor IP behind CloudFlare network
		if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
							$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
							$_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		}
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP))
		{
				$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
				$ip = $forward;
		}
		else
		{
				$ip = $remote;
		}

		return $ip;
	}
}

?>
